from dataset import maximal_episodes

def argument_parser_for_frequent_episodes(parser):
    # MAXIMAL EPISODES PARAMS
    maximal_episodes.argument_parser_for_maximal_episodes(parser)

    # FREQUENCY CONSTRAINT
    parser.add_argument('--minimal_frequency', 
            type = int, 
            default = 5,
            help = ('The minimal frequency for an episode to be considered '
                'as frequent enough.'))

    parser.add_argument('-C', '--candidate_generation', 
            help = "Choose the method to use in order to generate \
                    the candidates. Choose between edlike, fsminer \
                    and apriori. Default is fsminer.",
            choices=['edlike', 'fsminer', 'apriori', 'fpgrowth', 'fpminer'],
            default='fpgrowth',
            )

