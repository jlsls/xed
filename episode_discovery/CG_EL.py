#!/usr/bin/python
# -*- coding: utf-8 -*-

# FIXME: 
'''
Traceback (most recent call last):
  File "/usr/lib/python2.7/runpy.py", line 162, in _run_module_as_main
    "__main__", fname, loader, pkg_name)
  File "/usr/lib/python2.7/runpy.py", line 72, in _run_code
    exec code in run_globals
  File "/home/julie/Documents/xED/trunk/src/episode_discovery/CG_EL.py", line 567, in <module>
    el.build_from_MySQLdb_cursor(cursor)
  File "/home/julie/Documents/xED/trunk/src/episode_discovery/CG_EL.py", line 521, in build_from_MySQLdb_cursor
    self.new_event(event)
  File "/home/julie/Documents/xED/trunk/src/episode_discovery/CG_EL.py", line 191, in new_event
    child.link_to_parents(node, label) # Finish child_node's wiring 
  File "/home/julie/Documents/xED/trunk/src/episode_discovery/CG_EL.py", line 57, in link_to_parents
    other_parent = grand_parent.child[label]
KeyError: 'Meal_Preparation begin'
'''

import sys
import logging 
import time
import datetime

from dataset import Database, Event, Episode
import dataset.parser

from episode_discovery.CandidateGenerator import CandidateGenerator


class CG_EL(CandidateGenerator):
    pass

# Pas de periodicity !!!
class Node:
    def __init__(self, episode=tuple(), level=0, parent=None, label=None):
        self.episode = episode
        self.TQ = []
        self.support = 0

        self.parent = {}
        if parent and label:
            self.parent[label] = parent
            parent.child[label] = self
        self.child = {}
        self.level = level
        self.detached = False

    def __str__(self):
        s = "\n\tEpisode:\t%s\nsupport:\t%d\nparents:\t%s\nchildren:\t%s\n" % ( 
                ', '.join(self.episode),
                self.support,
                ', '.join(self.parent.keys()),
                ', '.join(self.child.keys()))
        return s
    def __repr__(self):
        return str(self)

    def link_to_parents(self, known_parent, label):
        """
        Knowing one parent, find all the others. The known parent is not 
        required to be linked with the current node yet.

        Parameters:
            episode: the set of labels composing the current node
            label: the diference between the parent and the current node

        Return: Nada

        Note: All the parents of the current node are available via 
        self.parent[label].parent[e].child[label] for e in episode.
        """

        assert self.level >= 2 
        # useless otherwise: __ROOT__ has no parents, and the nodes at level 1 
        # have only one parent: __ROOT__

        print 
        print self.episode

        known_parent.child[label] = self
        self.parent[label] = known_parent

        print "  KP:", known_parent.episode
        for e in self.episode:
            if e == label:
                continue
            grand_parent = known_parent.parent[e]
            print
            print " GP:", grand_parent.episode
            if label not in grand_parent.child:
                print grand_parent.child
            other_parent = grand_parent.child[label]
            print " OP:", other_parent.episode
            other_parent.child[e] = self
            self.parent[e] = other_parent


    def detach(self):
        # node is rare, kill it, and all of its children

        to_be_detached = [self, ]
        while to_be_detached:
            n = to_be_detached.pop(0)
            for key in n.parent.keys():
                # Remove link from parent to child
                assert n.parent[key].child[key] == n
                del n.parent[key].child[key]
                # Remove link from child to parent
                del n.parent[key]
            n.detached = True
            for child in n.child:
                to_be_detached.append(n.child[child])
                pass
            pass
        return
    
    def recompute_support(self):
        last_end = datetime.datetime.min
        support = 0
        for b, e in self.TQ:
            if b > last_end:
                last_end = e
                support += 1
        self.support = support
        return support

    def print_TQ(self):
        for ts, te in self.TQ:
            print "%s, %s → %s" % ts.strftime("%x"), ts.strftime("%X"), te.strftime("%X")

class EpisodeLattice:
    def __init__(self, 
            maximal_episode_duration=datetime.timedelta(seconds=1800), 
            minimal_support=5,
            maximal_depth=None, 
            window_length=datetime.timedelta(weeks=1)
            ):
        self.root = Node()
        self.maximal_episode_duration = maximal_episode_duration
        self.minimal_support = minimal_support
        self.maximal_depth = maximal_depth
        self.labels = []
        self.n_fq_episodes = 0
        self.current_level = 0


        # Struct for the streaming version
        self.window_length = window_length
        self.ROOT = Node()
        self.LABELS = []
        self.FEcount = 0
        self.RMN_ = [[], ]
        self.depth = 1

        # Some stats
        self._n_seen_events = 0
        
    def new_event(self, event):
        logging.debug(" New event: %s" % event)
        label = event.label
        timestamp = event.timestamp
        self._n_seen_events += 1

        # Add the new MOs the event participates in
        # 1. In the 1-episode
        if label not in self.ROOT.child:
            self.LABELS.append(label)
            self.ROOT.child[label] = Node(
                    episode = (label, ), 
                    level = 1, 
                    parent = self.ROOT, 
                    label = label)
            logging.debug("Created node: " + ",".join(self.ROOT.child[label].episode))
        node = self.ROOT.child[label]
        if node.TQ and timestamp == node.TQ[-1][0]:
            # Twice the same event -> nothing to do
            return 
        else:
            node.support += 1
            node.TQ.append((timestamp, timestamp))
            if node not in self.RMN_[0]:
                self.RMN_[0].append(node)
            self.apriori_check_node(node)

        # 2. As the extension of an RMN
        #TODO
        for RMN_depth in xrange(self.depth):
            for node in self.RMN_[RMN_depth][:]:
                # 2.a. Get a candidate node for extension
                if not node.parent or node.detached:
                    # Node not in the lattice anymore (rare)
                    if node.parent:
                        node.detach()
                        assert False
                    self.RMN_[RMN_depth].remove(node)
                    continue
                assert node.parent
                assert node.level == RMN_depth+1
                if label in node.episode:
                    continue

                # 2.b. Check the timestamp
                if label in node.child:
                    # The extended ep is already frequent. New MO?
                    child = node.child[label]
                    assert child.TQ
                    assert node.TQ
                    if child.TQ[-1][0] == node.TQ[-1][0]:
                        # node.TQ[-1] was already used to build an occurrence 
                        # starting in node.TQ[-1][0]
                        pass
                    else:
                        if timestamp - node.TQ[-1][0] < self.maximal_episode_duration:
                            # Does this occurrence make the last occurrence null and void? This happens only when start = end and last.end = end
                            if child.TQ[-1][1] == timestamp:
                                assert child.TQ[-1][0] <= node.TQ[-1][0]
                                child.TQ.pop()
                            # New MO !
                            child.TQ.append((node.TQ[-1][0], timestamp))
                            if child not in self.RMN_[RMN_depth+1]:
                                self.RMN_[RMN_depth+1].append(child)
                        else:
                            # node is too old, get it out of RMN
                            self.RMN_[RMN_depth].remove(node)
                        self.apriori_check_node(node)
                else:
                    # Has the extended ep just become frequent ?
                    TQ, support = self.merge_TQs(node.TQ, self.ROOT.child[label].TQ)
                    if support >= self.minimal_support:
                        child = Node(node.episode + (label, ), level=node.level+1)
                        logging.debug("Created node: %s", child.episode)
                        child.TQ = TQ
                        child.support = support
                        child.link_to_parents(node, label) # Finish child_node's wiring 
                        if len(self.RMN_) < child.level:
                            self.RMN_.append([])
                            self.depth += 1
                        if child not in self.RMN_[RMN_depth+1]:
                            self.RMN_[RMN_depth+1].append(child)
                        self.apriori_check_node(node)
                    pass
                pass


        
        # Remove outdated occurrences
        # TODO
        if self.window_length:
            threshold = timestamp - self.window_length
            to_be_investigated = [self.ROOT, ]
            while to_be_investigated:
                node = to_be_investigated.pop(0)
                for label in node.child.keys():
                    child = node.child[label]
                    has_changed = False
                    while child.TQ and child.TQ[0][0] < threshold:
                        child.TQ.pop(0)
                        has_changed = True

                    if has_changed:
                        if not child.TQ:
                            # No occurrence left
                            child.detach()
                        else:
                            child.recompute_support()
                            if child.support < self.minimal_support and child.level > 1:
                                child.detach()
                            else:
                                # Changed, but is still frequent
                                to_be_investigated.append(child)

        return


    def __len__(self):
        seen = []
        to_be_explored = [self.ROOT, ]
        i = 0
        while to_be_explored:
            node = to_be_explored.pop(0)
            seen.append(node)
            if node.recompute_support() >= self.minimal_support:
                i += 1
            for child in node.child:
                if node.child[child] not in seen and node.child[child] not in to_be_explored:
                    to_be_explored.append(node.child[child])
        return i




    def new_event_not_stream(self, event):
        """
        Add the current event to the first level of the lattice. When all 
        events are seen, call build_latiice to finish the construction of the 
        lattice.

        Parameter:
            event: an Event instance
        """
        label = event.label
        timestamp = event.timestamp

        if label not in self.root.child:
            self.labels.append(label)
            self.root.child[label] = Node(episode = (label, ), 
                    level = 1, 
                    parent = self.root, 
                    label = label)
        node = self.root.child[label]
        if node.TQ and timestamp == node.TQ[-1][0]:
            # Twice the same event
            return 
        node.support += 1
        node.TQ.append((timestamp, timestamp))

        for child in self.root.child.keys():
            while self.root.child[child].TQ and self.root.child[child].TQ[0][0] < timestamp - self.window_length:
                self.root.child[child].TQ.pop(0)
                self.root.child[child].support -= 1
            if not self.root.child[child].TQ:
                del self.root.child[child]
                self.labels.remove(child)



    def build_lattice(self):
        """
        Calls to new_event_not_stream built the first level of the lattice. 
        This function call finishes the generation of frequent episode, and 
        the construction of the lattice. 

        BFS traversal of the yet-to-be-built lattice. The nodes_to_extend list 
        contains the recently built nodes. When a node is in nodes_to_extend, 
        it means the corresponding episode is frequent, and superepisodes 
        should be searched (children nodes have to be investigated).
        """
        # FIXME: choix autre que arbitraire ?
        frequent_labels = [l for l in self.labels 
                if l in self.root.child and self.root.child[l].support >= self.minimal_support]
        label_index = {}
        for fl in frequent_labels:
            assert frequent_labels.count(fl) == 1
            label_index[fl] = frequent_labels.index(fl)

        nodes_to_extend = [self.root.child[i] for i in frequent_labels]

        while nodes_to_extend:
            node = nodes_to_extend.pop(0)
            # Logging purpose only...
            if node.level > self.current_level:
                logging.debug(" ... Building level %d (%d fq episodes so far)" 
                        % (node.level, self.n_fq_episodes))
                self.current_level = node.level

            self.n_fq_episodes += 1
            last_label_index = label_index[node.episode[-1]]
            for label in frequent_labels[last_label_index+1:]:
                # Build the MOs for node.episode U label
                assert label not in node.child # It'd mean the episode has already been explored. Not possible.

                TQ, support = self.merge_TQs(node.TQ, self.root.child[label].TQ)
                # If node.episode U label is frequent, its time queue is TQ
                if support >= self.minimal_support:
                    child_node = Node(node.episode + (label, ), level = node.level+1, parent = node, label=label)
                    child_node.TQ = TQ
                    child_node.support = support
                    child_node.link_to_parents(node, label) # Finish child_node's wiring 
                    nodes_to_extend.append(child_node) # child_node now needs children of its own!
                    pass
                pass
            pass
        return
                

    def merge_TQs(self, TQ1, TQ2):
        """
        Build the time queue and compute the support of the episode union of 
        the two episodes whose time queues are in input. 
        
        Parameters:
            TQ1, TQ2: list of 2-uples. The start and end times of the MOs of 
            the 2 subepisodes to merge.

        Returns:
            TQ: the union time queue
            support: the NOMOs count
        """
        last_end = datetime.datetime.min # For non overlapping logging
        i, j = 0, 0 # indexes on TQ1, TQ2
        L1, L2 = len(TQ1), len(TQ2)

        # Searched results
        TQ = []
        support = 0

        while (i < L1 and j < L2):
            if TQ1[i][1] >= TQ2[j][1]:
                # TQ1[i] finishes after TQ2[j]: an MO finishing in TQ1[i][1]?
                # Find the greatest j such that TQ2[j] finishes before TQ1[i]
                while (j+1 < L2 and TQ2[j+1][1] <= TQ1[i][1]):
                    j += 1
                if TQ1[i][0] <= TQ2[j][0]:
                    # TQ1[i] starts first
                    # Note: occurrence cannot be too long, otherwise item 
                    # i wouldn't be in TQ1 in the first place
                    # => We have a MO
                    TQ.append((TQ1[i][0], TQ1[i][1]))
                    if TQ1[i][0] > last_end:
                        # Count only if NOMO
                        support += 1
                        last_end = TQ1[i][1]
                    # increment the index of the suboccurrence starting first
                    if TQ1[i][0] == TQ2[j][0]:
                        j += 1
                    i += 1
                else:
                    # TQ2[j] starts first
                    if not self.maximal_episode_duration or TQ1[i][1] - TQ2[j][0] <= self.maximal_episode_duration:
                        # Max length: check!
                        # => We have an MO
                        TQ.append((TQ2[j][0], TQ1[i][1]))
                        if TQ2[j][0] > last_end:
                            # Count only of NOMO
                            support += 1
                            last_end = TQ1[i][1]
                    # increment the index of the suboccurrence starting first
                    j += 1

            else:
                # TQ2[j] finishes last: an MO finishing in TQ2[j][1]?
                # Find the greatest i such that TQ1[i] finishes before TQ2[j]
                while (i+1 < L1 and TQ1[i+1][1] <= TQ2[j][1]):
                    i += 1
                if TQ2[j][0] <= TQ1[i][0]:
                    # TQ2[j] starts first
                    # Note: cannot be too long, otherwise item j wouldn't be in TQ2 in the first place
                    TQ.append((TQ2[j][0], TQ2[j][1]))
                    if TQ2[j][0] > last_end:
                        # Count only of NOMO
                        support += 1
                        last_end = TQ2[j][1]
                    # increment the index of the suboccurrence starting first
                    if TQ1[i][0] == TQ2[j][0]:
                        i += 1
                    j += 1
                else:
                    #TQ1[i] starts first
                    if not self.maximal_episode_duration or TQ2[j][1] - TQ1[i][0] <= self.maximal_episode_duration:
                        # Max length: check!
                        # => MO
                        TQ.append((TQ1[i][0], TQ2[j][1]))
                        if TQ1[i][0] > last_end:
                            support += 1
                            last_end = TQ2[j][1]
                    # increment the index of the suboccurrence starting first
                    i += 1
        return TQ, support

    def check_apriori_property(self):
        logging.info(" Apriori, static lattice")
        to_be_explored = [self.root, ]
        seen = []
        while to_be_explored:
            n = to_be_explored.pop(0)
            seen.append(n)
            self.apriori_check_node(n)
            for child in n.child:
                to_be_explored.append(n.child[child])
                
        logging.info(" Apriori, dynamic lattice")
        to_be_explored = [self.ROOT, ]
        seen = []
        while to_be_explored:
            n = to_be_explored.pop(0)
            seen.append(n)
            self.apriori_check_node(n)
            for child in n.child:
                to_be_explored.append(n.child[child])

    def apriori_check_node(self, node):
        # Node is more frequent than its children. 
        if node.level==0:
            return

        node.recompute_support()

        for c in node.child:
            node.child[c].recompute_support()
            if node.child[c].support > node.support:
                print "parent:", node.episode, node.support
                node.print_TQ()
                print
                print "child: ", node.child[c].episode, node.child[c].support
                node.child[c].print_TQ()
                print
                assert False

    def compare_results(self):
        logging.info(" Traversal of the static tree, and assert matching with the dynamic one...")
        to_be_explored = [(self.root, self.ROOT), ]
        seen = []
        while to_be_explored:
            n1, n2 = to_be_explored.pop(0)
            seen.append(n1)
            assert n1.TQ == n2.TQ
            for child in n1.child:
                if n1.child[child] in seen or n1.child[child] in to_be_explored:
                    continue
                if child not in n2.child:
                    print "Dynamic does not have node", n1.child[child].episode
                assert child in n2.child
                to_be_explored.append((n1.child[child], n2.child[child]))

        logging.info(" Traversal of the dynamic tree, and assert matching with the static one...")
        to_be_explored = [(self.ROOT, self.root), ]
        seen = []
        while to_be_explored:
            n1, n2 = to_be_explored.pop(0)
            seen.append(n1)
            assert n1.TQ == n2.TQ
            for child in n1.child:
                if n1.child[child] in seen or n1.child[child] in to_be_explored:
                    continue

                if child not in n2.child:
                    print "Static does not have node", n1.child[child].episode
                else:
                    assert child in n2.child
                    to_be_explored.append((n1.child[child], n2.child[child]))

    def get_occurrences(self, episode):
        """
        Get the TQ for the episode 

        Parameter:
        ----------
            episode: list of labels
        """
        current_node = self.ROOT
        for label in episode:
            if label not in current_node.child:
                logging.warning(" The searched episode (%s) is not in the EpisodeLattice" % ', '.join(episode))
                return []
            else:
                current_node = current_node.child[label]
        else: 
            # The node corresponding to the searched episode has been reached
            return current_node.TQ

    def build_from_MySQLdb_cursor(self, cursor):
        """
        Build the EpisodeLattice with the events in the query.

        Assumes a "SELECT timestamp, label"-like query was executed, and that 
        the result iterator has not been consumed yet. 

        Parameter:
        ----------
            cursor: a valid MySQLdb cursor, with a SELECT statement
        """

        for timestamp, label in cursor:
            event = Event(timestamp, label)
            self.new_event(event)

if __name__ == '__main__':
    from argparse import ArgumentParser
    import DB_Utils

    parser = ArgumentParser()

    # DATA FILE
    parser.add_argument('-d', '--dbname', 
            metavar = 'DATASET',
            required = True, 
            help = 'Read DATASET from mysql server')

    parser.add_argument('-v', '--verbose', 
            action = 'store_true', 
            help = "Set logging to debub mode")

    parser.add_argument('--maximal_episode_duration', default = 1800, type=int, help="Maximal duration of the episode occurrences")
    parser.add_argument('--minimal_support', default = 5, type=int, help="Minimal count of NOMOs")
    parser.add_argument('--window_length', default=604800, type=int, help="Duration of the window")
    parser.add_argument('--maximal_depth', default=None, type=int, help="Maximal depth of the built lattice (NOT IMPLEMENTED YET!)") # TODO


    ## READ THE PARAMETERS
    options = parser.parse_args()

    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    logging.info("OPTIONS: \n%s" % 
            '\n'.join(['\t%s:\t%s' % (a[0], a[1]) 
                for a in vars(options).items()]))

    # GET THE EVENTS
    cursor = DB_Utils.db_connection(options.dbname)
    DB_Utils.get_events_in_cursor(cursor)

    # INITIALIZE THE LATTICE
    el = EpisodeLattice(
            minimal_support = options.minimal_support, 
            maximal_episode_duration = datetime.timedelta(seconds=options.maximal_episode_duration), 
            maximal_depth = options.maximal_depth,
            window_length = datetime.timedelta(seconds=options.window_length),
            )

    # BUILD IT
    el.build_from_MySQLdb_cursor(cursor)

    logging.info(" Streaming verion: %s nodes" % len(el))


#    el.check_apriori_property()
#    el.compare_results()

