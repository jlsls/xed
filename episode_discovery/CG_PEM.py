#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Not sure if it works, don't remember what it is
"""

import sys 
import logging
import time
import os

import dataset.parser
from   dataset import Database, Event, Episode

class CG_PEM:
    """
    Parallel episode miner
    """

    def __init__(self, episode_length=1800, 
            minimal_support = 5,
            episode_capacity = None,
            sharing_same_nodes = False,
            ):
        self.episode_length = episode_length
        self.episode_capacity = episode_capacity
        self.minimal_support = minimal_support
        self.ssn = sharing_same_nodes

        self.nitems = 0
        self.items = []
        self.IL = {} # item list
        self.EM = {} # episode matrix
        self.FET = {} # frequent episode tree
        self.MQ = {} # message queue, indexed on the labels

    def new_event(self, event):
        ts = event.timestamp
        label = event.label

        # Dict update 
        if label not in self.items:
            # New line
            self.EM[label] = {}

            # Add a column in the existing lines
            for l in self.items:
                self.EM[l][label] = {'locked': True,
                        'count': 0,
                        'TQ': [],
                        'first_el': None
                        }
            
            # TQ for the 1-itemset
            self.IL[label] = {'count': 0,
                    'TQ': []
                    }
            self.items.append(label)
            self.nitems += 1

        # Row update
        for l in self.EM[label]:
            if self.EM[label][l]['locked'] == False:
                # Occurrence in progress
                if self.EM[label][l]['first_el'] == True:
                    # Nothing to do : we saw twice label
                    pass
                else:
                    # Last seen was l
                    if ts - self.IL[l]['TQ'][-1] <= self.episode_length:
                        # An occurrence !
                        self.EM[label][l]['TQ'].append((self.IL[l]['TQ'][-1], ts))
                        self.EM[label][l]['count'] += 1
                        self.EM[label][l]['locked'] = True
                        self.EM[label][l]['first_el'] = None
                    else:
                        # Too long... label thus becomes our last seen event
                        self.EM[label][l]['first_el'] = True
            else:
                # No occurrence started yet
                self.EM[label][l]['locked'] = False
                self.EM[label][l]['first_el'] = True

        # Column update
        for l in self.EM:
            if label not in self.EM[l]:
                # The row was created AFTER the first occ of label
                continue
            if self.EM[l][label]['locked'] == False:
                # Occurrence in progress
                if self.EM[l][label]['first_el'] == True:
                    # Last seen was l 
                    if ts - self.IL[l]['TQ'][-1] <= self.episode_length:
                        # An occurrence !
                        self.EM[l][label]['TQ'].append((self.IL[l]['TQ'][-1], ts))
                        self.EM[l][label]['count'] += 1
                        self.EM[l][label]['locked'] = True
                        self.EM[l][label]['first_el'] = None
                    else:
                        # Too long... label thus becomes our last seen event
                        self.EM[l][label]['first_el'] = True
                else:
                    # Nothing to do: label was already the last seen event
                    pass
            else:
                # No occurrence started yet
                self.EM[l][label]['locked'] = False
                self.EM[l][label]['first_el'] = False

        # IL update
        if not self.IL[label]['TQ'] or self.IL[label]['TQ'][-1] != ts:
            self.IL[label]['TQ'].append(ts)
            self.IL[label]['count'] += 1
       

    def build_fet(self):
        self.fet = {'label': '__ROOT_',
                'parent': None,
                'children': [],
                'TQ': [],
                'count': 0,
                }

        extendable = []
        nnodes = 0
        
        # Init the tree: the frequent 1-episodes
        for el in self.IL:
            if self.IL[el]['count'] >= self.minimal_support:
                # the 1-episode is frequent
                new_node = {
                    'label': el,
                    'parent': self.fet,
                    'children': [],
                    'TQ': [(i, i) for i in self.IL[el]['TQ']],
                    'count': self.IL[el]['count']
                    }
                self.MQ[el] = new_node
                extendable.append(new_node)
                self.fet['children'].append(new_node)
                nnodes += 1

        # Search the tree: buildhigher-order episodes
        while extendable:
            current_ep = extendable.pop(0)
            last_item = current_ep['label']
            for el in self.EM[last_item]:
                tq = self.tq_for_ep_extended_with_el(current_ep, el)
                if len(tq) >= self.minimal_support:
                    # the (n+1)-ep is frequent. Add it in the tree, and set it as candidate for extension
                    new_node = {
                            'label': el,
                            'parent': current_ep,
                            'children': [],
                            'TQ': tq,
                            'count': len(tq)
                            }
                    if (self.ssn and 
                            el in self.MQ and 
                            self.MQ[el]['TQ'] == new_node['TQ']):
                        # Sharing same node ?
                        new_node['first same node'] = self.MQ[el]
                    else:
                        # Nope...
                        self.MQ[el] = new_node
                        extendable.append(new_node)
                    current_ep['children'].append(new_node)
                    nnodes += 1
                    if nnodes % 1000 == 0:
                        print "%d nodes" % nnodes

        print "%d frequent parallel episodes" % nnodes

    def tq_for_ep_extended_with_el(self, episode_node, event_label):
        tq1 = episode_node['TQ']
        tq2 = self.EM[episode_node['label']][event_label]['TQ']
        
        tq = []
        count = 0
        l1 = len(tq1)
        l2 = len(tq2)
        i, j = 0, 0

        while (i < l1 and j < l2):
            # Find the 1st j such that tq2[j] starts AFTER tq1[i]
            while (j < l2 and tq2[j][0] < tq1[i][1]):
                j += 1
            if j == l2:
                break

            # Find i such that tq2[j] happens AFTER tq1[i], but BEFORE 
            # tq1[i+1]
            while (i+1 < l1 and tq2[j][0] >= tq1[i+1][1]):
                i += 1
            if tq2[j][1] - tq1[i][0] <= self.episode_length:
                # `episode_length` constraint is met: we have a new DMO!
                assert tq2[j][1] - tq1[i][0] >= 0
                tq.append((tq1[i][0], tq2[j][1]))
                count += 1

            # Do it all over again, with the next value in tq1
            i += 1

        assert len(tq) == count
        return tq











if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()

    # DATA FILE
    parser.add_argument('-f', '--filename', 
            metavar = 'DATASET',
            required = True, 
            help = 'Read DATASET dump file')

    parser.add_argument('-v', '--verbose', 
            action = 'store_true', 
            help = "Set logging to debub mode")

    general_tools.frequent_episode_search.argument_parser_for_frequent_episodes(parser)
    parser.add_argument('-o', '--offline',
            action='store_true', default=False,
            help= ("Don't update the FET with incoming events, just EM. " +
                "The FET is then built manually"))

    parser.add_argument('--ssn', action='store_true',
            help = ("Reduce the number of nodes in the tree thanks to same nodes")
            )


    ## READ THE PARAMETERS
    options = parser.parse_args()

    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    print ("OPTIONS: \n%s" % 
            '\n'.join(['\t%s:\t%s' % (a[0], a[1]) 
                for a in vars(options).items()]))

    from general_tools.dataset.parser import read_dataset
    with open(options.filename, 'r') as f:
        dataset = read_dataset(f)

        cg_pem = CG_PEM(episode_length = options.episode_length,
                episode_capacity = options.episode_capacity,
                minimal_support = options.minimal_frequency,
                sharing_same_nodes = options.ssn)
        i = 0
        for event in dataset.events:
            i += 1
            if (i % 100000) == 0:
                print "%d events" % i
            cg_pem.new_event(event)

        cg_pem.build_fet()
