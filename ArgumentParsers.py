from argparse import *

class Parser(ArgumentParser):
    def __init__(self):
        ArgumentParser.__init__(self)#, add_help=False)
        pass

    # ---------------------------------------------------------------
    #
    # APriori frequent itemsets
    #
    # ---------------------------------------------------------------
    def cand_generation(self):
        group = self.add_argument_group(title='Candidate generation',
                description = 'Arguments for the candidate search')
        group.add_argument('-C', '--candidate_generation', 
                help = "Choose the method to use in order to generate \
                        the candidates. Choose between edlike, fsminer \
                        and apriori. Default is fsminer.",
                choices=['edlike', 'fsminer', 'apriori', 'fpgrowth', 'fpminer'],
                default='fpgrowth',
                )
        group.add_argument('--min_frequency', type=int,
                help = 'minimal support for an itemset to be frequent')
        group.add_argument('--ep_capacity', type=int,
                help = 'maximal length for the itemsets')
        group.add_argument('--ep_length', type=int,
                help = 'maximal duration of an episode, in seconds')
        pass


    def periodicity_determination(self):
        group = self.add_argument_group(title='Periodicity determination',
                description='Arguments for the periodicity description')
        group.add_argument('-P', '--periodicity_determination',
            help = "Choose the method to be used in order to find \
                    the periodicities. Choose between mixture and \
                    autocorrelation",
            choices = ['mixture', 'autocorrelation'],
            default='mixture',)

        # 
        # If periodicity_determination = mixture
        # Choice of comp nb determination method
        # 
        group.add_argument('--nbcomp_method', 
                choices = ['naive', '1dimDBSCAN', 'DBSCAN', ],
                help = "Choose the method to be used in order to determine the \
                        number of components in the mixture model. \
                        Useless option if `--periodicity_determination autocorrelation`")
        group.add_argument('--max_nbcomp', 
            help = "Constraint the maximal number of components in \
                    the mixture of Gaussian model when --bruteforce=True")
        group.add_argument('--brute_force', help='Try all values of nbcomp', action='store_true')


    def online_mining(self):
        group = self.add_argument_group(title='Online mining',
                description='Arguments to define the event stream mining')

        group.add_argument('--offline_mining', action='store_true', 
                help='Disables online mining fonctions')
        group.add_argument('--chunking_unit', 
                choices = ['event', 'time'], default='time',
                help='The basic unit to divide the database into chunks')
        group.add_argument('--chunking_step', type=int, 
                help='The step by which to move the window to make next database chunk')
        group.add_argument('--chunk_size', type=int,
                help='The duration of each chunk')
