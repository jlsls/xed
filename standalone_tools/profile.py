#!/usr/bin/python
# -*- coding: utf-8 -*-

import pstats
import os.path
from argparse import ArgumentParser

def profiling_stats(filename, n=10, order='cumulative'):
    """
    Open filename, print its profiling stats. The stats are ordered as  
    specified by order, and the printing is restricted to the first n items. 
    For more information on the available keywords for order, and their 
    meaning, check the documentation for pstats.Stats.sort_stats()

    Parameters:
        filename: str
            A profiler dump file, obtained via:
            python -m cProfile -o filename myscript.py ...
        n: int, optional, default to 10
        order: str, optional, defaults to 'cumulative'

    Returns:
        Nothing, the results are just printed.
    """

    if not os.path.isfile(filename):
        print "Nothing to do: there is no profile dump file to analyze"
        return 
    else:
        p = pstats.Stats(filename)
        p.sort_stats(order).print_stats(n)



if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-f', '--filename',
            help = 'Profiling dump file', 
            type=str, required=True
            )

    parser.add_argument('-o', '--order', 
            help = ('Sorting order for the stats. See the documentation of ' + 
                'pstats.Stats.sort_stats(...) for more information on the ' +
                'available keywords'), 
            choices = ['calls', 'cumulative', 'cumtime', 'file', 'filename', 
                'module', 'ncalls', 'pcalls', 'line', 'name', 'nfl', 'stdname', 
                'time', 'tottime'],
            default = 'cumulative'
            )

    parser.add_argument('-n', 
            help= "Restrict to the n first lines of stats",
            type=int,
            default=10)

    options = parser.parse_args()

    print ("Options:\n%s\n" % '\n'.join(['\t%s:\t%s' % (a[0], a[1])
        for a in vars(options).items()]))
    
    profiling_stats(options.filename, options.n, options.order)
