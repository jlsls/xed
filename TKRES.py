#!user/bin/python
import datetime
import math
import logging
import os
import itertools
import csv

import DB_Utils

class Parameters:
    """ Dummy class, repertoring the parameters used for TKRES """
    def __init__(self,  
            t_ep=datetime.timedelta(minutes=30), 
            batch_duration=datetime.timedelta(days=1),
            indexgroup=2,
            k=10,
            m=30):
        self.t_ep = t_ep
        self.batch_duration = batch_duration
        self.measures = "{0:0>7b}".format(indexGroup)
        self.k = k
        self.m = m

class Loggers:
    """ Dummy class, repertoring the loggers """
    def __init__(self):
        pass

    def add_logger(self, loggername, logger):
        setattr(self, loggername, logger)

    def add_csv_logger(self, name, path):
        self.add_logger(name, csv.writer(open(os.path.join(path, "%s.csv" % name), 'w')))
        

class TKRES_miner:
    """ The miner """
    def __init__(self, params, loggers, db_cursor, tabname="EVENT_VIEW"):
        self.params = params
        self.loggers = loggers
        self.db_cursor = db_cursor
        self.tabname = tabname

        # k-tree
        self.k_tree_root = TreeNode(None)
        self.k_tree_root.status = 'top_k'
        self.tree_depth = 0

        # top-k-list
        self.top_k_list = []
        self.n_episodes_in_top_k_list = 0

        self.batch_id = 0
        self.n_events_per_batch = []
        self.starttime_per_batch = []
        self.endtime_per_batch = []

        # For tree traversal
        self.first_node_per_depth = [None]
        self.last_node_per_depth = [None]
    
    def main(self, db_cursor, tabname="EVENT_VIEW"):
        first_timestamp = DB_Utils.beginning(db_cursor, tabname)
        last_timestamp = DB_Utils.end(db_cursor, tabname)
        query = """ SELECT timestamp, label FROM %s
                    ORDER BY timestamp """  % tabname
        db_cursor.execute(query)
        
        beginning_of_next_batch = first_timestamp
        spare_event = None

        # Read data the first m batches
        for i in xrange(params.m):
            logging.info(" =============> BATCH %d" % self.batch_id)
            current_batch, spare_event = self.get_events_in_batch(db_cursor, beginning_of_next_batch, spare_event=spare_event)
            beginning_of_current_batch = beginning_of_next_batch
            beginning_of_next_batch += params.batch_duration
            self.starttime_per_batch.append(beginning_of_current_batch)
            self.endtime_per_batch.append(beginning_of_next_batch)
            self.update_first_tree_level(current_batch)
            if current_batch:
                assert current_batch[0][0] >= beginning_of_current_batch
                assert current_batch[-1][0] <= beginning_of_next_batch

        # Build initial tree
        self.compute_tree_depth()
        ret = self.build_initial_tree()
        self.loggers.time_monitoring['TimeCreateTree'] = ret

        # Mine the episodes
        ret = self.mine_top_k_episodes()
        self.loggers.time_monitoring['mining'] = ret

        while beginning_of_current_batch < last_timestamp:
            logging.info(" =============> BATCH %d" % self.batch_id)

            # Remove outdated data
            ret = self.remove_outdated_data(self.endtime_per_batch[0])
            self.loggers.time_monitoring['remove_old_data'].append(ret)

            # Read data from the new batch
            current_batch, spare_event = self.get_events_in_batch(db_cursor, beginning_of_next_batch, spare_event=spare_event)
            beginning_of_current_batch = beginning_of_next_batch
            beginning_of_next_batch += params.batch_duration
            if not current_batch:
                logging.warning(" . Empty batch")
            else:
                assert current_batch[0][0] >= beginning_of_current_batch
                assert current_batch[-1][0] <= beginning_of_next_batch
            self.starttime_per_batch.append(beginning_of_current_batch)
            self.endtime_per_batch.append(beginning_of_next_batch)
            self.update_first_tree_level(current_batch)

            # Update the k-tree
            self.compute_tree_depth()
            ret = self.update_tree()
            self.loggers.time_monitoring['build_tree_update-mode'].append(ret)

            # Mine the results
            ret = self.mine_top_k_episodes()
            self.loggers.time_monitoring['mining_update-mode'].append(ret)

            # Log update time
            self.loggers.update_time.writerow([self.loggers.time_monitoring['read_data'][-1],
                self.loggers.time_monitoring['build_tree_update-mode'][-1],
                self.loggers.time_monitoring['mining_update-mode'][-1],
                self.loggers.time_monitoring['remove_old_data'][-1],
                ])

        return

    def get_events_in_batch(self, cursor, start, spare_event = None):
        """
        Get the list of the events occurring in the batch starting at `start`
        """
        end = start + self.params.batch_duration
        current_batch = []
        if spare_event:
            if spare_event[0] > end:
                return current_batch, spare_event
            assert spare_event[0] >= start
            current_batch.append(spare_event)
        spare_event = None
        row = cursor.fetchone()
        while row is not None:
            timestamp, label = row
            if timestamp > end:
                spare_event = (timestamp, label)
                break
            else:
                assert timestamp >= start
                current_batch.append((timestamp, label))
            row = cursor.fetchone()
        return current_batch, spare_event

    def update_first_tree_level(self, batch):
        """
        Read batch of data, update the first level of the tree
        """
        logging.debug(" Reading data batch #%d (%d events)" % (self.batch_id, len(batch)))
        _runtime_start = datetime.datetime.now()
        
        self.batch_id += 1;
        n_events = 0

        # While traversing the data, also get the length of the longest episode 
        # satisfying constraint t_ep.  
        # Use sl_window: sliding window of length t_ep
        sl_window = []
        max_ep_length = 0
        for timestamp, label in batch:
            # Get or create corresponding node
            if label not in self.k_tree_root.child:
                node = TreeNode(label)
                self.k_tree_root.map_to_child(node)
                self.add_neighbor(node, 0)
            else:
                node = self.k_tree_root.child[label]
            
            # Log current occurrence
            if not node.start_times or node.start_times[-1] != timestamp:
                # Otherwise: useless duplicate event
                node.log_new_occurrence(timestamp, timestamp)
                n_events += 1
        self.n_events_per_batch.append(n_events)

        # Log the details about the new batch
        self.loggers.batch_stats.writerow([self.batch_id,  
            self.n_events_per_batch[-1],  
            self.starttime_per_batch[-1], 
            self.endtime_per_batch[-1] 
            ])
        self.loggers.time_monitoring['read_data'].append(datetime.datetime.now() - _runtime_start)

        assert self.k_tree_root.n_children == len(self.k_tree_root.child)
        return 




    def build_initial_tree(self):
        """
        Initial tree construction, the first time m batches are seen, up to depth tree_depth
        """
        logging.debug(" Initial tree construction")
        StartRun = datetime.datetime.now()

        for depth in xrange(self.tree_depth-1):
            # Use nodes from levels 0..tree_depth-2 to build nodes from levels 
            # 1..tree_depth-1 => results in nodes stored in levels 
            # 0..tree_depth-1 = tree_depth levels
            assert len(self.first_node_per_depth) > depth
            node = self.first_node_per_depth[depth]
            self.first_node_per_depth.append(None) # init next level
            self.last_node_per_depth.append(None)

            # Take 2 siblings at depth depth, and merge them to create a node at depth depth+1
            while node:
                sibling = node.next_sibling
                while sibling:
                    # merge node and sibling
                    newnode = TreeNode(sibling.label)
                    n_occurrences = newnode.merge_TQ(node, sibling, self.params.t_ep, from_start=True)
                    if n_occurrences:
                        node.map_to_child(newnode)
                        self.add_neighbor(newnode, depth+1)
                    sibling = sibling.next_sibling
                node = node.neighbor
        self.loggers.tree_construction.writerow([sum(self.loggers.time_monitoring['read_data'], datetime.timedelta()), 
            self.loggers.time_monitoring['TimeCreateTree'], 
            self.loggers.time_monitoring['mining'] ]
            )
        return datetime.datetime.now() - StartRun

    def update_tree(self):
        """
        Update the tree, after a new batch of data is read
        Only the nodes at depth <= tree_depth are updated
        """
        logging.debug(" Tree update")
        StartRun = datetime.datetime.now()
        assert len(self.starttime_per_batch) == self.params.m
        assert len(self.first_node_per_depth) >= self.tree_depth
        
        # 1: find updated labels (depth == 1)
        node = self.first_node_per_depth[0]
        while node:
            assert node.lastconsider <= node.n_occurrences
            assert len(node.get_episode()) == 1
            if node.lastconsider < node.n_occurrences:
                # There were new occurrences for this node
                node.status = 'new_occurrences'
            else:
                node.status = 'no_new_occurrences'
            node = node.neighbor

        # 2: propagate changes to the other levels
        assert len(self.first_node_per_depth) >= self.tree_depth
        assert len(self.last_node_per_depth) >= self.tree_depth
        for depth in xrange(self.tree_depth-1):
            if len(self.first_node_per_depth) == depth+1:
                self.first_node_per_depth.append(None) # init next level
                self.last_node_per_depth.append(None)

            # Take 2 siblings at depth depth, and merge them. If the node 
            # (located at depth depth+1) already exists, update it, otherwise, 
            # create it.
            node = self.first_node_per_depth[depth]
            while node:
                right_sibling = node.next_sibling
                while right_sibling:
                    if right_sibling.label in node.child: 
                        assert self.first_node_per_depth[depth+1]
                        # Merged node already exists. 
                        merged_node = node.child[right_sibling.label]
                        if node.status == 'new_occurrences' or right_sibling.status == 'new_occurrences':
                            # Otherwise, nothing to do
                            n_occurrences = merged_node.merge_TQ(node, right_sibling, self.params.t_ep, from_start=False)
                            if n_occurrences:
                                merged_node.status = 'new_occurrences'
                            else:
                                merged_node.status = 'no_new_occurrences'
                        else:
                            merged_node.status = 'no_new_occurrences'
                    else:
                        # New node does not exist yet, create it
                        merged_node = TreeNode(right_sibling.label)
                        n_occurrences = merged_node.merge_TQ(node, right_sibling, self.params.t_ep, from_start=False)
                        if n_occurrences:
                            node.map_to_child(merged_node)
                            self.add_neighbor(merged_node, depth+1)
                            merged_node.status = 'new_occurrences'
                    right_sibling = right_sibling.next_sibling
                node = node.neighbor
        return datetime.datetime.now() - StartRun

    def remove_outdated_data(self, lastime):
        """
        Remove outdated data

        Parameters:
            lastime: datetime.datetime, the oldest date that remains valid. 
                Older things should be removed.
        """
        logging.debug(" Outdated data removal")
        StartRun = datetime.datetime.now()

        # 1. Empty the top-k list
        self.top_k_list = []
        self.n_episodes_in_top_k_list = 0

        # 2. Remove the nodes at depth deeper than tree_depth
        logging.debug(" . Remove the nodes at depth strictly deeper than tree_depth=%d" % (self.tree_depth))
        if len(self.first_node_per_depth) >= self.tree_depth:
            # 2a. remove references to deeper nodes from nodes at depth tree_depth
            node = self.first_node_per_depth[self.tree_depth-1]
            while node:
                node.reset_children()
                node = node.neighbor
        if len(self.first_node_per_depth) > self.tree_depth: 
            assert False
            # 2b. remove references to the nodes from the llist
            del self.first_node_per_depth[self.tree_depth:]
            del self.last_node_per_depth[self.tree_depth:]
            
        assert len(self.first_node_per_depth) == self.tree_depth
        node = self.first_node_per_depth[self.tree_depth-1]
        while node:
            assert node.n_children == 0
            node = node.neighbor

        # 3. Remove outdated occurrences for nodes at depth lower than tree_depth
        logging.debug(" . Remove outdated TQ entries from the nodes at depth lower or eq tree_depth=%d" % (self.tree_depth))
        for depth in xrange(self.tree_depth):
            # DFS traversal
            node = self.first_node_per_depth[depth]
            while node:
                assert node.label in node.parent.child
                while node.start_times and node.start_times[0] < lastime:
                    assert len(node.start_times) == len(node.end_times) == node.n_occurrences
                    node.start_times.pop(0)
                    node.end_times.pop(0)
                    node.n_occurrences -= 1
                if node.n_occurrences == 0:
                    # the last occurrence became outdated -> delete node
                    self.remove_node(node, depth)
                else:
                    node.status = "ready_for_next_batch"
                    node.lastconsider = node.n_occurrences
                node = node.neighbor

        # 4. Remove stats for the first batch in the sliding window
        self.n_events_per_batch.pop(0)
        self.starttime_per_batch.pop(0)
        self.endtime_per_batch.pop(0)

        return datetime.datetime.now() - StartRun

    def remove_node(self, node, depth):
        # Remove node from tree mapping
        node.remove()
        
        # Re-link the neighbor linked list
        prev = self.first_node_per_depth[depth]
        if prev == node:
            self.first_node_per_depth[depth] = node.neighbor
        else:
            current = prev.neighbor
            while current != node:
                assert current
                prev = current
                current = current.neighbor
            prev.neighbor = node.neighbor
            if self.last_node_per_depth[depth] == node:
                self.last_node_per_depth[depth] = prev
        
    def add_neighbor(self, node, level):
        """
        Append node at the end of the neighborhood linked list for level level
        
        Parameters:
            node: TreeNode, the node to insert in the neighborhood linked list
            level: the level in the the llist
        """
        assert len(self.first_node_per_depth) > level
        assert len(node.get_episode()) == level+1 # level=0 for len-1 episodes
        if self.first_node_per_depth[level] == None:
            self.first_node_per_depth[level] = node
            self.last_node_per_depth[level] = node
        else:
            assert not self.last_node_per_depth[level].neighbor
            self.last_node_per_depth[level].neighbor = node
            self.last_node_per_depth[level] = node
        return



    def mine_top_k_episodes(self):
        """
        Build the top-k list from the tree
        """
        logging.debug(" Episode mining")
        StartRun = datetime.datetime.now()
        
        # 1. Mine the episodes of depth <= tree_depth
        # Depth by depth list construction
        level = 0
        found_top_k = True
        while level < self.tree_depth:
            if not found_top_k:
                # No episode got in the top-k list earlier, it is pointless to continue
                logging.debug(" . . Didn't find anything interesting. Stopping here")
                break
            logging.debug(" . Mining episodes of length %d (<tree_depth)" % (level + 1))
            
            found_top_k = False
            node = self.first_node_per_depth[level]
            while node:
                assert node.n_occurrences > 0

                if node.parent.status == 'top_k':
                    newentry = ListEntry(measures=params.measures)
                    newentry.compute_significance(node, 
                            start_time=self.starttime_per_batch[0], 
                            end_time=self.endtime_per_batch[-1])
                    if self.n_episodes_in_top_k_list < params.k:
                        newentry.build_episode(node)
                        self.insert_less_than_k(newentry)
                        node.status = 'top_k'
                        found_top_k = True
                    elif newentry.significance > self.top_k_list[-1].significance:
                        newentry.build_episode(node)
                        self.insert_more_than_k(newentry)
                        node.status = 'top_k'
                        found_top_k = True
                    else:
                        node.status = 'not_top_k'
                node = node.neighbor
            level += 1

        # 2. Build and mine deeper levels of the tree
        if level != self.tree_depth:
            assert not found_top_k
        assert len(self.first_node_per_depth) == self.tree_depth
        assert self.first_node_per_depth[self.tree_depth-1] 
        horizontal_llist_head = self.first_node_per_depth[self.tree_depth-1]
        assert len(horizontal_llist_head.get_episode()) == self.tree_depth
        while found_top_k:
            logging.debug(" . Mining episodes of length %d (> tree_depth)" % (level+1))
            # found_top_k == True means that some episodes got in the top-k 
            # list at the level-1, it is worth it to continue the mining process
            found_top_k = False
            n1 = horizontal_llist_head
            horizontal_llist_head = None
            horizontal_llist_tail = None
            while n1:
                # Find an interesting node n1
                if n1.status != 'top_k':
                    n1 = n1.neighbor
                    continue
                n2 = n1.next_sibling
                while n2:
                    # Find an interesting sibling
                    if n2.status != 'top_k':
                        n2 = n2.next_sibling
                        continue

                    # Attempt merging of n1 and n2
                    newnode = TreeNode(n2.label)
                    n_occurrences = newnode.merge_TQ(n1, n2, self.params.t_ep, from_start=True)
                    
                    if n_occurrences:
                        newentry = ListEntry(measures=self.params.measures)
                        newentry.compute_significance(newnode, 
                                start_time=self.starttime_per_batch[0], 
                                end_time=self.endtime_per_batch[-1])
                        if self.n_episodes_in_top_k_list < self.params.k:
                            n1.map_to_child(newnode)
                            newentry.build_episode(newnode)
                            self.insert_less_than_k(newentry)
                            found_top_k = True
                            if not horizontal_llist_head:
                                horizontal_llist_head = newnode
                                horizontal_llist_tail = newnode
                            else:
                                horizontal_llist_tail.neighbor = newnode
                                horizontal_llist_tail = newnode
                            newnode.status == 'top_k'
                        elif newentry.significance > self.top_k_list[-1].significance:
                            n1.map_to_child(newnode)
                            newentry.build_episode(newnode)
                            self.insert_more_than_k(newentry)
                            found_top_k = True
                            if not horizontal_llist_head:
                                horizontal_llist_head = newnode
                                horizontal_llist_tail = newnode
                            else:
                                horizontal_llist_tail.neighbor = newnode
                                horizontal_llist_tail = newnode
                            newnode.status == 'top_k'
                        else:
                            # Merging did not turn out to be interesting
                            newnode.status = 'not_top_k'
                            pass
                    n2 = n2.next_sibling
                n1 = n1.neighbor
            level += 1
        else:
            logging.debug(" . . Didn't find anything interesting. Stopping here")
        
        assert self.top_k_list
        self.loggers.top_k_episodes.writerow( 
                [';'.join(map(str, entry.event)) for entry in self.top_k_list] + 
                [entry.regularity for entry in self.top_k_list] + 
                [entry.support for entry in self.top_k_list]
                )
        return datetime.datetime.now() - StartRun

    def insert_less_than_k(self, newentry):
        """
        Insert newentry in the top-k-list, assuming the list is not full yet
        """
        assert len(self.top_k_list) < k
        assert len(self.top_k_list) == self.n_episodes_in_top_k_list
        index = 0
        for entry in self.top_k_list:
            if entry.significance >= newentry.significance:
                index += 1
            else:
                self.top_k_list.insert(index, newentry)
                self.n_episodes_in_top_k_list += 1
                break
        else:
            # Reached the end of the list without hitting the break
            assert index == self.n_episodes_in_top_k_list
            self.top_k_list.append(newentry)
            self.n_episodes_in_top_k_list += 1

        # Verifications
        if index > 0:
            assert self.top_k_list[index-1].significance >= self.top_k_list[index].significance
        if index < self.n_episodes_in_top_k_list -1:
            assert self.top_k_list[index].significance >= self.top_k_list[index+1].significance
        return 

    def insert_more_than_k(self, newentry):
        """
        Insert newentry in the top-k-list, when the list is already full
        """
        assert len(self.top_k_list) == self.n_episodes_in_top_k_list
        index = 0
        for entry in self.top_k_list:
            if entry.significance >= newentry.significance:
                index += 1
            else:
                self.top_k_list.insert(index, newentry)
                break
        else:
            # Reached the end of the list without hitting the break
            assert False # Since the list is supposed to be full...
        self.top_k_list.pop()
        
        # Verifications
        if index > 0:
            assert self.top_k_list[index-1].significance >= self.top_k_list[index].significance
        if index < self.n_episodes_in_top_k_list -1:
            assert self.top_k_list[index].significance >= self.top_k_list[index+1].significance
        return


    def compute_tree_depth(self):
        """
        Min depth of the tree to store k episodes
        FIXME
        """
        accum = 0

        n_labels = self.k_tree_root.n_children
        fact_n_labels = math.factorial(n_labels)
        prev = self.tree_depth
        for i in range(1, n_labels):
            accum += fact_n_labels / (math.factorial(n_labels-i) * math.factorial(i))
            if accum > self.params.k:
                self.tree_depth = i
                break
        else:
            self.tree_depth = n_labels

        logging.debug(' Computing tree depth: %d' % self.tree_depth)
        if prev != self.tree_depth:
            logging.warning(" Tree depth changed")
        return 

class TreeNode:
    def __init__(self, label):
        self.label = label
        self.start_times = []
        self.end_times = []
        self.n_occurrences = 0

        # Navigation in the tree, updated when map_to_child is called
        self.child = {}
        self.n_children = 0
        self.first_child = None
        self.last_child = None
        self.parent = None
        self.next_sibling = None

        # Level-wise traversal, updated when the tree is built, updated, or pruned
        self.neighbor = None

        self.lastconsider = 0
        self.status = None

    def __str__(self):
        s = "%s: %d occs, %d children, status=%s" % (self.get_episode(), self.n_occurrences, self.n_children, self.status)
        return s

    def get_episode(self):
        e = []
        node = self
        while node.label != None:
            e.insert(0, node.label)
            node = node.parent
        return e

    def reset_children(self):
        """
        Remove every reference to the children of the node
        """
        self.child = {}
        self.n_children = 0
        self.first_child = None
        self.last_child = None

    def has_ancester(self, label):
        current_node = self
        while current_node:
            if current_node.label == label:
                return True
            else:
                current_node = current_node.parent
        return False

    def log_new_occurrence(self, start, end):
        """
        Register new occurrence of the current episode

        Parameters:
            start: datetime.datetime, the beginning of the occurrence
            end: datetime.datetime, the end of the occurrence
        """
        self.start_times.append(start)
        self.end_times.append(end)
        self.n_occurrences += 1

    def remove(self):
        """
        Remove node from the Tree.
        Update the mapping to its parent, as well as to its siblings
        """
        # Remove it from parent's children
        parent = self.parent
        del parent.child[self.label]
        parent.n_children -= 1

        # Re-link the siblings
        prev = parent.first_child
        if prev == self:
            parent.first_child == self.next_sibling
        else:
            current = prev.next_sibling
            while current != self:
                assert current
                prev = current
                current = current.next_sibling
            prev.next_sibling = self.next_sibling
            if parent.last_child == self:
                parent.last_child = prev
        
    def map_to_child(self, child):
        """
        Link current node to its child, and vice versa
        
        Parameters:
            child: TreeNode, the child node
        """
        assert child.label not in self.child
        self.child[child.label] = child
        if not self.first_child:
            self.first_child = child
            self.last_child = child
        else:
            self.last_child.next_sibling = child
            self.last_child = child

        child.parent = self
        self.n_children += 1
    
    def merge_TQ(self, node1, node2, t_ep=None, from_start=True):
        """
        Merge the minimal occurrences from node1 and node2 to get the minimal 
        occurrences of the current node

        Nota: not necessary, but node1 and node2 are meant to be siblings, and 
            new_node a child of node1.

        Parameters:
            node1, node2: TreeNode objects, the nodes to merge
            t_ep: datetime.timedelta, the maximal occurrence duration
            from_start: bool, optional, default=True.
                If True, start merging from the beginning of the occurrences 
                of node1 and node2. Otherwise, start from the occurrences at 
                index node1.lastconsider and node2.lastconsider, respectively.
        """
        if from_start:
            i = 0
            j = 0
        else:
            i = node1.lastconsider
            j = node2.lastconsider

        n_newoccs = 0
        while i < node1.n_occurrences and j < node2.n_occurrences:
            if node1.end_times[i] <= node2.end_times[j]:
                # b_j ends later: find greatest i such that b_j is still later
                while i+1 < node1.n_occurrences and node1.end_times[i+1] <= node2.end_times[j]:
                    i += 1
                assert node1.end_times[i] <= node2.end_times[j]

                if node1.start_times[i] < node2.start_times[j]:
                    start = node1.start_times[i]
                else:
                    start = node2.start_times[j]
                # Potential occurrence [start, b_j]
                if not t_ep or node2.end_times[j] - start <= t_ep:
                    # Short enough: new occurrence [start, b_j]
                    self.log_new_occurrence(start, node2.end_times[j])
                    n_newoccs += 1
                if node1.start_times[i] < node2.start_times[j]:
                    i += 1
                elif node1.start_times[i] == node2.start_times[j]:
                    # Both sub-occurrences start at the same time: increment both indexes
                    i += 1
                    j += 1
                else:
                    j += 1
            else:
                # a_i ends later: find greatest j such that a_i is still later
                while j+1 < node2.n_occurrences and node1.end_times[i] >= node2.end_times[j+1]:
                    j += 1
                if node1.start_times[i] < node2.start_times[j]:
                    start = node1.start_times[i]
                else:
                    start = node2.start_times[j]
                # Potential occurrence [start, a_i]
                if not t_ep or node1.end_times[i] - start <= t_ep:
                    # Short enough: new occurrence [start, a_i]
                    self.log_new_occurrence(start, node1.end_times[i])
                    n_newoccs += 1
                if node1.start_times[i] < node2.start_times[j]:
                    i += 1
                elif node1.start_times[i] == node2.start_times[j]:
                    # Both sub-occurrences start at the same time: increment both indexes
                    i += 1
                    j += 1
                else:
                    j += 1
        return n_newoccs


class ListEntry:
    def __init__(self, measures="0000010"):
        self.measures = measures
        self.event = []
        self.frequency = 0
        self.support = 0
        self.regularity = 0
        self.length = 0
        self.st_duration = 0
        self.periodicity = []
        self.significance = 0
        self.accuracy = 0
        self.coverage = 0
        self.variability = 0

    def __str__(self):
        return "%s\t%s\t%.2f%%" % (self.event, self.regularity, self.significance*100)

    def build_episode(self, node):
        """
        Go up the branch, to get the episode associated with the node
        """
        n = node
        while n.label != None:
            assert n.label not in self.event
            self.event.append(n.label)
            n = n.parent
            

    def compute_significance(self, episode_node, start_time=None, end_time=None, We=None, Wt=None, max_ep_length=None):
        """
        Provide the necessary values if you want to compute the measures!
        """
        sig = 0.0
        when = None

        self.frequency = episode_node.n_occurrences
        if self.measures[6] != '0': #frequency
            assert We
            self.significance += float(episode_node.n_occurrences) / We

        if self.measures[5] != 0: #regularity
            assert start_time
            assert end_time
            reg = episode_node.end_times[0] - start_time
            last_nomo = (episode_node.start_times[0], episode_node.end_times[0])
            self.support += 1
            when = "Debut"

            for stime, etime in itertools.izip(episode_node.start_times, episode_node.end_times):
                if stime <= last_nomo[1]:
                    # Dont compute regularity with overlapping occurrences
                    continue
                temp = etime - last_nomo[0]
                assert temp.total_seconds() > 0
                self.support += 1
                last_nomo = (stime, etime)
                if temp > reg:
                    reg = temp
                    when = "Milieu"

            temp = end_time - last_nomo[0]
            assert temp.total_seconds() >= 0
            if temp > reg:
                reg = temp
                when = "Fin"

            self.regularity = reg
            self.significance += 1.0 - float(reg.total_seconds())/(end_time-start_time).total_seconds()

        if self.measures[4] != '0': #length
            assert max_ep_length
            self.significance += float(level)/max_batch_length
            self.length = level

        if self.measures[3] != '0': #standard deviation of duration
            if level > 1:
                sum_duration = episode_node.end_times[0] - episode_node.start_times[0]
                for i in range(1, episode_node.n_occurrences):
                    sum_duration += (episode_node.end_times[i] - episode_node.start_times[i])

                mean_duration = sum_duration/episode_node.n_occurrences

                sum_st_duration = abs(((episode_node.end_times[0] - episode_node.start_times[0]) - mean_duration))**2

                for i in range(1, episode_node.n_occurrences):
                    sum_st_duration += (abs(((episode_node.end_times[0] - episode_node.start_times[0]) - mean_duration))**2)

                st_duration = math.sqrt(sum_st_duraion)

                self.significance += 1 - float(2*st_duration)/Wt
                self.st_duration = st_duration
        #....
        #if self.measures[4] != 0:
        #if self.measures[5] != 0:
        #if self.measures[6] != 0:


if __name__ == "__main__":
    import argparse
    import init_log
    
    # Command-line interface               
    parser = argparse.ArgumentParser('TKRES', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--dbname', 
            metavar = 'DATASET',
            required = True, 
            help = 'Read DATASET from mysql server')
    parser.add_argument('-t', '--tabname', default="EVENT_VIEW", 
            help="Name of the table where the events are stored. Should have a 'timestamp' and a 'label' columns")

    parser.add_argument('-v', '--verbose', 
            action = 'store_true', 
            help = "Set logging to debub mode")
    parser.add_argument('-k', default=10, type=int, help="number of desired results")
    parser.add_argument('-m', default=30, type=int, help="number of batches in the window")
    parser.add_argument('--tep', default=1800, type=int, help="maximal episode duration (in s)")
    parser.add_argument('--Wt', default=None, type=int, help="window length (in s) FIXME: probably not used")
    parser.add_argument('--We', default=None, type=int, help="window length (in #events) FIXME: probably not used")
    parser.add_argument('--indexgroup', default=2, type=int, 
            help="""
                Describes with measures should be used. Bit-wise coding: int required, transformed to bin, with these conventions:
                it contain 7 bits base on set of measures: start from the least significance bit (on the right)
                1         : means user select frequency as a measure
                10        : means user select regularity as a measure
                100       : means user select length as a measure
                1000      : means user select standard deviation of duration as a measure
                10000     : means user select accuracy of periodicity as a measure
                100000    : means user select coverage of periodicity as a measure
                1000000   : means user select variability of periodicity as a measure
                """)
    parser.add_argument("--log_directory", default="/home/julie/Documents/xED/exec_results")
    parser.add_argument("--batch_duration", type=int, default=1, help="Batch duration (in days)")

    # Read options
    options = parser.parse_args()
    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
        logging.info(" DEBUG MODE ON")
    else:
        logging.basicConfig(level=logging.INFO)
        logging.info(" info MODE ON")

    k = options.k
    m = options.m
    t_ep = datetime.timedelta(seconds=options.tep)
    batch_duration = datetime.timedelta(days = options.batch_duration)
    indexGroup = options.indexgroup

    params = Parameters(  
                t_ep=t_ep,
                batch_duration=batch_duration, 
                indexgroup=indexGroup,
                k=k,
                m=m)

    logging.info("OPTIONS: \n%s" % 
            '\n'.join(['\t%s:\t%s' % (a[0], a[1]) 
                for a in vars(options).items()]))

    # Initialize the loggers
    logpath = init_log.from_options(options, name='TKRES')
    loggers = Loggers()

    ## Stats about the batches
    loggers.add_csv_logger("batch_stats", logpath)
    loggers.batch_stats.writerow(["batch_id", "n_events", "start_time", "end_time"])

    ## Time monitoring
    time_monitoring = {'read_data':[], 'read_data_update-mode':[], 'TimeCreateTree':0, 'build_tree_update-mode':[], 'mining':0,
                   'mining_update-mode':[],'remove_old_data': []}
    loggers.add_logger("time_monitoring", time_monitoring)

    ## Tree construction time monitoring
    loggers.add_csv_logger("tree_construction", logpath)
    loggers.tree_construction.writerow(['read_data', 'TimeCreateTree', 'mining'])

    ## Tree update time monitoring
    loggers.add_csv_logger("update_time", logpath)
    loggers.update_time.writerow(['read_data', 'build_tree_update-mode', 'mining_update-mode', 'remove_old_data'])

    ## Stats about the top-k episodes
    loggers.add_csv_logger("top_k_episodes", logpath)
    loggers.top_k_episodes.writerow(["ep%d" % kk for kk in xrange(k)] + ["reg%d" % kk for kk in xrange(k)] + ["freq%d" % kk for kk in xrange(k)])

    ###############################
    # MAIN 
    ###############################
    db_cursor = DB_Utils.db_connection(options.dbname)
    tkres_miner = TKRES_miner(params, loggers, db_cursor, options.tabname)
    tkres_miner.main(db_cursor, options.tabname)

