#import yaml
import random
import yaml

class MixtureModelGenerator:
    def __init__(self, characteristics):
        """
        Framework for the generation of real-valued data, following a mixture 
        of gaussian distribution, evolving over time with controlled 
        charcteristics.

        Parameters:
            characteristics: a list of component descriptions. Each component 
                is seen as a gaussian distribution whose characteristics 
                evolves linearly over a time interval. Each description is a 
                dict with the keys:
                - id: Its value is just informational, and is meant to help the 
                identification of components whose behaviour changes over time 
                (and that tus defined piecewise).
                - from: The description of the component at the beginnng of the 
                considered time interval
                - to: Idem, but at the end of the interval.
                from and to link themselves to dicts, having 4 keys: 
                    - date: int, the beginning/end bounds of the time interval 
                    - mean: float, the mean of the gaussian distribution
                    - stddev: float, the stddev of the gaussian distribution
                    - weight: float, the "number" of samples to generate from 
                    the description. See the doc for the generate_data function 
                    for more detail on how the samples are generated.
        """
        self.characteristics = characteristics

    def generate_data(self):
        """
        Generates data points.

        The total ~date~ interval covered by the descriptions is traversed. At 
        each time, the active components are recovered. The mean, stddev and 
        weight of the component evolving uniformly over the covered time 
        interval, the true (current) value of the component is computed. 
        Corresponding samples are generated, thanks to calls to 
        random.gauss(mean, stddev). The number of calls is handled by weight.
        The weight is decomposed into two: its integer and decimal parts. At 
        least integer(weight) samples are generated. A random number p in 
        [0.0, 1.0) is selected (uniform distribution), and if 
        p < decimal(weight), another sample is generated. 
        """

        # Beginning/End of the generation
        beginning = None
        end = None
        slopes = []

        for comp in self.characteristics:
            print comp
            fdate = comp['from']['date']
            tdate = comp['to']['date']
            if beginning == None or fdate < beginning:
                beginning = fdate
            if end == None or tdate > end:
                end = tdate

            sl = {}
            for param in ['mean', 'stddev', 'weight']:
                initial_value = comp['from'][param]
                final_value   = comp['to'][param]
                sl[param] = (final_value-initial_value)/(tdate-fdate)
            slopes.append(sl)


        print slopes
        t = beginning
        count = 0
        while t < end:
            print '\n', t
            for comp in xrange(len(self.characteristics)):
                fdate = self.characteristics[comp]['from']['date']
                tdate = self.characteristics[comp]['to']['date']
                if t > tdate or t < fdate:
                    print "comp", self.characteristics[comp]['id'], "inactive"
                    # Comp inactive
                    continue

                # Comp is active
                mean = (self.characteristics[comp]['from']['mean'] + 
                        (t-fdate)*slopes[comp]['mean'])
                stddev = (self.characteristics[comp]['from']['stddev'] + 
                        (t-fdate)*slopes[comp]['stddev'])
                weight = (self.characteristics[comp]['from']['weight'] + 
                        (t-fdate)*slopes[comp]['weight'])
                print "Generating with:", mean, stddev, weight

                if random.random() < weight:
                    yield t, random.gauss(mean, stddev), self.characteristics[comp]['id']
                    count += 1
                else:
                    print "didn't work"

            t += 1

    def generate_to_csvfile(self, destination):
        """
        Generate data and store them to a csv file. 
        """
        with open(destination, 'w') as dest:
            values = []
            writer = csv.writer(dest)
            for value in self.generate_data():
                print value
                writer.writerow(value)
                values.append(value)
            return values


    def plot_values(self, figure, values):
        ts = [a[0] for a in values]
        vs = [a[1] for a in values]
        figure.plot(ts, vs, 'b+')

    def plot_ground_truth_to_existing_figure(self, figure, cmap='jet'):
        comps = {}
        # dict, indexed on the components id, and referencing 3 lists: one with timestamps, one with means, one with stddevs
        # Item order matters.

        for comp in self.characteristics:
            comp_id = comp['id']
            if comp_id not in comps:
                comps[comp_id] = [[], [], []]

            comps[comp_id][0].extend([comp['from']['date'], comp['to']['date']])
            comps[comp_id][1].extend([comp['from']['mean'], comp['to']['mean']])
            comps[comp_id][2].extend([comp['from']['stddev'], comp['to']['stddev']])

        ncomps = len(comps)
        
        import matplotlib.cm as cm
        colormap = cm.get_cmap(name=cmap)
        for comp_id in comps:
            color = colormap(random.random())
            print comps[comp_id]
            t, m, s = comps[comp_id]
            
            # lower bound
            lbound = map(lambda x, y: x-y, m, s)
            # upper bound
            ubound =  map(lambda x, y: x+y, m, s)
            figure.fill_between(t, lbound, ubound, color=color, alpha=.4)
            figure.plot(t, m, color=color, label=comp)


if __name__ == '__main__':
    format = '''
    python file, containing a list of associative arrays. Each associative array 
    represents a component between two timestamps, and is composed of 3 
    elements: 'id', 'from' and 'to'. 
        'id': number / string / whatever identifying the component. One 
            component having a different behaviour when time evolves can be 
            specified using this id. 
        'from': describes the initial characteristics of the component, it has 
            4 features:
            'date': int, the ref date, when the characteristics are initialized
            'mean': float, the mean of the gaussian components
            'stddev': float, the standard deviation of the component
            'weight': float, the amount of generated samples: a component 
                sampling, going from 'from.date'=0 to 'to.date'=10 with 
                'weight'=1 will have 10 samples. The same, with 'weight'=2, 
                will have 20 samples.
        'to': describes the final characteristics of the component, after the 
            shift that started at 'from.date'. It has the same 4 features. The 
            shift of the component from 'from' to 'to' is linear.

    The elements are not actually timestamped. The 'weight''s only purpose is 
    to weight the components relatively to one another.
    '''

    import argparse
    import pickle
    import csv
    parser = argparse.ArgumentParser(
            description="Build a dataset from a parameter file")
    parser.add_argument(
            '-p', '--paramfile',
            required=True,
            help="File containing the description of the mixture model to " +
            "be generated. %s" % format
            )
    parser.add_argument('-d', '--destination', required=True, 
            help="Writable file, where the generated data is stored (CSV)"
            )
    parser.add_argument('--plot_gt', action='store_true',
            help="Draw a figure with the ground truth characteristics"
            )

    args = parser.parse_args()
    with open(args.paramfile, 'r') as f:
        mm_char = pickle.load(f)
        mmg = MixtureModelGenerator(mm_char)
        values = mmg.generate_to_csvfile(args.destination)

        if args.plot_gt:

            import matplotlib.pyplot as plt
            plt.ion()
            fig, ax = plt.subplots(1, 1)

            mmg.plot_ground_truth_to_existing_figure(ax, cmap='jet')
            mmg.plot_values(ax, values)
            ax.figure.canvas.draw()

            raw_input("Press ENTER to quit")
