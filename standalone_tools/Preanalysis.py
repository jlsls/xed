import argparse
import MySQLdb
import datetime
from DB_Utils import db_connection, list_event_types

def tikz_support_hist(dataset):
	cursor = db_connection(dataset)
	s = support_per_event_type(cursor)
	print generate_tikz(s, dataset)

def support_per_event_type(cursor):
	cursor.execute(	""" SELECT sensor_id, name, count(timestamp) AS support 
						FROM EVENT JOIN SENSOR ON sensor_id = id
						GROUP BY sensor_id
						ORDER BY support DESC
					""" )
	sensor_support = []
	for sensor_id, name, support in cursor:
		if " end" in name:
			continue
		else:
			name = name[:-6]
			sensor_support.append((name, support))
	return sensor_support

def generate_tikz(sensor_support, dataset):
	nitems = len(sensor_support)
	s = """
%%%
%%% HISTOGRAMME DES SUPPORTS DES DIFFERENTS EVENT LABELS DANS """ + dataset + """
%%% Ajustez manuellement les yticks, et les x et y scales
%%% """
	s += """
\\begin{tikzpicture}

\\begin{axis}[
        x = 5.5mm,						% <---- xscale 
        y = 1cm/43,						% <---- yscale 
%        ylabel = {\# occurrences},
        ymajorgrids,
        major grid style={dashed,red},
        xticklabels={
		"""
	s += ', '.join([i[0] for i in sensor_support])
	s += """},
        xtick={1,...,
		"""
	s += str(nitems)
	s += """},
        x tick label style={rotate=30, anchor=east, align=center},
        ytick={0, 50, 100},				% <---- yticks 
    ]
    \\addplot[ybar,draw=blue,fill=blue!25, bar width=12] coordinates {
		"""
	s += '\n\t\t'.join(["(%d, %d)" % (i+1, sensor_support[i][1]) for i in xrange(nitems)])
	s += """
    };
\end{axis}

\end{tikzpicture}
""" 
	return s

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--dataset', help="SQL database name", required=True)

options = parser.parse_args()

tikz_support_hist(options.dataset)





