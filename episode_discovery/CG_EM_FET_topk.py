import sys
import logging 

sys.path.append('/home/julie/Documents/myed/branches')
import general_tools
import general_tools.dataset
import general_tools.dataset.parser
from general_tools.dataset import Database, Event, Episode

class EpisodeMatrix:
    def __init__ (self, labels):
        self.matrix = {}
        self.labels = labels
        self.last_instance = {} # Stores the timestamp of the last seen instance for each label
        self.label_count = {}
        self.single_events = {} # Indexed on the label, each value being itself a dict, with 'TQ', 'count' keys

        # Build empty matrix
        ## Create the rows
        for label in labels:
            self[label] = {}
            self.label_count[label] = 0
            self.single_events[label] = {'TQ': [], 'count': 0}

        ## Create the columns
        for label1 in labels:
            for label2 in labels:
                if label1 == label2:
                    pass
                else:
                    # Hard wire the 2 cells [i, j] and [j, i] to enforce the symetrical property
                    self.set_cell(label1, label2)

    def assert_symetrical(self):
        """
        Checks if the matrix is symetrical, and if its diagonal is empty
        """
        for l1 in self.labels:
            for l2 in self.labels:
                if l1 == l2:
                    if l2 in self[l1] or l1 in self[l2]:
                        print "Item on the diagonal..."
                        return False
                else:
                    if self[l1][l2] != self[l2][l1]:
                        print "Not symetrical..."
                        return False
        return True
        

    def __getitem__(self, item):
        return self.matrix[item]

    def __setitem__(self, item, value):
        self.matrix[item] = value

    def set_cell(self, x, y):
        """
        The matrix should be symetrical: the 2 sym. cells point to the same value
        """
        value = {'TQ': [], 'count': 0, 'last_label': None}
        self.matrix[x][y] = value
        self.matrix[y][x] = value

    def add_event(self, event):
        """
        Add event to the matrix: update the corresponding 2-episodes
        """
        logging.debug("Inserting event %s" % event)
        label = event.label
        timestamp = event.timestamp

        assert label in self.matrix
        # 2-ep update
        for label2 in self[label]:
            cell = self[label][label2]
            if cell['last_label']:
                # The cell is unlocked (last_label is set): 
                # An occurrence of either 2 labels was seen. Try to finish the 2-ep

                if cell['last_label'] == label:
                    # Same label... The 2-ep is not seen, but it's still opened
                    pass
                else:
                    # TODO: duration constraint
                    
                    # Different labels: the episode is fulfilled
                    cell['TQ'].append((self.last_instance[label2], timestamp))
                    cell['count'] += 1
                    cell['last_label'] = None # Locks the cell

            else:
                # The cell is locked: no occurrence of the 2-ep started yet. Start it with label
                cell['last_label'] = label

        # Update last_instance for label
        self.last_instance[label] = timestamp
        self.label_count[label] += 1
        self.single_events[label]['count'] += 1
        self.single_events[label]['TQ'].append((timestamp, timestamp))

    def remove_event(self, event):
        """
        Update the matrix by removing the occurrences linked with event
        """

        label = event.label
        timestamp = event.timestamp
        assert label in self

        for label2 in self[label]:
            cell = self[label][label2]
            if cell['TQ'][0][0] == timestamp:
                # event participated in an occurrence of this cell. It needs to be removed.
                cell['TQ'].pop(0)
                cell['count'] -= 1

                if cell['count'] == 0:
                    # It was the last event seen for this 2-ep: if the cell is unlocked, it needs to be locked
                    cell['last_label'] = None
        
        if self.last_instance[label] == timestamp:
            del self.last_instance[label]

        self.label_count[label] -= 1


class EpisodeTree:
    def __init__(self, episode_length=1800, minimal_frequency=5, maximal_depth=None):
        self.root = TreeNode(label="ROOT", parent=None)
        self.episode_length = episode_length
        self.minimal_frequency = minimal_frequency
        self.maximal_depth = maximal_depth

    def build(self, matrix):
        self.item_order = [item for item in matrix.label_count 
                if matrix.label_count[item] > self.minimal_frequency]
        self.item_order.sort(cmp=lambda x, y: cmp(matrix.label_count[y], matrix.label_count[x]))

        for item in self.item_order:
            node = TreeNode(item, self.root)
            node.count = matrix.single_events[item]['count']
            node.TQ = matrix.single_events[item]['TQ']
            self.root.add_child(node)
            if item != self.item_order[-1]:
                self.generate_children(node, matrix)

    def generate_children(self, node, matrix, depth=1):
        label = node.label

        for item in self.item_order[self.item_order.index(label)+1:]:
            TQ1 = node.TQ
            TQ2 = matrix[label][item]['TQ']
            
            TQ, count = self.merge_TQs(TQ1, TQ2)

            if count >= self.minimal_frequency:
                child_node = TreeNode(item, node)
                child_node.TQ = TQ
                child_node.count = count
                node.add_child(child_node)

                if item != self.item_order[-1] and (not self.maximal_depth or depth < self.maximal_depth):
                    self.generate_children(child_node, matrix, depth = depth+1)
            

    def merge_TQs(self, TQ1, TQ2):
        """
        Case 1:
            1    2
           ---  ---
        => -------- total
        Case 2:
           --- 1
             --- 2
        => ----- total
        Case 3:
           ------ 1
             --- 2
        => ------ total
        """
        last_end = 0
        i, j = 0, 0
        L1, L2 = len(TQ1), len(TQ2)

        TQ = []
        count = 0

        while (i<L1 and j<L2):
            if TQ1[i][1] >= TQ2[j][1]:
                # TQ1[i] finishes after TQ2[j]
                while (j < L2 and TQ2[j][1] <= TQ1[i][1]):
                    j += 1
                else:
                    if j == L2:
                        pass
                    else: 
                        assert TQ2[j][1] > TQ1[i][1]
                        if TQ1[i][0] <= TQ2[j-1][0]:
                            if TQ1[i][0]<=last_end:
                                # Overlapping...
                                i += 1
                                continue
                            else:
                                # Non overlapping!
                                if not self.episode_length or TQ1[i][1] - TQ1[i][0] <= self.episode_length:
                                    # Max length: check!
                                    # => NMO
                                    TQ.append((TQ1[i][0], TQ1[i][1]))
                                    count += 1
                                    last_end = TQ1[i][1]
                                    i += 1
                                    # Rq: j already incremented
                                else:
                                    # Too long
                                    # Not an occurrence
                                    # Inc the one starting earliest
                                    i += 1
                                    j -= 1
                        else:
                            #TQ2[j]-1 starts first
                            if TQ2[j-1][0]<=last_end:
                                # Overlapping...
                                # j already inc
                                continue
                            else:
                                # Non overlapping!
                                if not self.episode_length or TQ2[j-1][1] - TQ1[i][0] <= self.episode_length:
                                    # Max length: check!
                                    # => NMO
                                    TQ.append((TQ2[j-1][0], TQ1[i][1]))
                                    count += 1
                                    last_end = TQ1[i][1]
                                    i += 1
                                    # Rq: j already incremented
                                else:
                                    # Too long
                                    # Not an occurrence
                                    # Inc the one starting earliest
                                    i += 1
                                    j -= 1

            else:
                # TQ2[j] finishes last
                while (i < L1 and TQ1[i][1] <= TQ2[j][1]):
                    i += 1
                else:
                    if i == L1:
                        pass
                    else: 
                        assert TQ1[i][1] > TQ2[j][1]
                        if TQ2[j][0] <= TQ1[i-1][0]:
                            if TQ2[j][0]<=last_end:
                                # Overlapping...
                                j += 1
                                continue
                            else:
                                # Non overlapping!
                                if not self.episode_length or TQ2[j][1] - TQ2[j][0] <= self.episode_length:
                                    # Max length: check!
                                    # => NMO
                                    TQ.append((TQ2[j][0], TQ2[j][1]))
                                    count += 1
                                    last_end = TQ2[j][1]
                                    j += 1
                                    # Rq: i already incremented
                                else:
                                    # Too long
                                    # Not an occurrence
                                    # Inc the one starting earliest
                                    j += 1
                                    i -= 1
                        else:
                            #TQ1[i]-1 starts first
                            if TQ1[i-1][0]<=last_end:
                                # Overlapping...
                                # i already inc
                                continue
                            else:
                                # Non overlapping!
                                if not self.episode_length or TQ1[i-1][1] - TQ2[j][0] <= self.episode_length:
                                    # Max length: check!
                                    # => NMO
                                    TQ.append((TQ1[i-1][0], TQ2[j][1]))
                                    count += 1
                                    last_end = TQ2[j][1]
                                    j += 1
                                    # Rq: i already incremented
                                else:
                                    # Too long
                                    # Not an occurrence
                                    # Inc the one starting earliest
                                    j += 1
                                    i -= 1

        return TQ, count


            


class TreeNode:
    def __init__(self, label="", parent=None):
        self.label = label
        self.parent = parent
        self.children = {}
        self.TQ = []
        self.count = 0

    def add_child(self, node):
        self.children[node.label] = node
        return 
        
if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()

    # DATA FILE
    parser.add_argument('-f', '--filename', 
            metavar = 'DATASET',
            required = True, 
            help = 'Read DATASET dump file')

    parser.add_argument('-v', '--verbose', 
            action = 'store_true', 
            help = "Set logging to debub mode")

    ## READ THE PARAMETERS
    options = parser.parse_args()

    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    logging.info("OPTIONS: \n%s" % 
            '\n'.join(['\t%s:\t%s' % (a[0], a[1]) 
                for a in vars(options).items()]))

    from general_tools.dataset.parser import read_dataset
    with open(options.filename, 'r') as f:
        dataset = read_dataset(f)

    logging.debug("Labels: \n\t" + "\n\t".join(dataset.labels))
    matrix = EpisodeMatrix(dataset.labels)

    i = 0
    for event in dataset.events:
        i += 1
        if (i%10000) == 0:
            print "%d events" % i
        matrix.add_event(event)

    matrix.assert_symetrical()
    logging.info(" Matrix built")

    episode_length = 1800
    minimal_support = 4
    maximal_depth = None
    et = EpisodeTree(episode_length, minimal_support, maximal_depth)
    et.build(matrix)




