import MySQLdb
import datetime
import logging

def db_connection(dataset, host="localhost", user="julie", passwd="julie"):
    """
    Connect to the database with the provided identifiers, using MySQLbd

    Parameters:
    -----------
        dataset:    the name of the database
        host:       MySQL server host (optional, default: 'localhost')
        user:       MySQL user name. Needs to have at least read access on the 
            database (optional, default: 'julie')
        passwd:     the password for the user (optional, default: 'julie')

    Return:
    -------
        cursor
    """
    database = MySQLdb.connect(host="localhost",
            user="julie", 
            passwd="julie",
            db=dataset)
    cursor = database.cursor()
    logging.debug(" Database connection OK")
    return cursor

def list_event_types(cursor):
    """
    Get in a dictionary the mapping between event ids and the corresponding 
    labels

    Parameters: 
    -----------
        cursor: a valid MySQLdb connection

    Return:
    -------
        sensors: dictionary indexed on sensor_id -> sensor_label
    """
    cursor.execute("SELECT id, name FROM SENSOR")
    sensors = {}
    for id, name in cursor:
        sensors[id] = name
    
    return sensors

def get_events_in_cursor(cursor, tabname="EVENT_VIEW"):
    """
    Execute a SELECT timestamp, label query. 

    This function is meant to be used to get events. 

    Parameters:
    -----------
        cursor: a valid MySQLdb connection
        tabname: the name of the table where the events are stored
    """
    cursor.execute( """ SELECT timestamp, label
                        FROM %s
                        ORDER BY timestamp
                    """ % tabname)
    
def beginning(cursor, table="EVENT_VIEW"):
    """
    Get 1st timestamp in the dataset
    """
    query = "SELECT timestamp FROM %s ORDER BY timestamp LIMIT 1" % table
    cursor.execute(query)
    return cursor.fetchone()[0] 

def end(cursor, table="EVENT_VIEW"):
    """
    Get last timestamp in the dataset
    """
    query = "SELECT timestamp FROM %s ORDER BY timestamp DESC LIMIT 1" % table
    cursor.execute(query)
    return cursor.fetchone()[0] 

def duration(cursor, table="EVENT_VIEW"):
    """
    Get the duration of the dataset
    """
    return end(cursor, table) - beginning(cursor, table)

def length(cursor, table="EVENT_VIEW"):
    """
    Get the length of the dataset (# of events)
    """
    query = "SELECT count(*) FROM %s" % table
    cursor.execute(query)
    return cursor.fetchone()[0]

def labels(cursor, table="EVENT_VIEW"):
    """
    Get the list of the labels in the dataset, and the number of labels
    """
    query = "SELECT DISTINCT label from %s" % table
    cursor.execute(query)
    labels = []
    nlabels = 0
    for label, in cursor:
        labels.append(label)
        nlabels += 1
    return labels, nlabels

def nevents_per_label(label, cursor, table):
    """"
    Return the number of events having the given label
    """
    query = "SELECT COUNT(timestamp) FROM %s WHERE label=%%s" % table
    cursor.execute(query, label)
    return cursor.fetchone()[0]

def gaps_between_occurrences(label, cursor, table="EVENT_VIEW"):
    """
    Return the gaps between the consecutive occurrences for a given label
    """
    query = "SELECT timestamp FROM %s WHERE label=%%s ORDER BY timestamp" % table
    cursor.execute(query, label)
    prev = None
    gaps = []
    for ts in cursor:
        ts = ts[0]
        if prev:
            gaps.append(ts-prev)
            prev = ts
        else:
            prev = ts
    return gaps
