# -*- coding: utf-8 -*-

from dataset import Episode

class PeriodicEpisode(Episode):
    """
    A PeriodicEpisode object describes the (quite) regular
    occurrences of a given episode. The difference to perfect matching
    is also characterized.
    """
    def __init__(self, contents, periodicity, pat_qual=None):
        Episode.__init__(self, contents)
        self.periodicity = periodicity
        self.pattern_quality = pat_qual

    def __str__(self):
        s = str(self.id) + ": " \
                + str(self.periodicity) 
        return s

    def __eq__(self, per_epi):
        eq = Episode.__eq__(self, per_epi) \
                and self.periodicity == per_epi.periodicity
        return eq

    def merge(self, pe, end_of_pattern):
        """
        A merging can be done if the current periodic episode and `pe`
        have the same id, and if their periodicity have the same
        period. Their respective start and end times should be separed
        by less than `end_of_pattern` periods.

        Returns the periodic episode merging the 2 periodicity
        descriptions, and shifted
        """

        p1 = self.periodicity.period
        p2 = pe.periodicity.period
        s1 = self.periodicity.start
        s2 = pe.periodicity.start
        e1 = self.periodicity.end
        e2 = pe.periodicity.end

        if Episode.__eq__(self, pe) and\
                p1 == p2 and\
                abs(s1 - s2) < end_of_pattern * p1 and\
                abs(e1 - e2) < end_of_pattern * p1: 
            logging.info(" . merging episodes " + str(pe) + " and " +\
                    str(self))
            description = [i for i in self.periodicity.description]
            description.extend(
                    [i for i in pe.periodicity.description])
            description.sort(cmp=lambda x, y: cmp(x[0], y[0]))
            periodicity = Periodicity(self.periodicity.period,
                    tuple(description),
                    min(s1, s2),
                    max(e1, e2)) 
            perepi = PeriodicEpisode(self.contents, 
                    periodicity)

            return perepi
        else:
            return None

    def difference(self, pe, end_of_pattern):
        """
        Returns: 
        A new PeriodicEpisode instance if it makes sense to substact 
        `pe` from `self` (None otherwise). That is if `pe` is the 
        negation of `self`, and they occur in the same time range.
        The new period is the lowest common multiple of the 2
        arguments periods.

        Warning: 
        The `erratum` attribute is NOT updated (though it is created).
        This is because of encapsulation matters: this doesn't have
        access to the occurrence times of the episodes.

        Arguments:
        pe -- The PeriodicEvent to try and substract from the current
        event
        end_of_pattern -- A parameter defining what "occur in the same
        range" mean: the 2 `start` (resp. `end`) attributes of the
        PeriodicEvent s must be separated by less than
        `end_of_pattern` times the period
        """
        p1 = self.periodicity.period
        p2 = pe.periodicity.period
        s1 = self.periodicity.start
        s2 = pe.periodicity.start
        e1 = self.periodicity.end
        e2 = pe.periodicity.end
        p = lcm(p1, p2)
        if p1 < p2:
            smallest = self
            biggest = pe
        else:
            smallest = pe
            biggest = self

        negation = '!' + str(smallest.id) + ' ('
        for c in smallest.contents:
            negation += '-' + c + ' '
        negation = negation[:-1]
        negation += ')'
        
        if abs(s1 - s2) < end_of_pattern * p and\
            abs(e1 - e2) < end_of_pattern * p and\
            biggest.contents == frozenset([negation]): 
                logging.info(" . building the difference episode of "\
                        + str(self) + " \\ " + str(pe))
                smallest_per = smallest.periodicity.extend(p)
                biggest_per = biggest.periodicity.extend(p)
                periodicity = smallest_per.difference(biggest_per,
                        end_of_pattern) 
                return PeriodicEpisode(smallest.id,
                        smallest.contents, periodicity,
                        )

        return None


    def merge_episode(self, pe, end_of_pattern=0, ep_length=10):
        p1, p2, p3 = self.periodicity.merge(pe.periodicity, end_of_pattern, ep_length)
#        print "(periodicity) p1, p2, p3: " +str((p1, p2, p3))
        if p3:
            return (PeriodicEpisode(self.contents, p1), 
                    PeriodicEpisode(pe.contents, p2),
                    PeriodicEpisode(self.contents.union(pe.contents), p3))
        else:
            return (deepcopy(self), deepcopy(pe), None)
