#!/usr/local/bin/python
# -*- coding: utf-8 -*-

class Transaction_:
    def __init__(self, valid_subset):
        self.items = set([e.content for e in valid_subset])
        self.valid_subset = valid_subset
    
    def __iter__(self):
        return iter(self.items)
    def __len__(self):
        return len(self.items)
    
    def __str__(self):
        s = "Transaction_: "
        for i in self.items:
            s += "  " +str(i)
        return s
    def __repr__(self):
        return str(self)

    def remove(self, item):
        self.items.remove(item)

    def issubset(self, transaction):
        return self.items.issubset(transaction.items)


class Itemset:
    def __init__(self, items):
        self.items = frozenset(items)

    def __iter__(self):
        return iter(self.items)

    def __str__(self):
        s = "Itemset: "
        for i in self.items:
            s += "  " +str(i)
        return s
    def __repr__(self):
        return str(self)

    # COMPARISON TOOLS
    def __eq__(self, itemset):
        return self.items == itemset.items
    def __hash__(self):
        return hash(self.items)

    # SET OPERATIONS
    def issubset(self, itemset):
        return self.items.issubset(itemset.items)
    def union(self, itemset, transactions, useful_transactions, occurrences):
        """Returns a new itemset"""
        result = frozenset(itemset.items.union(self.items))
        new_itemset = Itemset(result)
        return new_itemset
    def isdisjoint(self, itemset):
        return self.items.isdisjoint(itemset.items)

        
EMPTY_ITEMSET = Itemset(set())

class CandidateGenerator_APriori(CandidateGenerator):

    def generate_candidates(self):
        with open(self.logfile, 'a') as f:
            f.write("\tapriori search for itemsets: \n" )
        t = time()
        self.valid_subsets = self.find_valid_subsets()
        self.transactions = self.prepare_transactions()
        with open(self.logfile, 'a') as f:
            f.write("\t\tvalid subsets + transactions preparation: %.2fs\n" % (time()-t))
        
        self.occurrences_ = {}
        self.useful_transactions = {}
       
        self.frequent_items = set()
        self.frequent_itemsets = set()

        t1 = time()
        itemsets = self.a_priori()
        with open(self.logfile, 'a') as f:
            f.write("\t\tapriori search for itemsets: %.2fs\n" % (time()-t1))

        t1 = time()
        candidates = set()
        for itemset in itemsets:
            episode = Episode(itemset)
            candidates.add(episode)
            self.occurrences[episode] = filter_occurrences([Occurrence(episode, l) for l in self.occurrences_[itemset]])
        with open(self.logfile, 'a') as f:
            f.write("\t\tcreation of the corresponding episodes, and listing of their occurrences: %.2fs\n" % (time()-t1))

        name = "candidates_round_%d.txt" % cfg.COUNTER_iteration
        fname = '/'.join([self.storing_prefix,\
                'candidates', name])
        with open(fname, 'w') as f:
            f.write("# CANDIDATES FOR ROUND %d\n\n%s" % \
                    (cfg.COUNTER_iteration, str_list(candidates)))
        with open(self.logfile, 'a') as f:
            f.write("\ttotal execution time: %.2fs\n" % (time()-t))
            f.write("\t%d candidates\n" % len(candidates))
        return candidates


    def a_priori(self):
        """
        The complete algorithm
        """

        if len(self.transactions) == 0:
            return set()
        else:
            self.find_frequent_items()
            self.build_frequent_itemsets()
            return self.frequent_itemsets

    def find_frequent_items(self):
        logging.info(" . Looking for frequent items")
        ti = clock()
        for t in self.transactions:
            for item in t:
                itemset = Itemset([item])
                if itemset in self.occurrences_:
                    pass
                else:
                    self.store_it_all(itemset)
                    assert itemset in self.occurrences_
                    if len(self.occurrences_[itemset])>=self.minimal_support:
                        self.frequent_items.add(itemset)
                        logging.debug(" . . %s: %d" % (str(itemset), 
                                len(self.occurrences_[itemset])))

        logging.debug(" . . Done. Took %ds to find %d frequent items" \
                % (clock()-ti, len(self.frequent_items)))


    def build_frequent_itemsets(self):
        # Build itemsets
        logging.info('Building the frequent itemsets...')
        lastly_added = set([EMPTY_ITEMSET]) 
        if self.maximal_episode_length:
            maximal_episode_length = min(
                    [self.maximal_episode_length, max(map(len, self.transactions))])
        else:
            maximal_episode_length = max(map(len, self.transactions))

        for size in range(1, maximal_episode_length+1):
            logging.debug(' ... of size ' + str(size))
            added_last_round = copy(lastly_added)
            prod = product(added_last_round, self.frequent_items)
            lastly_added = set()
            
            for j in self.frequent_items:
                for i in added_last_round:
                    itemset = i.union(j, 
                            self.transactions, 
                            self.useful_transactions, 
                            self.occurrences_)
                    self.store_it_all(itemset)
                    supp = len(self.occurrences_[itemset])
                    if supp < self.minimal_support \
                            or itemset in self.frequent_itemsets:
                        pass
                    else:
                        lastly_added.add(itemset)
                        self.frequent_itemsets.add(itemset)
            logging.debug( ' ... ... there are ' +str(len(lastly_added))+ \
                    " such itemsets")
        logging.debug( ' ... done')
        logging.info("There is a total of " +str(len(self.frequent_itemsets))+ \
                " frequent itemsets")
        return self.frequent_itemsets



    def prepare_transactions(self):
        t = clock()
        mk_transaction = lambda x: Transaction_(set(x))
        logging.info(" . Building the transactions...")
        transactions = map(mk_transaction, self.valid_subsets[:])
        logging.debug(" . . Done. Took %ds to make %d transactions" % \
                (clock()-t, len(transactions)))

        return transactions

    def store_it_all(self, itemset):
        """
        !!! Modifies the lists and dictionaries in input 
        (updates them for current itemset)
        """
        if itemset in self.useful_transactions:
            pass
        else:
            trans = []
            for t in self.transactions: 
                if all(map(lambda x: x in t, itemset.items)):
                    trans.append(t)
            self.useful_transactions[itemset] = trans
        
        
        assert all(map(
                lambda x: x.__class__.__name__ == 'Transaction_', 
                self.useful_transactions[itemset]))
        if itemset in self.occurrences_:
            pass
        else:
            self.occurrences_[itemset] = set()
            for t in self.useful_transactions[itemset]:
                valid_subsets = []
                for c in itemset:
                    valid_subsets.append([e for e in t.valid_subset \
                            if e.content == c])
                # Restreint aux seuls éléments qui m'intéressent
                self.occurrences_[itemset].update(
                        [frozenset(i) for i in product(*valid_subsets)]
                        )


if __name__ == "__main__":
    parser = Parser()
    parser.cand_generation()

    parser.add_argument('-d', '--database',
            help= "DATABASE is the pickle dump file of the database",
            required=True)
    parser.add_argument('-p', '--storing_prefix',
            help = "The destination where to store the log files",
            default = '.')

    arguments = parser.parse_args()

    kwargs = {}
    if arguments.minimal_support:
        kwargs["minimal_support"] = arguments.minimal_support
    if arguments.maximal_episode_duration:
        kwargs["maximal_episode_duration"] = arguments.maximal_episode_duration
    if arguments.maximal_episode_length:
        kwargs["maximal_episode_length"] = arguments.maximal_episode_length
    
    import os
    kwargs["storing_prefix"] = arguments.storing_prefix
    if os.access(arguments.storing_prefix, os.F_OK):
        pass
    else:
        os.makedirs(self.params.storing_prefix)
    if os.access('/'.join([arguments.storing_prefix, 'candidates']), os.F_OK):
        pass
    else:
        os.makedirs('/'.join([arguments.storing_prefix, 'candidates']))
    
    t = time()
    logging.info("Loading the dataset...")
    with open(arguments.database, 'r') as f:
        database = load(f)
    logging.debug(" . Done. Took %.2f" % (time() - t))

    t = time()
    logging.info("Generating the candidates...")
    cg = CandidateGenerator_APriori(database, **kwargs)
    candidates = cg.generate_candidates()
    logging.debug(" . Done. Took %.2f" % (time() - t))

    print candidates
    print len(candidates)

