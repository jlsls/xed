import numpy
import datetime
import csv
import itertools

import DB_Utils

def general_stats(cursor, table="EVENT_VIEW"):
    print "Beginning: %s" % DB_Utils.beginning(cursor, table)
    print "End: %s" % DB_Utils.end(cursor, table)
    print "Duration: %s" % DB_Utils.duration(cursor, table)
    print "# of events: %s" % DB_Utils.length(cursor, table)
    l, nl = DB_Utils.labels(cursor, table)
    print "# of labels: %s" % nl
    print "Labels:"
    for label in ["label", "nevents", "mingap", "maxgap", "meangap", "stdgap"]:
        print "{0:^30}".format(label),
    print 
    for label in l:
        nevents = DB_Utils.nevents_per_label(label, cursor, table)
        gaps = map(lambda x: x.total_seconds(), DB_Utils.gaps_between_occurrences(label, cursor, table))
        if gaps:
            mingap  = datetime.timedelta(seconds=min(gaps))
            maxgap  = datetime.timedelta(seconds=max(gaps))
            mean    = datetime.timedelta(seconds=numpy.mean(gaps))
            stddev  = datetime.timedelta(seconds=numpy.std(gaps))
        else:
            mingap = "/"
            maxgap = "/"
            mean = "/"
            stddev = "/"
        for data in (label, nevents, mingap, maxgap, mean, stddev):
            print "{0:^30}".format(data),
        print

def label_stats(cursor, table="EVENT_VIEW", output="/tmp/labels.csv"):
    with open(output, 'w') as f:
        writer = csv.writer(f)
        l, nl = DB_Utils.labels(cursor, table)
        writer.writerow(['label', 'nevents', 'mingap', 'maxgap', 'meangap', 'stdgap'])
        for label in l:
            nevents = DB_Utils.nevents_per_label(label, cursor, table)
            gaps = map(lambda x: x.total_seconds(), DB_Utils.gaps_between_occurrences(label, cursor, table))
            if gaps:
                mingap = min(gaps)
                maxgap = max(gaps)
                mean = numpy.mean(gaps)
                stddev = numpy.std(gaps)
            else:
                mingap = "/"
                maxgap = "/"
                mean = "/"
                stddev = "/"
            writer.writerow([label, nevents, mingap, maxgap, mean, stddev])

    
def log_gaps(cursor, table="EVENT_VIEW"):
    labels, nl = DB_Utils.labels(cursor, table)
    gaps = []
    for label in labels:
        gaps.append(map(lambda x: x.total_seconds(), DB_Utils.gaps_between_occurrences(label, cursor, table)))
    return labels, gaps

def gaps_hist(gaps, bins=20):
    range_min = gaps[0][0]
    range_max = gaps[0][0]
    for row in gaps:
        if row:
            mini = min(row)
            maxi = max(row)
            if mini < range_min:
                range_min = mini
            if maxi > range_max:
                range_max = maxi
    print (range_min, range_max)
    hists = []
    for gaps_row in gaps:
        numpy.histogram(numpy.arange(50), bins=bins)
        hist, cbins = numpy.histogram(gaps_row, bins=bins, range=(range_min, range_max))     
        hists.append(hist)
    return hists, cbins

def log_gaps_to_csv(cursor, table="EVENT_VIEW", output="/tmp/gaps.csv", fillvalue=None):
    labels, gaps = log_gaps(cursor, table)
    f = open(output, 'w')
    csv_writer = csv.writer(f)
    csv_writer.writerow(labels)
    for row in itertools.izip_longest(*gaps, fillvalue=fillvalue):
        csv_writer.writerow(row)
    f.close()



def gaps_hist_to_csv(cursor, table="EVENT_VIEW", bins=100, output="/tmp/gaps_hist.csv", hrange=(None,None), hdrop=False):
    labels, gaps = log_gaps(cursor, table)

    hmin, hmax = hrange
    if hmin and not hmax:
        l = lambda x: x > hmin
    elif hmax and not hmin:
        l = lambda x: x < hmax
    elif hmin and hmax:
        l = lambda x: x > hmin and x < hmax
    else:
        l = lambda x: True
    for i, gapsrow in enumerate(gaps):
        for i, gapsrow in enumerate(gaps):
            gaps[i] = [y for y in itertools.ifilter(l, gapsrow)]
                    
    hists, bins = gaps_hist(gaps, bins=bins)
    if hdrop:
        for i in xrange(len(hists)):
            drop = True
            j = 0
            while drop:
                if hists[i][j] == 0:
                    drop = False
                else:
                    hists[i][j] = 0
                j += 1

    # Store to csv
    with open(output, 'w') as f:
        csv_writer = csv.writer(f)
        csv_writer.writerow(['bin_start', 'bin_end'] + labels)

        i = 0
        for row in itertools.izip(*hists):
            csv_writer.writerow([bins[i], bins[i+1]] + list(row))
            i += 1

    

        


if __name__ == "__main__":
    import MySQLdb
    import argparse 
    parser = argparse.ArgumentParser("Data exploration",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-d', '--database', help="MySql database", required=True)
    parser.add_argument('-t', '--table', default="EVENT_VIEW", 
            help="""The event table that should be analyzed. Expected: label 
            and timestamp columns""")
    parser.add_argument('-L', default=False, action='store_true', help="Print the label stats")
    gaps_options = parser.add_argument_group("log the gaps")
    gaps_options.add_argument("-G", default=False, help="Log all the gaps between consecutive occurrences", action='store_true')
    gaps_options.add_argument("--Gout", default="/tmp/gaps.csv", help="CSV output for the gaps")
    gaps_options.add_argument("-H", default=False, action='store_true', help="Get histograms of the gaps")
    gaps_options.add_argument("--Hbins", default=100, type=int, help="Nbins in the histogram")
    gaps_options.add_argument("--Hmax", default=None, type=int, help="Gaps bigger than HMAX are not used in the histogram")
    gaps_options.add_argument("--Hmin", default=None, type=int, help="Gaps smaller than HMIN are not used in the histogram")
    gaps_options.add_argument("--Hout", default="/tmp/gaps_hist.csv", help="CSV output for the hist")
    gaps_options.add_argument("--Hdrop", default=False, action='store_true', help="Take the gaps longer than the first empty bin")

    options = parser.parse_args()
    cursor = DB_Utils.db_connection(options.database)
    if options.L:
        general_stats(cursor, options.table)
        label_stats(cursor, options.table)

    if options.G:
        log_gaps_to_csv(cursor, table=options.table, output=options.Gout)

    if options.H:
        gaps_hist_to_csv(cursor, table=options.table, output=options.Hout, bins=options.Hbins, hrange=(options.Hmin, options.Hmax),hdrop=options.Hdrop)
        


