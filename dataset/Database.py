#!/usr/bin/python
# -*- coding: utf-8 -*-

from Import import *

class Database:
    """
    The Database instances contain the data to be mined, or that are
    partially or totally mined

    Attributes:
    events -- The events of the database, that have not (yet) been
        identified as being part of a PeriodicEpisode
    episodes -- Groupings of one or more events. 
    periodic_eps -- episodes for which a regular description for the
        occurrence times as been identified
    last_episode_id -- Counter to identify the episodes
    """
    def __init__(self):
        self.events = []
        self.episodes = set()
        self.periodic_eps = set()
        self.last_episode_id = 0
        self.rare_events = []

    def copy(self):
        d = Database()
        d.events = copy(self.events)
        d.episodes = copy(self.episodes)
        d.periodic_eps = copy(self.periodic_eps)
        d.rare_events = copy(self.rare_events)
        return d

    def __str__(self):
        """
        Human understandable description :)
        """
        s = "###Episodes:\n"
        for f_epi in self.episodes:
            s += str(f_epi) + "\n"
        s += "\n###Periodic episodes:\n"
        for p_epi in self.periodic_eps:
            s += str(p_epi.contents)+ "\n" + str(p_epi) + "\n\n"
        s += "\n###Events:\n"
        for ev in self.events:
            s += str(ev) + "\n"
        return s

    def __repr__(self):
        return self.__str__()

    def __len__(self):
        return len(str(self))

    def remove_event(self, event):
        """
        Remove `event` from the list of events. It is an errot if
        `event` is not in events
        """
        assert event in self.events
        self.events.remove(event)


    def add_event(self, event):
        """
        Add `event` to the event list, and sort the event list. This
        second part is because many functions rely of the fact that
        the event list is sorted.
        """
        self.events.append(event)
        #self.events.sort()

    def add_episode(self, episode):
        """
        Add `episode` to the set of episodes
        """
        self.episodes.add(episode)

    def add_periodic_episode(self, periodic_ep):
        """
        Add `periodic_ep` to the set of periodic_eps
        """
        self.periodic_eps.add(periodic_ep)
        episode = Episode(periodic_ep.contents)
        if episode not in self.episodes:
            self.add_episode(episode)
            

    def merge_periodic_eps(self, end_of_pattern):
        """
        Merge similar periodic episodes. 
        Two cases are considered:
            - 2 episodes stand for the same events
            - an episode is the negation of the other one
        Rely on the methods `merge` and `difference` from the
        PeriodicEpisode module.

        Parameters:
        end_of_pattern -- defines if 2 episodes occur on the same time
            range
        """

        # Merge periodic episodes describing the same events
        merged = True
        r = 0
        while merged:
            r += 1
            merged = False

            # Pairs of distinct periodic episodes
            comb = combinations(self.periodic_eps, 2)
            for c in comb:
                if c[0] not in self.periodic_eps or \
                        c[1] not in self.periodic_eps:
                    continue

                new = c[0].merge(c[1], end_of_pattern)
                if new:
                    merged = True
                    self.periodic_eps.remove(c[0])
                    self.periodic_eps.remove(c[1])
                    self.add_periodic_episode(new)
                            
                else:
                    # Nothing to do
                    pass
        
        # Merge episodes and their negations (happens when some events
        # are detected as errors at first sight, but then, regularity
        # in the "errors" is detected).
        #
        # A typical example would be when some episode happens 
        # everyday but Sundays. It would probably be considered as a 
        # daily episode, and then the absence of the episode would be
        # detected as a weekly episode, happening on Sundays.
        merged = True
        while merged:
            merged = False
            # Pairs of distinct periodic episodes
            comb = combinations(self.periodic_eps, 2)
            for c in comb:
                new = c[0].difference(c[1], end_of_pattern)
                if new:
                    merged = True
                    self.periodic_eps.remove(c[0])
                    self.periodic_eps.remove(c[1])
                    self.add_periodic_episode(new)
                            
                else:
                    # Nothing to do
                    pass

        return



    def get_episode(self, contents):
        """
        Return the episode whose contents attribute matches `contents`
        if such an episode exists and is stored in the episode list.
        Return None otherwise.
        """
        for episode in self.episodes:
            if episode.contents == contents:
                return episode
        return None

##    def get_label(self, episode):
##        """
##        Return the label of the episode of the Database describing the
##        same contents as `episode`. If no such episode exists, the
##        next label name is created, and last_episode_id is incremented
##        """
##        ep = self.get_episode(episode.contents)
##        if ep:
##            return ep.label
##        else:
##            self.last_episode_id += 1
##            return self.last_episode_id

    def get_event(self, timestamp):
        """
        Get the event occurring at `timestamp`. If there is none, a
        warning is usued, and None is returned.
        """
        for event in self.events:
            if event.timestamp == timestamp:
                return event
        else:
            logging.warning(" In get_event: No event occurs at " + \
                    str(timestamp))
            return None

    def get_events(self, start, end):
        """
        Get the list of the events happening between start and end
        """
        timestamps = [i.timestamp for i in self.events]
        restr_timestamps = sublist(timestamps, start, end)
        return map(self.get_event, restr_timestamps)


    def split_data(self, online_mining):
        intervals = []
        def condition_size(i, j):
            if online_mining['chunking_unit'] == 'time':
                return self.events[j].timestamp \
                        - self.events[i].timestamp \
                        > online_mining['chunk_size']
            elif online_mining['chunking_unit'] == 'event':
                return j-i > online_mining['chunk_size']

        def condition_step(ilast, inew):
            if online_mining['chunking_unit'] == 'time':
                return self.events[inew].timestamp \
                        - self.events[ilast].timestamp \
                        > online_mining['chunking_step']
            elif online_mining['chunking_unit'] == 'event':
                return inew-ilast > online_mining['chunking_step']
            
        for j in xrange(len(self.events)):
            if not condition_size(0, j):
                continue
            intervals.append((0, j))
            break
        else:
            return [(i,j)]

        ilast = 0
        for i in xrange(1, len(self.events)):
            if not condition_step(ilast, i):
                continue
            ilast = i
            for j in xrange(len(self.events)):
                if not condition_size(i, j):
                    continue
                intervals.append((i, j))
                break
            else:
                intervals.append((i, j))
                break
        return intervals


if __name__ == "__main__":
    c1 = "Prépare un café"
    c2 = "Bois un café"
    c3 = "Se lève"
    c4 = "S'habille"
    c5 = "Quitte la maison"

    t1 = "7h05"
    t2 = "7h15"
    t3 = "7h"
    t4 = "7h30"
    t5 = "7h40"

    e1 = Event(t1, c1)
    e2 = Event(t2, c2)
    e3 = Event(t3, c3)
    e4 = Event(t4, c4)
    e5 = Event(t5, c5)

    p = Periodicity((24*2600,), 0.0, None) #tous les jours, depuis l'origine des temps
    p2 = Periodicity((24*3600,), 2000.0, None) #tous les jours, depuis l'origine des temps + 2000s

    episode = Episode(set([c1, c3, c3]))
    p_ep = Periodic_Episode(set([c1, c3, c3]), p)
    p_ep2 = Periodic_Episode(set([c1, c3, c3]), p2)

    data = Database()

    data.add_episode(episode)
    data.add_periodic_episode(p_ep)
    data.add_event(e4)
    data.add_event(e5)
    data.add_periodic_episode(p_ep2)

    print data
