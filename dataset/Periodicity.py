# -*- coding: utf-8 -*-

#from Import import *
import time

# FIXME import clean pour ces fonctions définies dans Utils
def str_time(t, format="%Y-%m-%d %H:%M:%S"):
	"""
	Human understandable time representation:
	Default representation is YYYY-mm-dd HH:MM:SS
	"""
	return time.strftime(format, time.gmtime(t))
def time_(t):
	return time.strftime("%H:%M:%S", time.gmtime(t))
def day_and_time(t):
	return time.strftime("%a %H:%M:%S", time.gmtime(t))


class Periodicity:
    """
    Periodicity is described through these parameters:
    period -- the period of the signal (int)
    start -- the date of the first occurrence of the described pattern
    end -- the end of the pattern
    `start` and `end` are expressed in seconds sinc the epoch.
    description -- it's an abstraction describing the occurrences of 
    the pattern as a (sum of) normal distribution(s).
    Each component of `description` contains the mean and the standard 
    deviation of one of the normal distributions.
    """
    def __init__(self, period, description, start, end=None):
        self.period = period
        compare = lambda x,y: cmp(x[0], y[0])
        d = [i for i in description]
        d.sort(cmp=compare)
        #self.description = description 
        self.description = tuple(d)
        self.start = start
        self.end = end

    def __str___(self):
        s = str(self.period) + " " + str_time(self.start) + " " + \
                str_time(self.end) + " " + str(self.description)
        return s

    def __repr__(self):
        s = str(self.period) + " " + str_time(self.start) + " " + \
                str_time(self.end) + " (" 
                
        for m, v in self.description:
            if self.period == 604800:
                s += "(" + day_and_time(m)+ ", " + str(v) + "),"
            elif self.period == 86400:
                s += "(" + time_(m)+ ", " + str(v) + "),"
            else:
                s = s[:-1]
                s += str(self.description)
                s = s[:-1]

        s += ")"
        return s

    def __eq__(self, periodicity):
        eq = self.period == periodicity.period \
                and self.start == periodicity.start \
                and self.end == periodicity.end \
                and self.description == periodicity.description
        return eq

    def __nonzero__(self):
        if hasattr(self, 'period') and \
                hasattr(self, 'start') and \
                hasattr(self, 'end') and \
                hasattr(self, 'description'):
            return True
        else:
            return False

    def is_empty(self):
        return len(self.description)==0

class Periodicity_ED(Periodicity):
    def __init__(self, period, description, start, end=None):
        Periodicity.__init__(self, period, [(i, 0) for i in description], start, end)
        self.description = tuple(description) # NON TRIÉ!

    def __repr__(self):
        return "%d %s %s %s" % (self.period, str_time(self.start), str_time(self.end), self.description)
