# -*- coding: utf-8 -*-

import logging

class CandidateGenerator:
	def __init__(self, dataset, 
			maximal_episode_duration	= 1800, 
			maximal_episode_length		= None, 
			minimal_support				= 5,
			storing_prefix				= '/tmp', 
			):
		
		self.dataset = dataset
		self.maximal_episode_duration = maximal_episode_duration
		self.maximal_episode_length = maximal_episode_length
		self.minimal_support = minimal_support

		self.storing_prefix = storing_prefix
		self.logfile = '/'.join( 
				[storing_prefix, 'candidates', 'time_candidate_generation.txt'])
		self.candidates = set() # FIXME: not nec, since occurrences covers it
		self.occurrences = {}

		#FIXME: kezako?
		self.transaction_counter = 0

	def find_valid_subsets(self):
		#FIXME: kezako?
		t = time()
		logging.info(' . Looking for the valid subsets')

		self.dataset.events.sort()#cmp=Event.__cmp__)
		valid_subsets = []
		l = len(self.dataset.events)

		for i in range(l):
			if i % 500 == 0:
				logging.debug(" . . %d" % i)
			start = self.dataset.events[i].timestamp
			end = start + self.maximal_episode_duration
			j = 0
			evts = []
			while j+i<l and self.dataset.events[i+j].timestamp < end:
				evts.append(self.dataset.events[i+j])
				j += 1
			valid_subsets.append(evts)
		t = time()-t

		logging.debug(' . . Found %d valid subsets. Took %.2fs.' %\
				(len(valid_subsets), t))
		with open(self.logfile, 'a') as f:
			f.write("\t\tvalid subsets generation: %.2fs\n" %t)
			f.write("\t\t%d valid subsets\n" % len(valid_subsets))
		
		return valid_subsets

	def find_occurrences(self, episode):
		#FIXME: kezako?
		if episode not in self.occurrences:
			self.occurrences[episode] = filter_occurrences(
					episode.find_occurrences_2(self.valid_subsets)
					)
		return self.occurrences[episode]

    


class MaximalEpisodesBuilder:
	"""
	Calling these "maximal episode" is poorly chosen, in the sense that they 
	are not episodes, but merely lists of events. However, the episodes 
	present in the dataset can all be extracted from the maximal episodes. 
	The name "maximal episode" comes from the original design of Episode 
	Discovery [1] (and we chose not to change it).

	[1] Edwin O. Heierman. Using information-theoretic principles to discover 
	interesting episodes in a time-ordered input sequence. Phd thesis. 
	University of Texas at Arlington, 2004.
	"""

	def __init__(self, 
			maximal_episode_duration			= 1800, 
			maximal_episode_length	= None, 
			memory_length						= None):
		
		self.maximal_episode_duration = maximal_episode_duration
		self.maximal_episode_length = maximal_episode_length
		self.memory_length = memory_length # FIXME completely useless for now

		# Time-wrapping window
		self.window_contents = []

	def build_maximal_episodes(self, dataset):
		"""
		Follows quite precisely the algorithm described in [1]
		"""
		maximal_episodes = []
		for event in dataset.events:
			me = self.new_event(event)
			if me:
				maximal_episodes.append(me)
		if self.window_contents:
			maximal_episodes.append(list(self.window_contents))
		logging.debug(" Built the maximal episodes. %d max episodes." % len(maximal_episodes))
		return maximal_episodes

	def new_event(self, event):
		created = None
		if self.window_contents:
			max_allowed_start = event.timestamp - self.maximal_episode_duration
			if (self.window_contents[0].timestamp < max_allowed_start or 
					(self.maximal_episode_length and 
						len(self.window_contents) == self.maximal_episode_length)):
				created = list(self.window_contents)
				self.window_contents.pop(0)
			while (self.window_contents and 
					self.window_contents[0].timestamp < max_allowed_start):
				self.window_contents.pop(0)

		self.window_contents.append(event)
		return created
