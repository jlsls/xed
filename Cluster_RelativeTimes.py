# -*- coding: utf-8 -*-

import numpy as np
from scipy.spatial import distance
import sklearn.cluster as skcluster
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
import pylab as pl
from itertools import cycle, izip
import logging
import matplotlib.pyplot as plt
import time

from Utils import *

class Clusterer(object):
    def __init__(self, obs_times, period, max_dist=100, min_samples=5, normalize=False):
        self.obs_times = obs_times
        self.period = period
        self.max_dist = max_dist
        self.min_samples = min_samples
        self.normalize = normalize

        self.offset = time_wrapping_offset(obs_times, period)
        self.rel_times = [(int(o+self.offset)%int(period), o+self.offset) \
                for o in obs_times]

    
class Clusterer_naive(Clusterer):
    def cluster(self):
        self.rel_times.sort(cmp = lambda x, y: cmp(x[0], y[0]))
        logging.debug(' . . . . . clustering the relative occurrence times')
        t = time.time()
        last = self.rel_times[0][0]
        self.outliers = []
        self.clusters = [[self.rel_times[0]]]
        for i in range(1, len(self.rel_times)):
            if self.rel_times[i][0] - last > self.max_dist:
                if len(self.clusters[-1]) < self.min_samples:
                    out = self.clusters.pop()
                    self.outliers.extend(out)
                self.clusters.append([])
            else:
                self.clusters[-1].append(self.rel_times[i])
            last = self.rel_times[i][0]
        logging.debug(' . . . . . . done. Took %.2fs to find %d clusters' % (time.time()-t, len(self.clusters)))
        return [[i[1] for i in o] for o in self.clusters]

    def plot(self):
        plt.figure(figsize=(8, 2))
        for t in self.rel_times:
            if t in self.outliers:
                plt.plot(t[0], 0, 'ko')
        colors = cycle(['b', 'RoyalBlue', 'c', 'g', 'YellowGreen', 'y', 'Darkorange', 'OrangeRed', 'r', 'm', 'Purple'])
        labels = []
        for cluster, color in izip(self.clusters, colors):
            if len(cluster) == 0:
                continue
            s = 0
            for t in cluster:
                s += t[0]
                markersize = 10
                plt.plot(t[0], 0, color=color, marker='o', markersize=markersize)
            labels.append(s/len(cluster))
        label_names = map(lambda x: str_rel_time(x-self.offset, self.period), labels)
        #plt.xticks(labels, label_names, 
        #        horizontalalignment='right', verticalalignment='top',
        #        rotation=30)
        plt.axhline(0, 'k--')


class Clusterer_DBSCANlike(Clusterer):
    def plot(self):
        plt.figure(figsize=(8, 2))
        for t in self.rel_times:
            if t in self.outliers:
                plt.plot(t[0], 0, 'ko', markersize=6)
        colors = cycle(['b', 'RoyalBlue', 'c', 'g', 'YellowGreen', 'y', 'Darkorange', 'OrangeRed', 'r', 'm', 'Purple'])
        labels = []
        nitems = []
        for cluster, color in izip(self.clusters, colors):
            s = 0
            for t in cluster:
                s += t[0]
                if t in self.core_samples:
                    markersize = 16
                else:
                    markersize = 6
                plt.plot(t[0], 0, color=color, marker='o', markersize=markersize)
            labels.append(s/len(cluster))
            nitems.append(len(cluster))
        already_existing_ticks = [int(i) for i in plt.xticks()[0]]
        
        
        print labels
        label_names = map(lambda x,y: '%s\n(%d items)' % (str_rel_time(x-self.offset, self.period), y), labels, nitems)
        if len(labels) < 5:
            label_names.extend(['%s' % str_rel_time(x-self.offset, self.period) for x in already_existing_ticks])
            labels.extend(already_existing_ticks)
        plt.xticks(labels, label_names, 
                horizontalalignment='right', verticalalignment='top',
                rotation=30)
        plt.axhline(y=0, color='k', linestyle='--')
        


class Clusterer_1dimDBSCAN(Clusterer_DBSCANlike):
    def cluster(self):
        logging.debug(' . . . . . clustering the relative occurrence times')
        t = time.time()
        lists = []
        self.core_samples = []
        for i in self.rel_times:
            L = set([j for j in self.rel_times if abs(j[0] - i[0]) <= self.max_dist])
            #if len(L) <= self.min_samples:
                  #outliers.append(i[1])
            if len(L) >= self.min_samples:
                self.core_samples.append(i)
            lists.append(L)

        self.clusters = [lists[0]]
        for l in lists[1:]:
            for j in self.clusters:
                if any(i for i in l if i in j):
                    j.update(l)
                    break
            else:
                self.clusters.append(l)

        self.outliers = []
        map(self.outliers.extend, [l for l in self.clusters if len(l) < self.min_samples])
        logging.debug(' . . . . . . done. Took %.2fs to find %d clusters' % (time.time()-t, len(self.clusters)))
        return [[i[1] for i in o] for o in self.clusters]


class Clusterer_DBSCAN(Clusterer_DBSCANlike):
    def cluster(self):
        logging.debug(' . . . . . clustering the relative occurrence times')
        t = time.time()
        le = len(self.rel_times)
        X = np.zeros((le, 2))
        for i in range(le):
            X[i][0] = self.rel_times[i][0]
        D = distance.squareform(distance.pdist(X))
        if self.normalize:
            D = (D / np.max(D))
        
        core_samples, labels = skcluster.dbscan(D, eps = self.max_dist, min_samples = self.min_samples, metric='precomputed')
        #db.fit(D)

        self.core_samples = [self.rel_times[i] for i in core_samples]
        #labels = db.labels_
        # Number of clusters in labels, ignoring noise if present.
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        logging.debug(' . . . . . . estimated number of clusters: %d' % n_clusters_)

        self.clusters = [[self.rel_times[index[0]] for index in np.argwhere(labels == i)] for i in set(labels) if i != -1]

        self.outliers = [self.rel_times[index[0]] for index in np.argwhere(labels == -1)]
        logging.debug(' . . . . . . done. Took %.2fs to find %d clusters' % (time.time()-t, len(self.clusters)))
        return [[i[1] for i in o] for o in self.clusters]



