# -*- coding: utf-8 -*-

#from Import import *
from time import clock
import logging
import os
import numpy
from numpy import mean, median, std
import itertools
import pickle

from dataset import Event, Episode
from Utils import *

# Candidate generators
import episode_discovery
import episode_discovery.CG_EDlike as CG_EDlike
import episode_discovery.CG_EL as CG_EL
import episode_discovery.CG_FPgrowth as CG_FPgrowth
# TODO: apriori, EM&FET, PEM

# Periodicity
import periodicity_analysis.PeriodicityFinder as PeriodicityFinder
import periodicity_analysis.PeriodicityFinder_autocorrelation as PeriodicityFinder_autocorrelation
from dataset.PeriodicEpisode import PeriodicEpisode
from dataset.Periodicity import Periodicity, Periodicity_ED

# Factorization
from Factorization import *

class Miner:
	"""
	Miner object
	"""
	def __init__(self, dataset, parameters, factorizations=[], round_count=0):
		"""
		Miner constructor

		Attributes:
		dataset -- A data.Database instance. The dataset to be mined
		parameters -- The parameters used to customize mining
		factorizations -- The consecutive factorizations that led to
			the current dataset. 
		"""
		self.dataset = dataset
		self.dataset.events.sort()
		self.parameters = parameters
		self.factorizations = factorizations
		self.round_count = round_count + 1
		
		self.candidates = set()
		self.occurrences = {}
		self.logger = {}
		self.logger[self.round_count] = {}

		self.units_of_length = {
				"missing_episode": 3, # ts, label
				"event": 3, # ts, label
				"episode": 1, # id
				"episode_label": 1,
				"periodicity": 6, # id, period, start, end
				"component": 2, # mean, std
				"shifted_episode": 2 # ts
				}
	

	def __len__(self):
		# FIXME: if the definition of __len__ is ever modified, 
		# the estimate_new_length needs to be updated too !
		result = 0
		for event in self.dataset.events:
			if event.characterizes_missing:
				result+= self.units_of_length["missing_episode"]
			else: 
				result += self.units_of_length["event"]

		for factorization in self.factorizations:
			result += self.units_of_length["episode"] + len(factorization.episode)*self.units_of_length["episode_label"]
			if isinstance(factorization, FactorizePeriodicEpisode):
				# Periodicity description length
				result+= (self.units_of_length["periodicity"] +  
						len(factorization.episode.periodicity.description) * self.units_of_length["component"])

				# Shifted occurrences
				result+= len(factorization.episode.pattern_quality.shifted) * self.units_of_length["shifted_episode"]
		return int(result)

	def __str__(self):
		s = "### EPISODES:\n"
		for f in self.factorizations:
			s += Episode.__str__(f.episode)+"\n"
		
		s += "\n### PERIODIC EPISODES:\n"
		for f in self.factorizations:
			if isinstance(f, FactorizePeriodicEpisode):
				ae = len(f.episode.pattern_quality.as_expected)
				sh = len(f.episode.pattern_quality.shifted)
				mi = len(f.episode.pattern_quality.missing)
				ex = len(f.episode.pattern_quality.extra)

				acc = ae / float(ae + sh + mi)
				ds = ae / float(ae + sh + mi + ex)
				s += "%s %s Acc: %.2f, DC: %.2f\n" % (str(f.episode), 
						str(map(str_time, f.episode.pattern_quality.shifted)),
						acc, ds)
	   
		s += "\n### EVENTS:\n"
		self.dataset.events.sort()
		for ev in self.dataset.events:
			s += str(ev) + '\n'

		return s

	def __repr__(self):
		return str(self)

	def log_to_csv(self):
		with open('/tmp/xED_results.csv', 'w') as f:
			f.write("\n".join(["%s;%s" % (','.join(fact.episode.contents), fact.compression_power) for fact in self.factorizations]))

	def mine(self):
		"""
		Try and describe the current dataset, finding remarkable
		periodic episodes.
		Returns: The best dataset description that could be mined
		"""
		
		time_per_iteration = []
		start_t = clock()

		logging.info("\nITERATION %d\n%s\n" % \
				(self.round_count, "="*70))
		path = self.initialize_new_round()
		with open('%s/initial_db.txt' % path, "w") as f:
			f.write(str(self))
		
		prev_len = len(self)
		self.mine_loop(path)
		with open('%s/obtained_db.txt' % path, "w") as f:
			f.write(str(self))

		# Logger
		self.logger[self.round_count]['data length before this round'] = prev_len
		self.logger[self.round_count]['data length after this round'] = len(self)
		self.logger[self.round_count]['compression'] = len(self) - prev_len
		self.logger[self.round_count]['time this round'] = clock()-start_t
		self.logger[self.round_count]['event count in data'] = len(self.dataset.events)
		self.logger[self.round_count]['factorization count in data'] = len(self.factorizations)


		while len(self) < prev_len and self.dataset.events:
			self.round_count += 1

			start_t = clock()
			logging.info("\nITERATION %d\n%s\n" % \
					(self.round_count, "="*70))
			path = self.initialize_new_round()

			# Logger
			self.logger[self.round_count] = {}
			with open('%s/initial_db.txt' % path, "w") as f:
				f.write(str(self))
			
			prev_len = len(self)
			loop_t = clock()
			self.mine_loop(path)
			time_per_iteration.append(clock()-loop_t)

			# Store the obtained dataset
			with open('%s/obtained_db.txt' % path, "w") as f:
				f.write(str(self))
			# Logger
			self.logger[self.round_count]['data length before this round'] = prev_len
			self.logger[self.round_count]['data length after this round'] = len(self)
			self.logger[self.round_count]['compression'] = len(self) - prev_len
			self.logger[self.round_count]['time this round'] = clock()-start_t
			self.logger[self.round_count]['event count in data'] = len(self.dataset.events)
			self.logger[self.round_count]['factorization count in data'] = len(self.factorizations)

		# Logger
		self.logger['dataset'] = self.dataset.name
		self.logger['parameters'] = self.parameters
		self.store_logs()
	
		with open(os.path.join(self.parameters['storing_prefix'], 'factorizations.dump'), 'w') as f:
			pickle.dump(self.factorizations, f)


		# Logger bis
		headers = None
		with open(os.path.join(self.parameters['storing_prefix'], 'factorization_stats.csv'), 'w') as f:
			for fact in self.factorizations:
				stats = fact.stats()
				if not headers:
					headers = stats.keys() # FIXME: assumes i'm only using periodic facts
					f.write(','.join(['episode', ] + headers) + '\n')

				f.write(','.join([';'.join(fact.episode.contents), ] + [str(stats[k]) for k in headers]) + '\n')

			
		return self

	def episode_general_stats(self, episode):
		info = {'len': len(episode), 'count': len(self.occurrences[episode])}
		durations = map(lambda o: o[-1].timestamp -o[0].timestamp, self.occurrences[episode])

		info['min_duration'] = min(durations)
		info['max_duration'] = max(durations)
		info['mean_duration'] = mean(durations)
		info['std_duration'] = std(durations)
		info['median_duration'] = median(durations)
		return info

	def mine_loop(self, path):
		"""
		One iteration of the xED algorithm
		Candidate generation, Candidate analysis, Factorization
		"""
		
		#---------------------------------------------------------------#
		# Candidate generation										  #
		#---------------------------------------------------------------#
		t_cg = clock()												  #
		self.candidates = self.generate_candidates()					#
		#---------------------------------------------------------------#
		#															   #
		# Logger
		candidate_length = [len(c.contents) for c in self.candidates]
		self.logger[self.round_count]['time for frequent episode generation'] = clock()-t_cg
		self.logger[self.round_count]['frequent episode count'] = len(self.candidates)
		self.logger[self.round_count]['frequent episodes length characteristics'] = {
				"minimum": min(candidate_length),
				'maximum': max(candidate_length),
				'average': numpy.mean(candidate_length),
				'median': numpy.median(candidate_length)}
		#---------------------------------------------------------------#


		logging.info(" dataset length: " + str(len(self)))
		best_cands = []
		cand_count = 0
		logging.info(' Starting candidate study...')
		t_ca = clock()
		for candidate in self.candidates:
			with open('%s/cand_stats.csv' % path, 'a') as f:
				f.write(';'.join(candidate.contents))
				info = self.episode_general_stats(candidate)
				for k in ('len', 'count', 'min_duration', 'max_duration', 'mean_duration', 'median_duration', 'std_duration'):
					f.write(',' + str(info[k]))
				f.write('\n')

			#-----------------------------------------------------------#
			# Candidate study										   #
			cand_count += 1											 #
			if cand_count % 50 == 0:
				logging.info(" Candidate %d: %s" %(cand_count, candidate))  #
			else:
				logging.debug(" Candidate %d: %s" %(cand_count, candidate))  #
			new_interesting_cands = self.study_candidate(candidate,path,
					runtime_plot=self.parameters["runtime_plot"],
					interactive=self.parameters["interactive"],
					format=self.parameters["format"],
					)
			best_cands.extend(new_interesting_cands)					#
			with open('%s/candidates/%s.txt' % (path, candidate.id), 'a') as f:
			#with open('%s/candidates/%s.txt' % (path, '_'.join(candidate.contents)), 'a') as f:
				f.write('\n\nAfter study, deemed interesting =\n')
				f.write('%s\n' % new_interesting_cands)
			#-----------------------------------------------------------#

		# Logger
		self.logger[self.round_count]['time for episode analysis'] = clock()-t_ca
		self.logger[self.round_count]['candidate factorization count'] = len(best_cands)

		if len(best_cands) == 0:
			logging.debug("Best cands is empty")
			self.logger[self.round_count]['time for factorization sorting'] = 'N/A'
			self.logger[self.round_count]['time for factorizing'] = 'N/A'
			self.logger[self.round_count]['factorization count'] = 'N/A'
			return 
		# Sort by (increasing) size
		t_ca = clock()
		best_cands = self.sort_potential_factors(best_cands)

		# Logger
		self.logger[self.round_count]['time for factorization sorting'] = clock()-t_ca
				
		logging.debug(" . candidate factorizations: %s" % best_cands)
		seen = set()
		#new_miner = self
		fname = '%s/factorizations.txt' % path
		t_ca = clock()
		nb_fact = 0
		for factor, l in best_cands:
			assert l < 0
			candidate = factor.episode
			if any(map(factor.collides, self.factorizations)):
			#if any(map(factor.collides, new_miner.factorizations)):
				logging.warning(" . collision when considering %s" \
						% candidate)
				break
			else:
				logging.info(" FACTORIZED BY: " + str(candidate))
				factor.compression_power = float(len(self)) / (len(self) + l)
				self.factorize(factor)
				nb_fact += 1
				#new_miner = new_miner.factorize(factor)
				with open(fname, 'a') as f:
					f.write('\n\nFactorized by:\n%d: %s\n%s' % \
							(factor.episode.id,
							str(factor.episode.contents),
							str(factor.episode.pattern_quality)))

		# Logger
		self.logger[self.round_count]['time for factorizing'] = clock()-t_ca
		self.logger[self.round_count]['factorization count'] = nb_fact

		logging.info("END OF ROUND %d, DATABASE: \n%s" %(self.round_count,  str(self)))
		return

	def study_candidate(self, candidate, path, **kwargs):
		"""
		Study the periodicity of the candidate episode.
		If such a periodicity is found, return the corresponding periodic 
		episode.
		"""
		assert candidate in self.occurrences
		cpath = '%s/candidates/%s.txt' % (path, candidate.id)
		#cpath = '%s/candidates/%s.txt' % (path, '_'.join(candidate.contents))
		# kwargs intéressants: runtime_plot, interactive, form
		best_cands = []
		cand_t = clock()
		best_factorization = None
		
		occurrences = self.find_occurrences(candidate)
		logging.debug(" . support: " + str(len(occurrences)))
		if len(occurrences) < self.parameters['minimal_support']:
			logging.error("len(occurrences): %d < minimal_support: %d" % 
					(len(occurrences), 
						self.parameters['minimal_support']))
			assert False 
			# Candidate generation should not propose rare candidates

		pf_method = {'mixture': PeriodicityFinder.PeriodicityFinder,
				'autocorrelation': PeriodicityFinder_autocorrelation.PeriodicityFinder_autocorrelation}
		
		pf = pf_method[self.parameters['pem_method']](candidate, occurrences, path, self.parameters)
		pat_quals = pf.what_periodicity(**kwargs)
		
		for pat_qual in pat_quals:
			if not pat_qual:
				continue
			episode = PeriodicEpisode(candidate.contents, pat_qual.periodicity, pat_qual)
			factor = FactorizePeriodicEpisode(episode, occurrences)
			#TODO: enregistrer dans prefixpath/logs/factors.txt plutôt
			with open(cpath, 'a') as f:
				f.write("%s\n"%factor)
			
			# length variation for the db if it is factorized by factor
			delta_l = self.estimate_new_length(factor) 
			if delta_l < 0:
				best_cands.append((factor, delta_l))
				logging.debug(" . . found better length: %d"\
						%(len(self) + delta_l))
				with open(cpath, 'a') as f:
					f.write("This one is interesting (%d → %d)\n"%(len(self), delta_l))
			else:
				with open(cpath, 'a') as f:
					f.write("NOT interesting (%d → %d)\n"%(len(self), delta_l))

			logging.debug(" . . done. Took %.2fs" % (clock()-cand_t))
		return best_cands

	def generate_candidates(self):
		cg_method = {'edlike': CG_EDlike.CG_EDlike,
				'fpgrowth':  CG_FPgrowth.CG_FPgrowth,
				'el': CG_EL.CG_EL,
				#FIXME 'apriori':  CandidateGenerator_APriori,
				}
		cg = cg_method[self.parameters['fem_method']]( 
				self.dataset, 
				storing_prefix = self.parameters['storing_prefix'], 
				maximal_episode_duration = self.parameters["maximal_episode_duration"],
				maximal_episode_length = self.parameters["maximal_episode_length"],
				minimal_support = self.parameters["minimal_support"],
				)
		self.candidates = cg.generate_candidates()
		occs = 0
		for episode in cg.occurrences:
			self.occurrences[episode] = cg.occurrences[episode]
			occs += len(self.occurrences[episode])
			self.dataset.new_episode(episode)
		
		del cg

		logging.info(' . . found %d candidates' % len(self.candidates))
		return self.candidates

	def prune_rare_events(self):
		support = {}
		for event in self.dataset.events:
			if event.label in support:
				support[event.label].append(event)
			else:
				support[event.label] = [event]
		for content in support:
			if len(support[content]) < self.parameters['minimal_support']:
				logging.debug(" . Removed: " + content)
				self.dataset.rare_events.extend(support[content])
				map(self.dataset.events.remove, support[content])
		logging.info(" . There are "
				+str(len(self.dataset.events))+ " events left")
		return


	def estimate_new_length(self, factor):
		res = 0
		rmd = []
		pat_qual = factor.episode.pattern_quality
		for o in factor.episode_occurrences:
			if any(map(lambda e: e in rmd, o)):
				# Occurrences may overlap. The 2nd one must not be factorized
				continue
			start = o[0].timestamp
			if start in pat_qual.as_expected or start in pat_qual.shifted:
				if all(map(lambda e: e not in rmd, o)):
					assert all(map(lambda e: e in self.dataset.events, o))
					factor.concerned_events.extend(o)
					rmd.extend(o)
					res -= len(o) * self.units_of_length['event']
				if start in pat_qual.shifted:
					res += self.units_of_length['shifted_episode']
		description = factor.episode.periodicity.description
		
		missing_label = 'missing %d' % factor.episode.id
		for m in pat_qual.missing:
			factor.missing_characterization.append(Event(m, missing_label, characterizes_missing=factor.episode))
			res += self.units_of_length['missing_episode']

		# Count episode description length
		res += self.units_of_length['episode'] + len(factor.episode) * self.units_of_length['episode_label']

		# Count periodicity description length
		res += self.units_of_length['periodicity'] + len(description)*self.units_of_length['component']
		return res

	def factorize(self, factor):
		"""
		Simplify the dataset with factor FACTOR.
		The returned miner is a new instance
		FACTOR is added to the new_miner's list of factorizations
		"""
		map(self.dataset.events.remove, factor.concerned_events)
		map(self.dataset.new_event, factor.missing_characterization)
		self.dataset.new_periodic_episode(factor.episode)
		self.factorizations.append(factor)
		return self

	def factorize_frequent_episodes(self):
		found_better=True
		while found_better:
			found_better=False
			sizes = []
			for episode in self.occurrences:
				if len(episode.contents) > 1:
					occs = self.find_occurrences(episode)
					size = sum([len(o) for o in occs])
					sizes.append((episode, size))
			sizes.sort(cmp=lambda x, y: cmp(y[1], x[1]))

			new_miner = deepcopy(self)
			new_miner.round_count += 1
			seen = set()
			for epi, size in sizes:
				occs = self.occurrences[epi]
				already_seen = lambda x: x in seen
				conflict = lambda l: any(map(already_seen, l.events))
				if any(map(conflict, occs)):
					break
				else:
					evts = []
					last = deepcopy(new_miner)
					for o in occs:
						start = o.beginning().timestamp
						evts.extend(o.events)
						map(new_miner.dataset.events.remove, o.events)
						new_miner,dataset.events.append(Event(start, episode.id))
					seen.update(evts)
					f=FactorizeEpisode(epi, occs) 
					f.concerned_events=evts[:]
					new_miner.factorizations.append(f)
					if len(new_miner) < len(self):
						found_better = True
					else:
						new_miner = last
						break 
			if found_better:
				Miner.__init__(self, new_miner.dataset, new_miner.parameters, factorizations = new_miner.factorizations)
				self.round_count += 1

		assert new_miner.round_count > 1
		return new_miner

	def find_occurrences(self, episode):
		assert episode in self.occurrences
		return self.occurrences[episode]

	def sort_potential_factors(self, best_cands):
		"""
		for each best_cand in best_cands:
		best_cand[0]: factor
		best_cand[1]: delta_l (compression power)
		"""
		# Sort on compression power first, then on accuracy
		best_cands.sort(key = lambda x: (x[1], x[0].episode.pattern_quality.accuracy()))
		return best_cands

	def initialize_new_round(self):
		path = '%s/iteration%02d' % (self.parameters['storing_prefix'], self.round_count)
		os.makedirs(path)
		if self.parameters['runtime_plot']:
			os.makedirs('%s/histograms' % path)
			os.makedirs('%s/outliers' % path)
		os.makedirs('%s/candidates' % path)
		os.makedirs('%s/factorizations' % path)
		with open('%s/overview.txt' % path, 'w') as f:
			pass
		logging.info(' Logs are stored in %s' % path)
		with open('%s/cand_stats.csv' % path, 'w') as f:
			f.write(','.join(('episode', 'len', 'count', 'min_duration', 'max_duration', 'mean_duration', 'median_duration', 'std_duration')))
			f.write('\n')

		return path

	def store_logs(self):
		"""
		Store the data in self.logger, in a csv-like format, in prefix/logs
		The data will be read to draw charts using GNUplot
		"""
		#TODO
		path = os.path.join(self.parameters['storing_prefix'], 'logs')
		os.makedirs(path)

		# Time 
		time_logfile = open(os.path.join(path, 'execution_time.data'), 'w')
		time_logs = ['time this round', 'time for frequent episode generation', 
				'time for episode analysis', 'time for factorization sorting', 
				'time for factorizing']
		time_logfile.write('# iteration\t%s\n' % '\t'.join(time_logs))

		# Object counts
		oc_logfile = open(os.path.join(path, 'object_counts.data'), 'w')
		oc_logs = ['frequent episode count', 'candidate factorization count']
		oc_logfile.write('# iteration\t%s\n' % '\t'.join(oc_logs))

		# Characteristics for frequent episodes
		fe_logfile = open(os.path.join(path, 'frequent_episodes.data'), 'w')
		fe_logs = ['minimum', 'maximum', 'average', 'median']
		fe_logfile.write('# iteration\t%s\n' % '\t'.join(fe_logs))

		# Dataset characteristics
		dc_logfile = open(os.path.join(path, 'dataset_characteristics.data'), 'w')
		dc_logs = ['data length before this round', 'data length after this round', 
				'compression', 'event count in data', 'factorization count in data']
		dc_logfile.write('# iteration\t%s\n' % '\t'.join(dc_logs))

		for iteration in self.logger:
			if not isinstance(iteration, int):
				continue
			
			t = '%d\t' % iteration
			t += '\t'.join(str(self.logger[iteration][key]) for key in time_logs)
			time_logfile.write(t + "\n")

			o = '%d\t' % iteration
			o += '\t'.join(str(self.logger[iteration][key]) for key in oc_logs)
			oc_logfile.write(o + "\n")

			e = '%d\t' % iteration
			e += '\t'.join(str(self.logger[iteration]['frequent episodes length characteristics'][key]) for key in fe_logs)
			fe_logfile.write(e + "\n")

			d = '%d\t' % iteration
			d += '\t'.join(str(self.logger[iteration][key]) for key in dc_logs)
			dc_logfile.write(d + "\n")

		time_logfile.close()
		oc_logfile.close()
		fe_logfile.close()
		dc_logfile.close()
