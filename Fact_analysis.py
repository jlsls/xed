from Import import *

with open('/tmp/miner.dump', 'r') as f:
    miner = load(f)


def print_period(t):
    if t == 604800:
        return "1 week"
    else:
        return "1 day"

print """
\\begin{tabular}{|c|c|c|c|c|c|}
\\hline
"""
for f in miner.factorizations:
    print ', '.join([c for c in f.episode.contents]), '&', \
            str_time(f.episode.periodicity.start), '&', \
            str_time(f.episode.periodicity.end), '&', \
            print_period(f.episode.periodicity.period), '&', \
            '['+ ', '.join(['(%s, %f)' % (str_rel_time(d[0], f.episode.periodicity.period), d[1]) for d in f.episode.periodicity.description]), '] &', \
            '%.0f' % (100 * float(len(f.episode.pattern_quality.as_expected)) / \
            float(len(f.episode.pattern_quality.as_expected) + \
            len(f.episode.pattern_quality.shifted) + \
            len(f.episode.pattern_quality.missing))), "\\% \\\\ \\hline"

print """
\\end{tabular}
"""
