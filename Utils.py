# -*- coding: utf-8 -*-

import datetime
import time
import pylab as pl
#from Import import *


"""
Useful functions, without direct link with the project
"""


def remove_duplicates(L):
	"""
	! Order is not preserved!
	Returns a copy of L, where each item is unique
	"""
	return list(set(L))


def parse_time(t, dt_format="%Y-%m-%d %H:%M:%S"):
    """
    Parse time representation. Default representation is YYYY-mm-dd HH:MM:SS  
    """
    return datetime.datetime.strptime(t, dt_format)

def str_time(t, dt_format="%Y-%m-%d %H:%M:%S"):
	"""
	Human understandable time representation:
	Default representation is YYYY-mm-dd HH:MM:SS
	"""
	return time.strftime(dt_format, time.gmtime(t))

def str_list(L):
	result = ""
	for l in L:
		result += str(l) + "\n"
	return result

def str_dict(D):
	s ="\n"
	for k, v in D.items():
		s += str(k) + ":\t" + str(v) + "\n"
	return s

def filter_list_of_tuples(L):
	"""
	The same element should not appear twice
	"""
	seen = {}
	L_ = []
	for l in L:
		for e in L:
			if e in seen:
				break
		else:
			L_.append(l)

	return L_


def get_occurrences(occurrences, start_times):
	result = []
	for start_time in start_times:
		for occurrence in occurrences:
			#t = [o.timestamp for o in occurrence]
			if start_time == occurrence.beginning().timestamp:
				result.append(occurrence)
	return result

def time_gaps(L):
	"""
	Returns the list of the gaps between two consécutive elements of L
	"""
	return [L[i+1]-L[i] for i in range(len(L)-1)]


def autocorrelation(L, limit):
	"""
	Computes the autocorrelation coefficients of `L`
	For each t, the autocorrelation is worth the same as the
	correlation coefficient between L[t:] and L[:-t]
	`limit` is here to prevent useless computing: only the first
	values are potentially interesting.
	"""
	#logging.debug("list to 'autocorrelate': " + str(L))
	std_dev = std(L)
	autocorr = [0]
	if std_dev:
		for tau in range(1, len(L)-1):
			if mean([sum(L[i:i+tau]) for i in range(len(L) - tau)]) > limit:
				break;
			a = array([L[tau:], L[:-tau]])
			c = corrcoef(a)
			autocorr.append(c[0][1])
	#logging.debug("autocorrelation: " + str(autocorr))
	return autocorr


def build_start(first, period):
	return (int(first)/int(period)) * period

def build_end(last, period):
	if (int(last)/int(period)) * period == last:
		return (int(last)/int(period)) * period
	return (int(last)/int(period) + 1) * period


def day_and_time(t):
	return time.strftime("%a %H:%M:%S", time.gmtime(t))

def time_(t):
	return time.strftime("%H:%M:%S", time.gmtime(t))

def str_rel_time(t, period):
	if t < 0:
		t += period
	if period == 86400:
		return time_(t)
	elif period == 604800:
		return day_and_time(t)
	else:
		return str(t)


def gcd(a, b):
	if b == 0: return a
	return gcd(b, a % b)

def lcm(a, b):
	return a * b / gcd(a, b)

def epoch2date(t):
	return pl.num2date(pl.epoch2num(t))


def get_obs_times(occurrences):
	start = []
	return [float(o.beginning().timestamp) for o in occurrences]


def filter_occurrences(occurrences):
	t = time()
	occurrences=list(occurrences)
	occurrences.sort()
	results = set()
	seen = {}

	for o in occurrences:
		for e in o.events:
			if e in seen:
				break
			else:
				seen[e] = True
		else: 
			results.add(o)

	return results


def count_list_items(L):
	L.sort()
	result = {}
	prev = None
	for i in L:
		if i == prev:
			result[i] += 1
		else:
			result[i] = 1
			prev = i
	return result


def time_wrapping_offset(occ_times, period):
	"""
	Finds the best place to break between days, so that episodes  
	don't get caught up on the limit (events around midnight)
	"""
	rel_times = [int(o)%int(period) for o in occ_times]
	rel_times.sort()
	tg = time_gaps(rel_times)
	tg.append(rel_times[0]+period - rel_times[-1])

	# Find the maximal interval, and the corresponding index
	max_gap = 0
	index = 0
	i = 0
	for gap in tg:
		if gap > max_gap:
			max_gap = gap
			index = i
		i += 1
	
	# Compute the offset
	offset = rel_times[index] + float(max_gap)/2
	if offset >= period:
		offset -= period
	return offset


# Utils on datetime and timedelta objects
def datetime2unix(dt):
	epoch = datetime.datetime.utcfromtimestamp(0)
	delta = dt - epoch
	return int(delta.total_seconds())

def datetime2relativeposition(dt, period):
	""" 
	Equivalent to the modulo operation. Except that this one is designed for datetime objects.
	"""
	return (dt - datetime.datetime.min).total_seconds() % period.total_seconds()

def datetime2roundeddatetime(dt, period):
	"""
	Original datetime minus the result of the modulo operation
	"""
	return dt - datetime.timedelta(seconds=datetime2relativeposition(dt, period))

