# -*- coding: utf-8 -*-
#!/usr/bin/python

import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from Factorization import FactorizePeriodicEpisode
import logging
import pylab as pl
from Utils import str_rel_time, epoch2date, build_start

class Plot:
    def __init__(self, miner, start, interactive=False, width=None, height=None):
        self.miner = miner
        self.start = start
        self.width = width
        self.height = height

        
    def plot(self, path='', form='png', interactive=False, xlim=None):
        logging.info(" plotting the timeline...")
        fac_len = len(self.miner.factorizations)
        pl.figure(figsize=(8, fac_len/2))
        pl.clf()

        names = {}
        first = epoch2date(self.start)
        xts= build_start(self.start, 86400)
        xt = xts
        while xt-xts < 30*86400:
            xt += 86400
            pl.axvline(epoch2date(xt), linestyle='--', color='k')
        # For the legend
        pl.plot_date(first, 0, fmt='r-', linewidth=4, label='expected interval')
        pl.plot_date([first], [0], fmt='bo', label='observed', markersize=10)
        #pl.plot_date([first], [0], fmt='bx', label='shifted observation')
        pl.plot_date([first], [0], fmt='g^', 
                label='extra observation', markersize=10)
#        pl.xlabel("time")
        #pl.ylabel("episode label")
#        pl.title("Timeline: the expected and observed events and"\
#                " episodes\n", weight='demibold')
        
        dec = lambda x: x.decode('utf-8')
        position = {}
        k = 1
        for factor in self.miner.factorizations:
            ep = factor.episode
            print str(ep)+": " + str(k)
            #if ep.id not in position:
            position[factor.fac_id] = k
            k+=1

        i = 0
        for factor in self.miner.factorizations:
            i += 1
            pe = factor.episode
            factor.plot_factor(self.miner.parameters['maximal_episode_duration'], position[factor.fac_id])
            print factor.fac_id, position[factor.fac_id], i
            names['episode %d' % i] = position[factor.fac_id]
            #names[',\n'.join(map(dec, pe.contents))] = position[factor.fac_id]

        # plot the events that are left in the dataset
#        labels = [i.id for i in self.miner.dataset.periodic_eps]
#        for e in self.miner.dataset.events:
#            if not e.characterizes_missing and not e.is_fac:
#                ep = self.miner.dataset.get_episode(
#                        frozenset([e.content]))
#
#                if ep:
#                    if ep.id not in position:
#                        position[ep.id] = k
#                        k +=1
#                    names[u': '.join([str(ep.id), u'\n'.join(map(dec, ep.contents))])] = position[ep.id]
#                    ts = epoch2date(e.timestamp)
#                    pl.plot_date([ts], [position[ep.id]], fmt='gx')
#                else:
#                    logging.warning(" event " + str(e) + " was not plotted")

        items = names.items()
        contents = [k[0] for k in items]
        labels = [k[1] for k in items]

        pl.yticks(labels, contents, size='medium')
        pl.ylim(ymax = len(labels)+1)
        if xlim:
            pl.xlim(epoch2date(xlim[0]), epoch2date(xlim[1]))

        fontP = FontProperties(size='medium')
        pl.xticks(horizontalalignment='right',
                verticalalignment='top',
                rotation=15, size='medium')
        #fontP.set_size("small")
        pl.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05),
				          fancybox=True, shadow=True, 
						  ncol=3, prop=fontP, 
						  #bbox_to_anchor=(1.02, 1.05), fancybox=True, shadow=True
						  )
        #pl.legend(loc=6, prop=fontP, bbox_to_anchor=(1, 0.8), fancybox=True, shadow=True)
        if interactive: 
            pl.show()
            raw_input("\nPress any key to continue")

        else:
            name = 'timeline.%s' % form
            fname = '/'.join([path, name])
            pl.savefig(fname, bbox_inches='tight')
            logging.info(" Saved in file " +fname)


    def plot_histograms(self, path='', form='png', interactive=False):
        
        l = len([ f for f in self.miner.factorizations \
                if isinstance(f, FactorizePeriodicEpisode)])
        nb_rows = l/2 +1
        #plt.figure(figsize=(16, nb_rows*4))
        #plt.subplots_adjust(hspace=0.6)
        nb = 0
        for f in self.miner.factorizations:
            if isinstance(f, FactorizePeriodicEpisode):
                nb += 1
                plt.figure()
                f.plot_histogram(path, form, interactive)#, (nb_rows, 2, nb))

#        if nb % 2 == 0:
#            plt.legend(
#                    loc=2, bbox_to_anchor=(0, -0.2),
#                    fancybox=True, shadow=True,
#                    ncol=3)
#        else:
#            plt.legend(
#                    loc=6, bbox_to_anchor=(1.1, 0.5),
#                    fancybox=True, shadow=True,
#                    )

        if interactive:
            plt.show()
            raw_input("\nPress any key to continue")
        else:
            name = 'histograms.%s' % form
            fname = '/'.join([path, name])
            plt.savefig(fname, bbox_inches='tight')


    def plot_occurrence_times(self, path='', form='png', interactive=False, xlim=None):
        for f in self.miner.factorizations:
            if isinstance(f, FactorizePeriodicEpisode):
                f.plot_occurrence_time(path, form, interactive, xlim)


    def plot_timegaps(self):
        for f in self.miner.factorizations:
            print f.__dict__
            print f.occurrences
            occs = [i for i in f.occurrences]
            occs.sort()
            gaps = []
            for i in range(1, len(occs)):
                gaps.append(occs[i].beginning().timestamp - occs[i-1].beginning().timestamp)
            print gaps
            nbbins = max(gaps)/600 #paquets de 10min
            plt.hist(gaps, nbbins, histtype ='bar', color='red', range=(0,  max(gaps)))
            local=''.join(["timegaps_for_ep", str(f.episode.id)])
#            if form:
#                local='.'.join([local, form])
            self.p.savefig(local, bbox_inches='tight')
            self.p.clf()


def interpret_xlim(start, end):
    res = ["%a %b %d %X", "%Y-%m-%d %X", "%x", "%Y-%m-%d"]
    for re in res:
        try:
            s = strptime(start, re)
            e = strptime(end, re)
            return map(mktime, [s, e])
        except ValueError:
            continue
    else:
        return map(float, [start, end])

if __name__ == '__main__':
    import pickle
    from time import time, sleep
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-f', '--filename', required=True, 
            help="The dump file from which info should be plotted")
    parser.add_argument('-H', '--histograms', action='store_true',
            help="Plot the histograms for the periodic episodes",
            default=False)
    parser.add_argument('-o', '--occurrences', action='store_true',
            help="Plot the occurrence times for the periodic\
                    episodes", default=False)
    parser.add_argument('-t', '--timeline', action='store_true',
            help="Plot the timeline, with expected and observed\
                    events", default=False)
    parser.add_argument('-a', '--allplots', action='store_true', 
            help="The same as -hot", default=False)
    parser.add_argument('-p', '--prefix', dest='prefix', default='.', 
            help="Prefix path, defining where to store the plots")
    parser.add_argument('-i', '--interactive', action='store_true',
            default=False, help="Interactive mode, display matplotlib frames")
    parser.add_argument('-g', '--gaps', help='plot time gaps', action='store_true')
    parser.add_argument('--format', help='The output format for the plots (pdf, png, ...). Default is png',
            default='png')
    parser.add_argument('--min_xlim', help='Define data range to be plotted')
    parser.add_argument('--max_xlim', help='Define data range to be plotted')

    arguments = parser.parse_args()
    plt_hist, plt_occs, plt_tl = False, False, False
    if arguments.histograms or arguments.allplots:
        plt_hist = True
    if arguments.occurrences or arguments.allplots:
        plt_occs = True
    if arguments.timeline or arguments.allplots:
        plt_tl = True
    if arguments.interactive:
        plt.ion()
    if arguments.gaps:
        miner = None
        minerdump = arguments.filename
        with open(minerdump, 'r') as f:
            miner = load(f)

        if not miner:
            print >>stderr, 'No Miner instance...'
            raise BaseException
        start = min(miner.dataset.events).timestamp
        plots = Plot(miner, start, interactive=arguments.interactive)
        plots.plot_timegaps()
        
    else:
        miner = None
        minerdump = arguments.filename
        with open(minerdump, 'r') as f:
            miner = pickle.load(f)

        if not miner:
            print >>stderr, 'No Miner instance...'
            raise BaseException

        start = min(miner.dataset.events).timestamp
        plots = Plot(miner, start, interactive=arguments.interactive)

        # Gestion du format des images
        if arguments.format:
            form = arguments.format
        else:
            form = 'png'
        
        # Gestion des xlim 
        if arguments.min_xlim and arguments.max_xlim:
            xlim = interpret_xlim(arguments.min_xlim, arguments.max_xlim)
        else:
            xlim = None

        #plots.nmax = 6
        if plt_hist:
            print "Plotting the histograms"
            plots.plot_histograms(arguments.prefix, interactive=arguments.interactive, form=form)
        if plt_occs:
            print "Plotting the periodic episodes occurrences"
            plots.plot_occurrence_times(arguments.prefix, interactive=arguments.interactive, form=form, xlim=xlim)
        if plt_tl:
            print "Plotting the timeline of events"
            plots.plot(arguments.prefix, interactive=arguments.interactive, form=form, xlim=xlim)
            

