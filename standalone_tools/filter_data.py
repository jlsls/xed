import DB_Utils

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()

    dataset = parser.add_group("Dataset selection")
    dataset.add_argument('-d', '--dataset', required=True, help="MySQL database")
    dataset.add_argument('-t', '--table', default="EVENT_VIEW",  
            help="""Table where the events are. Should have a timestamp and a 
            label column""")

    parser.add_argument('-g', '--gap_thereshold', default=600, help="Consecutive occurrences closer than
