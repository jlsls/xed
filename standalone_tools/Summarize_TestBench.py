import os
import re
pref = '/tmp'
datasetname = 'HouseA_activities'
log_to = '/tmp/overview.csv'
with open(log_to, 'w') as f:
    pass

c = 0
for path in os.listdir(pref):
    if datasetname not in path:
        continue
    c += 1
    if c%10 == 0:
        print c, 'tests'

    try:
        with open(os.path.join(pref, path, 'overview.xml'), 'r') as f:
            lines = f.readlines()
        
        in_param = -1
        in_ite = 0
        pnames = ''
        pvalues = ''
        while lines:
            line = lines.pop(0)
            if '/parameter' in line:
                in_param = 1 # -1: avant, 0: dans, 1: apres
                with open(log_to, 'a') as f:
                    f.write("\n\n" + pnames + "\n")
                    f.write(pvalues + "\n")
                    f.write("\n\niteration;cand_gen_time;cand_gen_nb;cand_len_min;cand_len_max;cand_len_mean;cand_len_median;cand_analysis_time;cand_analysis_nb;cand_sort_time;factorization_time;factorization_nb;compression_nb;total_iteration_time;previous_length;new_length;nb_events;nb_perepis;\n")
            elif 'parameters' in line:
                in_param = 0
            elif in_param == 0:
                m = re.search(".+param name='(.+)' value='(.+)'/>", line)
                if m:
                    name, value = m.group(1, 2)
                    if name in ["episode_capacity", "episode_length", "minimal_frequency", "max_stddev", "minimal_accuracy"]:
                        pnames += name+';'
                        pvalues += value+';'


            elif "/iteration>" in line:
                in_ite = 0
                with open(log_to, 'a') as f:
                    f.write(";".join([iteration, cgt, cgn, mi, ma, me, med, cat, can, cs, fat, fan, co, gt, pl, nl, ee, pe]) + ';\n')
            elif 'iteration' in line:
                m = re.match(".+iteration id='(.+)'>", line)
                cgt, cgn, mi, ma, me, med, cat, can, cs, fat, fan, co, gt, pl, nl, ee, pe = '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''
                if m:
                    in_ite = 1
                    iteration = m.group(1)
            elif in_ite == 1:
                r = ".*cand_gen time='(.+)' nb='(.+)'/>"
                m = re.search(r, line)
                if m:
                    cgt, cgn = m.group(1, 2)
                    continue
                r = ".*cand_lengths min='(.+)' max='(.+)' mean='(.+)' median='(.+)'/>"
                m = re.search(r, line)
                if m:
                    mi, ma, me, med = m.group(1, 2, 3, 4)
                    continue
                r = ".*cand_analysis time='(.+)' nb='(.+)'/>"
                m = re.search(r, line)
                if m:
                    cat, can = m.group(1, 2)
                    continue
                r = ".*cand_sort time='(.+)'/>"
                m = re.search(r, line)
                if m:
                    cs += m.group(1)
                    continue
                r = ".*factorization time='(.+)' nb='(.+)'/>"
                m = re.search(r, line)
                if m:
                    fat, fan = m.group(1, 2)
                    continue
                r = ".*compression nb='(.+)'/>"
                m = re.search(r, line)
                if m:
                    co = m.group(1)
                    continue
                r = "global time='(.+)' prevlength='(.+)' newlength='(.+)'/>"
                m = re.search(r, line)
                if m:
                    gt, pl, nl = m.group(1, 2, 3)
                    continue
                r = "data events='(.+)' perepis='(.+)'/>"
                m = re.search(r, line)
                if m:
                    ee, pe = m.group(1, 2)


        
    except IOError:
        print '%s/overview.xml' % path, "doesn't exist (yet)"
        c -= 1

print c, 'tests in total'
