# -*- coding: utf-8 -*-

import logging
from numpy import asarray
from sklearn.mixture import GMM
from math import sqrt

import Utils

from dataset.Periodicity import Periodicity
from Cluster_RelativeTimes import *
import cfg

class PeriodicityFinder:
	def __init__(self, episode, occurrences, path, 
			storing_prefix = '/tmp',
			default_periods = {86400: 5400, 604800:10800},
			max_stddev = None,
			end_of_pattern = 5,
			minimal_accuracy = 0.5,
			max_nbcomp = None,
			method = 'mixture',
			nbcomp_determination = 'DBSCAN',
			brute_force = False,
			runtime_plot = False,
			format = 'pdf',
			interactive = False):

		self.episode = episode
		self.occurrences = list(occurrences)
		self.occurrences.sort()
		self.path = path

		self.storing_prefix = storing_prefix
		
		# Periodicity characteristics
		self.default_periods = default_periods
		self.max_stddev = max_stddev
		self.end_of_pattern = end_of_pattern
		self.minimal_accuracy = minimal_accuracy
		self.max_nbcomp = max_nbcomp

		# Periodicity search
		self.method = method
		self.nbcomp_determination = nbcomp_determination
		self.brute_force = brute_force

		# Plot utilities
		self.runtime_plot = runtime_plot
		self.format = format
		self.interactive = interactive

		self.obs_times = [o[0].timestamp for o in self.occurrences]
		self.fname = '%s/candidates/%s.txt' % (path, episode.id)
		#self.fname = '%s/candidates/%s.txt' % (path, '_'.join(episode.contents))
		with open(self.fname, 'a') as f:
			f.write("="*50)
			f.write("\nIteration %d\n\n" % cfg.COUNTER_iteration)
			f.write("CANDIDATE %s:\n" % str(episode))
			f.write("%d occurrences\n\n" % (len(occurrences)))
			f.write("period\tnbcomp\tPattern quality\n" + \
					"------\t------\t---------------\n\n")

	def what_periodicity(self, runtime_plot=False, interactive=False, format="png"):
		logging.debug(" . . DETERMINING PERIODICITIES")

		periods = self.default_periods.keys()
		#TODO: If a reliable method is found, 
		#extend here periods list with relevant values
		# periods.extend(...)

		best_ratio = 0
		best_pattern = [EMPTY_PAT_QUAL]
		
		for period in periods:
			pat_quals = self.what_periodicity_for_period(period, runtime_plot, interactive, format)
			ratios = map(Ratio, pat_quals)
			if sum([r.factorized for r in ratios]) > best_ratio:
				with open(self.fname, 'a') as f:
					f.write("  ↑ found a better pattern (le meilleur "\
							+ "pour le moment, toutes périodes confondues)\n\n")
				best_pattern = pat_quals
				best_ratio = sum([r.factorized for r in ratios])
		logging.debug(' . best patterns: \n%s' % best_pattern)
		with open(self.fname, 'a') as f:
			f.write("---------------------------------------\n" +\
					"And the winner is : %s\n--------------------------------------\n\n" \
					% (str(best_pattern)))
		return best_pattern

	def what_periodicity_for_period(self, period, runtime_plot=False, interactive=False, format="png"):
		#global cfg.COUNTER_iteration
		logging.debug(' . . . for period %d' % (period))

		gaps = [self.obs_times[i+1] -self.obs_times[i] \
				for i in range(len(self.obs_times)-1)]
		too_big_index = [i for i in range(len(gaps)) if gaps[i] > self.end_of_pattern*period]
		too_big_index.insert(0, 0)
		too_big_index.append(len(self.obs_times))
		#FIXME: ne marche pas si plusieurs too_big_gaps font 
		#exactement la même longueur. Ce qui n'est pas si 
		#improbable que ça dans une situation de factorisation 
		#d'anciens missing
		l_obs_times = [self.obs_times[
			too_big_index[i]+1 : too_big_index[i+1]+1] 
			for i in range(len(too_big_index)-1)]


		patterns = []
		for obs_times in l_obs_times:
			if len(obs_times) < 1:#self.params.candidate_generation['min_support']:
				continue
			nb_periods = int(obs_times[-1]-obs_times[0]) / period
			# Remove the outliers
			logging.debug(' . . . . remove outliers')
			eps = self.default_periods[period]#.01
			min_samples = nb_periods*(self.minimal_accuracy/2)

			d = {'naive': Clusterer_naive,
					'1dimDBSCAN': Clusterer_1dimDBSCAN,
					'DBSCAN': Clusterer_DBSCAN}
			clusterer = d[self.nbcomp_determination](obs_times, period, eps, min_samples)
			clusters = clusterer.cluster()
			obs_times_ = []
			map(obs_times_.extend, [i for i in clusters])
			obs_times_.sort()
			nclusters = len(clusters)#r[1]
			offset = clusterer.offset#r[2]


			if runtime_plot:
				clusterer.plot()
				plt.title(u"Détermination du nb de clusters pour " +\
						"\nep %s" % self.episode + \
						"\nperiod: %d" % period + \
						"	round: %d" % cfg.COUNTER_iteration + \
						"\nnb_clusters: %d" % nclusters + \
						"	max dist: %ds" % eps + \
						"	min samples: %d" % min_samples)
				name = "outliers_ep%d_period%d_round%d.%s" \
						% (self.episode.id, period, \
						cfg.COUNTER_iteration, format)
				plotname = '/'.join([self.path, 'outliers', name])
				if interactive:
					plt.show()
					#raw_input("\nPress ENTER to continue")
				else:
					plt.savefig(plotname, bbox_inches='tight')
			logging.debug(' .'*5+' %d'%(len(obs_times)-len(obs_times_))\
					+' obs_times were removed because considered as outliers' )
			
			if not len(obs_times_):
				with open(self.fname, 'a') as f:
					f.write("%d\t-\t%s (no obs_times cluster)\n" \
							% (period, str(EMPTY_PAT_QUAL)))
				logging.debug(" . . . . . no obs_time left, return")
				patterns.append(EMPTY_PAT_QUAL)
			

			# Constaint; all the comps should appear in each period 
			#(or at least in most)
			# So nb_comp < max nb of occurrences of ep in a period
			
			#L = [int(i)/int(period) for i in obs_times_]
			#counts = count_list_items(L)
			#max_ = max(counts[i] for i in counts) 
			#if self.params.max_nbcomp:
			#	max_ = min(max_, self.params.max_nbcomp)
			#
			#logging.debug(' . . . . try the %d possible nbcomp' % (max_))
			#for nbcomp in range(1, max_ +1):
			if self.brute_force:
				if self.max_nbcomp:
					tryme = range(1, int(self.max_nbcomp)+1)
				else:
					tryme = range(1, nclusters+1)
			else:
				tryme = [nclusters,]
			best_pattern = EMPTY_PAT_QUAL
			best_ratio = Ratio(EMPTY_PAT_QUAL)
			for nbcomp in tryme:
				if nbcomp > len(obs_times_):
					break
				obs_times = obs_times_[:]
				pat_qual =  self.what_periodicity_for_period_and_nbcomp(
						obs_times, period, nbcomp, offset, runtime_plot, interactive, format)
				ratio = Ratio(pat_qual)
				if ratio > best_ratio:
					with open(self.fname, 'a') as f:
						f.write(" ↑better pattern for this period\n\n")

					best_ratio = ratio
					best_pattern = pat_qual

			patterns.append(best_pattern)
#			if runtime_plot:
#				import code
#				code.interact(local=locals())
		return patterns


	def what_periodicity_for_period_and_nbcomp(self, obs_times, period, nbcomp, offset, runtime_plot=False, interactive=False, format="png"):
		relative_obs_times = asarray([[float(int(o)%int(period))] for o in obs_times])
		if len(obs_times) <= nbcomp:
			return EMPTY_PAT_QUAL
		nbperiods = (obs_times[-1] - obs_times[0]) / period
		assert all(map(lambda x: isinstance(x[0], float), relative_obs_times))
		g = GMM(nbcomp)
		g.fit(relative_obs_times)
		
		if not g.converged_:
			with open(self.fname, 'a') as f:
				f.write("%d\t%d\t%s (GMM did't converge)\n" \
						% (period, nbcomp, str(EMPTY_PAT_QUAL)))
			return EMPTY_PAT_QUAL
		 
		distrib_per_comp = g.predict(relative_obs_times)
		distrib_per_comp.sort()
		distrib_per_comp = count_list_items(distrib_per_comp)

		description = [(g.means_[i][0]-offset, \
				max([1.0, round(sqrt(g.covars_[i][0]))])) \
				for i in range(nbcomp)]
		logging.debug(" . . . . . raw description: %s" \
				% str([(str_rel_time(i[0], period), i[1]) \
				for i in description]))
		

		# Plot histogram
		if runtime_plot:
			ticks = [i for i in plt.xticks()[0]]
			ticks_labels = [i._text for i in plt.xticks()[1]] # GET THE TEXT!!!
			for i,j in description:
				ticks.extend([i-2*j+offset, i+offset, i+2*j+offset])
				plt.axvspan(i-2*j+offset, i+2*j+offset, color='NavajoWhite')
				ticks_labels.extend(map(lambda x: str_rel_time(x, period), [i-2*j, i, i+2*j]))
			#plt.figure()
			if period == 86400:
				nbins = 24*4 # max tous les quarts d'heure
			else:
				nbins = 24*7 # max toutes les heures
			plt.hist(relative_obs_times, bins=nbins)
			plt.xticks(ticks, ticks_labels, 
					verticalalignment='top', 
					horizontalalignment='right', 
					rotation=30)
			if interactive:
				pl.show()
				print "offset: %d" % offset

			else:
				name = 'hist%s_per%s_ncomp%s_iter%d.%s' % \
						(str(self.episode.id), str(period), str(nbcomp),
								cfg.COUNTER_iteration, format)
				filename = '/'.join(\
						[self.path,'histograms',name])
				plt.savefig(filename)

		# Filter the components
		to_be_rm = []
		for i in range(nbcomp):
			if i not in distrib_per_comp:
				to_be_rm.insert(0, i)
			else:
				if distrib_per_comp[i] < nbperiods*self.minimal_accuracy:
					to_be_rm.insert(0, i)
				elif self.max_stddev and description[i][1] > self.max_stddev:
					to_be_rm.insert(0, i)
				elif description[i][1] > self.default_periods[period]:
					to_be_rm.insert(0, i)

		for i in to_be_rm:
			description.pop(i)
		description.sort(cmp=lambda x, y: cmp(x[0], y[0]))
		logging.debug(" . . . . . filtered description: %s" % \
				str([(str_rel_time(i[0], period), i[1]) for i in description]))

		# get start
		first = None
		for obs in obs_times:
			bk = False
			for comp in description:
#				if abs((int(obs-offset) % period) - comp[0]) < 2*comp[1]:
#					first = obs-offset
#					bk = True
#					break
				if abs((int(obs) % period) - comp[0]-offset) < 2*comp[1]:
					first = obs-offset
					bk = True
					break
			if bk:
				break
		if not first:
			logging.debug(' . . . . . could not find start')
			with open(self.fname, 'a') as f:
				f.write("%d\t%d\t%s (Could not find start)\n" % \
						(period, nbcomp, str(EMPTY_PAT_QUAL)))
				f.write("	%s" % \
						str([(day_and_time(i[0]), i[1]) \
						for i in description]))
			return EMPTY_PAT_QUAL
		start = build_start(first, period)

		# get end
		last = None
		for obs in obs_times[::-1]:
			bk = False
			for comp in description:
				if abs((int(obs) % period) - comp[0]-offset) < 2*comp[1]:
					last = obs-offset
					bk = True
					break
			if bk:
				break
		if not last:
			logging.debug(' . . . . . could not find end')
			with open(self.fname, 'a') as f:
				f.write("%d\t%d\t%s (Could not find end)\n" \
						% (period, nbcomp, str(EMPTY_PAT_QUAL)))
				f.write("	%s" % str([(day_and_time(i[0]), i[1])\
						for i in description]))
			return EMPTY_PAT_QUAL
		end = build_end(last, period)

		# build periodicity
		description_wo_offset = []
		for m, s in description:
			if m < 0:
				description_wo_offset.append((m+period, s))
			else:
				description_wo_offset.append((m, s))

		periodicity = Periodicity(
				period, description_wo_offset, start, end)
		# assess its quality
		pat_qual = self.pattern_quality(periodicity, obs_times, offset)
		if pat_qual:
			assert isinstance(pat_qual.offset, float)
			with open(self.fname, 'a') as f:
				f.write("%d\t%d\t%s\n" % \
						(period, nbcomp, str(pat_qual)))
			return pat_qual
		else:
			#logging.debug(' . . . useless')
			with open(self.fname, 'a') as f:
				f.write("%d\t%d\t%s (too many missings)\n" \
						% (period, nbcomp, str(EMPTY_PAT_QUAL)))
				f.write("	%s" % str([(day_and_time(i[0]), i[1]) \
						for i in description]))
			return EMPTY_PAT_QUAL

	def pattern_quality(self, periodicity, obs_times_, offset):
		obs_times = [i-offset for i in obs_times_]
		obs_times.sort()
		len_d = len(periodicity.description)
		nbperiods = (obs_times[-1] - obs_times[0]) / periodicity.period
		expected_occ_count = nbperiods * len(periodicity.description)
		
		# The maximal number of allowed missing occurrences, in order to 
		# respect the minimal accuracy constraint
		max_missing = expected_occ_count * (1 - self.minimal_accuracy)
		
		def next_exp(iteration, comp):
			return periodicity.start \
					+ iteration * periodicity.period \
					+ periodicity.description[comp][0]
		def update_values(iteration, comp):
			comp = (comp + 1) % len_d
			if comp == 0:
				iteration += 1
			exp = next_exp(iteration, comp)
			return iteration, comp, exp

		tot_obstimes = nbperiods*len_d

		current = obs_times.pop(0)
		iteration = 0
		comp = 0

		as_expected = []
		shifted = []
		extra = []
		missing = []
		shifted_or_extra = []

		while current < periodicity.start:
			extra.append(current)
			current = obs_times.pop(0)
		exp = next_exp(iteration, comp)
		while current < periodicity.end and obs_times:
			if len(missing) > max_missing:
				# The accuracy constraint is broken
				break
			if abs(current - exp) < 2*periodicity.description[comp][1]:
				as_expected.append(current)
				iteration, comp, exp = update_values(iteration, comp)
				current = obs_times.pop(0)
			elif abs(current - exp)<4*periodicity.description[comp][1]:
				if current < exp:
					shifted_or_extra.append((current, exp))
					current = obs_times.pop(0)
				else:
					shifted.append(current)
					iteration, comp, exp = update_values(
							iteration, comp)
					current = obs_times.pop(0)
			elif current < exp:
				extra.append(current)
				current = obs_times.pop(0)
			else:
				missing.append(exp)
				iteration, comp, exp = update_values(iteration, comp)

		else: # The minimal accuracy constraint is respected
			for (seen, exp) in shifted_or_extra:
				if exp in missing:
					missing.remove(exp)
					shifted.append(seen)
				else:
					extra.append(seen)
			extra.append(current)
			extra.extend(obs_times)
			pat_qual = PatternQuality(periodicity, as_expected, 
					shifted, missing, extra)
			logging.debug(' . . . Pattern ' + str(pat_qual))
			pat_qual.offset = float(offset)
			return pat_qual
		return EMPTY_PAT_QUAL

class PatternQuality:
	def __init__(self, periodicity, 
			as_expected, shifted, missing, extra):
		self.periodicity = periodicity
		self.as_expected = as_expected
		self.shifted = shifted
		self.missing = missing
		self.extra = extra

	def __nonzero__(self):
		assert isinstance(self.as_expected, list)
		return len(self.as_expected) > 0

	def __str__(self):
		if not self:
			return "Empty pattern"

		s = 'Periodicity %s\n' % self.periodicity + \
				'\t%d asexpected occurrences\n'%len(self.as_expected)+\
				'\t%d shifted occurrences\n'%len(self.shifted) + \
				'\t%d missing occurrences\n'%len(self.missing) + \
				'\t%d extra occurrences' %len(self.extra)
		return s
	def __repr__(self):
		return str(self)

	def accuracy(self):
		return float(len(self.as_expected)) / (len(self.as_expected) + len(self.missing))

EMPTY_PAT_QUAL = PatternQuality(Periodicity(0, [], 0, 0),  
		[], [], [], [])


class Ratio:
	def __init__(self, pat_qual):
		if not pat_qual:
			self.factorized = 0
			self.accuracy = 0
			self.is_adapted = 0
		else:
			nb_as_expected = len(pat_qual.as_expected)
			nb_missing = len(pat_qual.missing)
			nb_shifted = len(pat_qual.shifted)
			nb_extra = len(pat_qual.extra)
			
			# nb of times the pattern will be factorized 
			# -- the extra entries that'll have to be inserted
			self.factorized = nb_as_expected \
					+ 0.5*nb_shifted \
					- nb_missing
			 
			# what happened as expected / what should have happened
			self.accuracy = float(nb_as_expected + nb_shifted) / \
					float(nb_as_expected + nb_shifted + nb_missing)
			
			# what the pattern describes / all what is observed
			self.is_adapted = float(nb_as_expected + nb_shifted) / \
					float(nb_as_expected + nb_shifted + nb_extra)

	def __cmp__(self, ratio):
		if (self.factorized > ratio.factorized) \
				or (self.factorized == ratio.factorized \
				and self.accuracy > ratio.accuracy):
			return 1
		# TODO: cas de l'égalité
		else: 
			return -1

	def __str__(self):
		s = 'factorized: %f\n' %(self.factorized) +\
				'accuracy: %f\n' %(self.accuracy) +\
				'is_adapted: %f' %(self.is_adapted)
		return s

