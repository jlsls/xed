#!/usr/bin/python

import itertools
from Import import *

#
# Parse arguments
#
parser = Parser()

parser.add_argument('-f', '--file', dest = 'dbname', 
        metavar = 'DATABASE', help = 'load DATABASE',
        required = True)
parser.add_argument('--storing_prefix', 
        help='Directory where to store the results')

arguments = parser.parse_args()


# 
# Parameter ranges to explore
#
episode_lengths = [300, ]#[1, 10, 60, 120, 180, 240, 300, 400, 500, 600, 900, 1200, 2400, 3600]
minimal_frequencies = [4, ]#[15, 12, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
episode_capacities = [1, 2, 3, 4, 5, 6, 7, 8]
min_accuracies = [0.5]#[1, 0.75, 0.5]
max_stddevs = [None]#[1, 60, 900, 1800, 3600, 7200]

p = Parameters()
p = parse_config_file('data/parameters.xml')

 
#
# Init the storing repertories + read dataset
#
with open(arguments.dbname, 'r') as f:
    data = general_tools.dataset.parser.read_dataset(f)
if arguments.storing_prefix:
    p.general['storing_prefix'] = os.path.join(arguments.storing_prefix, data.name)
else:
    p.general['storing_prefix'] = os.path.join(p.general['storing_prefix'], data.name)
if os.access(p.general['storing_prefix'], os.F_OK):
    #assert False # TODO collison handling 
    pass
else:
    os.makedirs(p.general['storing_prefix'])


# 
# Explore the parameter ranges
#

param_file = open(os.path.join(p.general['storing_prefix'], 'parameters.data'), 'w')
params = ['episode_length', 'minimal_frequency', 'episode_capacity', 'minimal_accuracy', 'max_stddev']
param_file.write('# ' + '\t'.join(params) + '\n')

exec_time_file = open(os.path.join(p.general['storing_prefix'], 'execution_time.data'), 'w')
exec_time = ['time this round', 'time for frequent episode generation', 
        'time for episode analysis', 'time for factorization sorting',
        'time for factorizing']
exec_time_file.write('# ' + '\t'.join(exec_time) + '\n')

fq_ep_file = open(os.path.join(p.general['storing_prefix'], 'frequent_episodes.data'), 'w')
fq_ep = ['minimum', 'maximum', 'average', 'median'] 
fq_ep_file.write('# ' + '\t'.join(fq_ep) + "\n")

data_char_file = open(os.path.join(p.general['storing_prefix'], 'data_characteristics.data'), 'w')
data_char_1 = ['data length before this round']
data_char_2 = ['data length after this round', 'event count in data', 'factorization count in data']
data_char_file.write('# ' + '\t'.join(data_char_1 + data_char_2) + '\n')

go_file = open(os.path.join(p.general['storing_prefix'], 'generated_objects.data'), 'w')
go = ['frequent episode count', 'candidate factorization count']
go_file.write('# ' + '\t'.join(go) + '\n')

for el, mf, ec, ma, ms in itertools.product(episode_lengths, minimal_frequencies, episode_capacities, min_accuracies, max_stddevs):
    P = deepcopy(p)
    P.candidate_generation['episode_length'] = el
    P.candidate_generation['minimal_frequency'] = mf
    P.candidate_generation['episode_capacity'] = ec
    P.pattern_quality['minimal_accuracy'] = ma
    P.periodicity_determination['max_stddev'] = ms
    param_file.write('\t'.join(str(i) for i in [el, mf, ec, ma, ms]) + '\n')

    d = deepcopy(data)
    m = complete_mining(P, d)

    et = {}
    for ex_t in exec_time:
        et[ex_t] = sum(m.logger[i+1][ex_t] for i in range(m.round_count) if m.logger[i+1][ex_t] != 'N/A')
    exec_time_file.write('\t'.join(str(et[ex_t]) for ex_t in exec_time) + "\n")

    fq_ep_file.write('\t'.join(str(m.logger[1]['frequent episodes length characteristics'][i]) for i in fq_ep) + '\n')

    for i in data_char_1:
        data_char_file.write(str(m.logger[1][i]) + '\t')
    for i in data_char_2:
        data_char_file.write(str(m.logger[m.round_count][i]) + '\t')
#    for i in data_char_3:
#        data_char_file.write(str(m.logger[1][i]) + '\t' + str(m.logger[m.round_count][i]) + '\t')
    data_char_file.write('\n')

    go_file.write('\t'.join(str(m.logger[1][i]) for i in go) + '\n')


param_file.close()
exec_time_file.close()
fq_ep_file.close()
data_char_file.close()
go_file.close()
