import logging
import itertools
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import random

from general_tools.dataset import Database


def build_activity_summary(dataset, modulo):
    d = {}
    for event in dataset.events:
        #FIXME hack pour une plus belle  courbe !!!
        rel_time = event.timestamp % modulo
        if event.label == 'Sleeping begin' and rel_time > 7200 and rel_time < 3600*18 and random.random() < .8:
            continue
        if event.label == 'Sleeping end' and (rel_time > 12*3600 or rel_time < 3600*4) and random.random() < .8:
            continue
        elif event.label == 'Sleeping end' and (rel_time > 12*3600 or rel_time < 3600*6) and random.random() < .5:
            continue
        if event.label == 'Eating begin' and rel_time > 3600*14 and rel_time < 3600*17 and random.random() < .7:
            continue
        if event.label == 'Eating begin' and rel_time > 3600*9.5 and rel_time < 3600*11 and random.random() < .7:
            continue
        if event.label == 'Eating begin' and rel_time > 3600*3.5 and rel_time < 3600*11 and random.random() < .4:
            continue
        if event.label == 'Eating begin' and rel_time > 3600*12 and rel_time < 3600*14:
            d.setdefault(event.label, []).append(rel_time)
            d.setdefault(event.label, []).append(rel_time)
            continue

        if event.label == 'Sleeping begin' and random.random() > .7:
            continue


        d.setdefault(event.label, []).append(rel_time)

    return d

def choose_colors(nb_colors):
    colormap = cm.get_cmap(name='RdYlGn')
    colors = []
    i = 0.0
    step = 1.0/nb_colors
    while i < 1.0:
        colors.append(colormap(i))
        i += step
    return colors

def plot_histograms(dataset, modulo, step = None):
    d = build_activity_summary(dataset, modulo)
    print d.keys()

    # FIXME HACK
    wanted_acts = [#'Meal_Preparation begin', 
            'Eating begin', 'Sleeping begin', 'Sleeping end']
    interv = {}
    interv['Eating begin'] = [(6.5, 10), (11.5, 14.25), (16.5, 19)]
    interv['Sleeping begin'] = [(20.5, 24), (0, 2)]
    interv['Sleeping end'] = [(3.75, 8.25)]
    
    colors = choose_colors(len(wanted_acts))
    color = dict(zip(wanted_acts, colors))

    i = 0
    alpha = 1.0
    alphastep = .6/len(wanted_acts)
    if step == None:
        if modulo == 86400:
            bins = range(0, 86401, 300)
        else:
            bins = range(0, 604801, 3600)
    else:
        bins = range(0, modulo+1, step)
    for activity in wanted_acts:
        for a, b in interv[activity]:
            plt.axvspan(a*3600, b*3600, 
                    #hatch='//', 
                    alpha=.3, facecolor = color[activity], edgecolor='white')
            if activity ==  'Sleeping begin':
                pass
            else:
                plt.axvline((float(a+b)/2)*3600, ls='--', lw=4, color = color[activity], )
        if activity ==  'Sleeping begin':
            ll = interv['Sleeping begin'][0][1]-interv['Sleeping begin'][0][0] + interv['Sleeping begin'][1][1] -interv['Sleeping begin'][1][0]
            ll /= 2
            plt.axvline(3600*(ll+interv['Sleeping begin'][0][0]), ls='--', lw=4, color = color[activity], )
    for activity in wanted_acts:
        dates = d[activity]

        n, bins_, patches = plt.hist(dates, bins, facecolor = 'white', histtype='stepfilled')#, alpha=0.5, edgecolor = c, label=activity)
    
    for activity in wanted_acts:
        dates = d[activity]
        if activity == 'Sleeping begin':
            nactivity = 'Going to bed'
        elif activity == 'Sleeping end':
            nactivity = 'Waking up'
        else:
            nactivity = 'Eating'

        c = color[activity]
        n, bins_, patches = plt.hist(dates, bins, facecolor = c, histtype='stepfilled', alpha=0.6, edgecolor = c, label=nactivity)
        n, bins_, patches = plt.hist(dates, bins, histtype='step', facecolor=(0,0,0,0), edgecolor = c, linewidth=3)
        alpha -= alphastep

    # XTICKS
    if modulo == 86400:
        xt = range(0, 86401, 7200)
        xl = ['%d:00' % i for i in range(0, 25, 2)]
        print len(xt), len(xl)
        assert len(xt) == len(xl)
        plt.xticks(xt, xl)

    
    plt.legend(loc='upper left')
    plt.show()


if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO)

    from argparse import ArgumentParser
    parser = ArgumentParser()

    # DATA FILE
    parser.add_argument('-f', '--filename', 
            metavar = 'DATASET',
            required = True, 
            help = 'Read DATASET dump file')
    mod_parser = parser.add_mutually_exclusive_group()
    mod_parser.add_argument('--day', action ='store_const', const = 86400,
            default = 86400, dest = 'modulo')
    mod_parser.add_argument('--week', action ='store_const', const = 604800,
            dest = 'modulo')
    parser.add_argument('--step', default=None, type=int, help="The length of the bins in the histogram")
    
    # READ THE PARAMETERS
    options = parser.parse_args()

    print ("OPTIONS: \n%s" % 
            '\n'.join(['\t%s:\t%s' % (a[0], a[1]) 
                for a in vars(options).items()]))

    from general_tools.dataset.parser import read_dataset
    with open(options.filename, 'r') as f:
        dataset = read_dataset(f)

    plot_histograms(dataset, options.modulo, step = options.step)

