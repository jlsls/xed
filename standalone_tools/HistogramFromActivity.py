import sys
import datetime

import logging
import random
import numpy
import csv

import standalone_tools.MixtureModelGenerator_FromData as FD
from DB_Utils import db_connection, list_event_types


#import matplotlib.pyplot as plt
#import matplotlib.cm as cm

def draw_histograms(names, series, period=86400):
	plt.ion()
	set_names = list(set(names))
	set_names.sort()
	colors = [cm.get_cmap("brg")(random.random()) for i in range(len(set_names))]
	dulled_c = [(c[0], c[1], c[2], 0.5) for c in colors]
	print colors

	hatches = ['/',  '\\', 'o',  'O',  '.',  '*']
	looks = []

	plt.figure()
	if period == 86400:
		bin_width = 1800
		bins = [i*bin_width for i in range(0, period/bin_width)]
		xticks = [i*3600 for i in range(0, 25, 3)]
		xlabels = ["%d:00" % i for i in range(0, 25, 3)]

	for i in range(len(set_names)):
		name = set_names[i]
		color = colors[i]
		color2 = dulled_c[i]
		hatch = hatches[i]
	
		x = []
		for j in range(len(names)):
			if names[j] == name:
				x.extend(series[j])
		_, _, patches = plt.hist(x, bins=bins, facecolor=color2, edgecolor="black", histtype='stepfilled', label=name, hatch=hatch, alpha=0.5)
		_, _, patches = plt.hist(x, bins=bins, edgecolor=color, histtype='step', linewidth = 4 )
		print bins
	
	plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
			           ncol=3, mode="expand", borderaxespad=0.)
	plt.xticks(xticks, xlabels, rotation=30)
	plt.show()

def read_episode_file(episode_file):
    episodes = []
    if episode_file:
        with open(episode_file, 'r') as f:
            csv_reader = csv.read(f)
            for row in csv_reader:
                episodes.append(row)
    return episodes

def get_occurrences_for_episode(cursor, episode, maximal_episode_duration, table="EVENT_VIEW"):
    FD.exec_select_relevant_events_from_labels(cursor, episode, table)
    TQ = FD.get_occurrences(cursor, maximal_episode_duration)
    return TQ

def make_histogram(TQ, bins, period, range=None, density=False):
    TQ1 = map(lambda x: FD.datetime2unix(x) % period, TQ)
    hist, bin_edges = numpy.histogram(TQ1, bins, range=range, density=density)
    return hist, bin_edges




if __name__ == "__main__":
	from argparse import ArgumentParser
	parser = ArgumentParser()

	parser.add_argument("-d", "--dataset", help="(mysql) dataset to be used for the data generation", required=True)
        parser.add_argument("-t", "--table", help="Table where the events are stored. The table is expected to have a `label` and a `timestamp` column", default="EVENT_VIEW")
	parser.add_argument("-o", "--output", help="destination file", default="/tmp/hist.txt")

	parser.add_argument("--maximal_episode_duration", type=int, default=1800)
	parser.add_argument("--period", type=int, default=86400)

	parser.add_argument("-v", "--verbose", action="store_true", help="Verbose")
        parser.add_argument("-l", "--episode_file", help="Restrict the analysis to the episodes in `EPISODE_FILE`. Expected format: csv-like, one line per episode", default=None)
        parser.add_argument("-b", "--bins", type=int, default=10, help="Number of bins in the histogram")

	options = parser.parse_args()

	if options.verbose:
		logging.basicConfig(level=logging.DEBUG)
	else:
		logging.basicConfig(level=logging.INFO)

	logging.info("\n\t".join([" Parameters used", ] + ["%s: %s" % (k, str(v)) for k, v in options.__dict__.items()]))
	
        cursor = db_connection(options.dataset)
        episodes = read_episode_file(options.episode_file)
        if not episodes:
            # Default behaviour: consider all the labels
            query = """ SELECT DISTINCT label FROM %s """ % options.table
            cursor.execute(query)
            for label in cursor:
                episodes.append(label)
            pass

        results = numpy.zeros((options.bins, len(episodes)+2))
        # 1 row = values for 1 bin
        # 1 column = values for 1 episode
        # 1st row: header
        # 1st and 2nd columns: edges of the bins
        first_time = True
        for e, episode in enumerate(episodes):
            print episode
            TQ = get_occurrences_for_episode(cursor, episode, options.maximal_episode_duration, options.table)
            hist, bins = make_histogram(TQ, options.bins, options.period, range=(0, options.period), density=False)
            print hist
            print bins
            if first_time:
                first_time = False
                for i in xrange(len(bins)-1):
                    results[i][0] = bins[i]
                    results[i][1] = bins[i+1]
            for i, h in enumerate(hist):
                results[i][e+2] = h

        with open(options.output, 'w') as f:
            writer = csv.writer(f)
            row = ["bin_begin", "bin_end"]
            row.extend(map(lambda x: '-'.join(x), episodes))
            writer.writerow(row)
            for row in results:
                writer.writerow(row)
	raw_input("ENTER to exit")
