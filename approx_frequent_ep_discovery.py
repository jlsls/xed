#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Lattice des episode fréquents de la dernière fenêtre approximatifs. 
Objectif: pour chaque noeud, ne garder qu'un support approx, une MO (la dernière), la date de fin de la dernière NOMO
Stratégie: support approximatif, calculé à l'aide d'un décay factor. 
    Support_t = Support_t- * max(0, T_win - (t - t-))/Twin + 1
    ie: si t - t- = 0: on ajoute 1
        si t - t- >= Twin: on reset à 1

    Parcours du treillis similaire à xED_stream, avec des RM nodes, qui pourraient être liés via linkedlist.

Pbm:            pour voir qu'un épisode devient fréquent, j'ai besoin de 2 TQ de sous épisodes. Hors je ne veux pas les garder. 
Proposition:    si les subep deviennent (sub) fréquents, alors je commence à compter le support approximatif de l'ep.
Conséquences:   en T_win max, le support approximatif s'approche du support réel.
                en O(T_win * f_sub/f_freq * (len(E) -1)), je commence à m'intéresser à E

Pbm2: Outdated episodes
Probosition: si f(E) < f_rare: on supprime le noeud et sa descendance. Attention à ne pas supprimer un noeud qui est devenu intéressant depuis moins de T_win.

TODO: 
    * implem de tout ça
    * comparer support réel et support approximatif: écart absolu et temps de propagation de l'information
    * s'assurer que ça n'a pas déjà été fait...
    * taille de la lattice exacte/approx nb de noeuds vs conso mémoire
    * temps de mise à jour exact / approx
"""

import sys
import os
import logging 
import time
import datetime
import random

from Utils import datetime2relativeposition, datetime2roundeddatetime
import init_log

class NodeApprox:
    """
    Node structure for the approximate discovery of frequent episodes in the 
    FEL. Contains data on the (sub-)frequent episode, links with the others 
    nodes in the lattice, and the necessary data for (approximate) support 
    computation

    Episode and mapping attributes:
        episode: tuple, contains the labels composing the episdode
        level: int, the depth of the node in the lattice
        parent: dict, indexed on the labels in self.episode. Maps the node to 
            its parents
        child: dict, indexed on labels (not in self.episode). Maps the node 
            to its children
        next_RMN: link to the next node in the RMN linked list

    Support-related attributes:
        creation_date: datetime.datetime, used for marking newly investigated 
            episodes. Expected purpose: give them enough time to get a stable 
            support value.
        approx_support: float, suport value
        last_mo: the start and end timestamps (datetime.datetime objects) of 
            the last observed minimal occurrence. Purpose: build the next 
            occurrence.
        last_nomo: the start and end timestamps (datetime.datetime objects) of 
            the last observed minimal occurrence, that does not overlap with 
            the previous NOMO. Purpose: support computation.
        last_support_update: datetime.datetime, marking the time the support 
            was updated. Necessary to allow support update even if no 
            occurrence is observed.

    """
    def __init__(self, episode=tuple(), level=0):
        """
        NodeApprox constructor

        Parameters:
            episode: tuple of str, optional, defaults to tuple(). The labels 
                making the episode
            level: int, optional, defaults to 0. The depth of the node in the 
                lattice. Should equal len(self.episode).
        """
        # Node in the lattice
        self.episode = episode
        self.level = level
        assert self.level == len(self.episode)
        self.parents = {}
        self.children = {}
        self.next_RMN = None

        # Support data
        self.creation_date = None # pour ne pas supprimer un recemment créé
        self.approx_support = 0
        self.last_mo = None
        self.last_nomo = None
        self.last_support_update = None

    
    def build_child(self, extra_label):
        """
        Create an episode, addig `extra_label` to self.episode. Create the 
        corresponding child node, and link that node to all of its parents 
        (which should exist before the call to link_to_parents...)

        Parameter:
            extra_label: str, the label which should be added to self.episodes 
                to make up the new episode

        Return:
            the new node
        """
        child = NodeApprox(episode = self.episode + (extra_label, ), level = self.level + 1)
        child.link_to_parents(self, extra_label)
        return child

    def link_to_parents(self, known_parent, linking_label):
        """
        Knowing one parent and the label bridging the episodes, link the 
        current node to its other relatives in the lattice. 
        
        Remark: The parents look like 
            known_parent.parent[some_label].child[linking_label], where 
            some_label is a label in self.episode and some_label != linking_label

        Parameters:
            known_parent: one of self's parents. No previous link between self 
                and known_parent is needed
            linking_label: the label differentiating self from known_parent

        Returns: nothing
        """
        self.parent[linking_label] = known_parent
        known_parent.child[linking_label] = self
        for label in self.episode:
            if label == linking_label:
                continue
            else:
                assert label in known_parent.parent
                grand_parent = know_parent.parent[label]
                other_parent = grand_parent.child[linking_label]
                self.parent[label] = other_parent
                other_parent.child[label] = self


    def update_with_new_occurrence(self, occurrence):
        """
        Update the support, based on the incoming occurrence

        Decay the current support (multiply with 
            timedelta_since_last_update / self.window_length), add new 
            occurrence. Also update the self.last_mo / self.last_nomo (if 
            relevant) / self.last_support_update. 

        Parameter:
            occurrence: (datetime.datetime, datetime.datetime), the start and 
                end timestamps of the new occurrence to consider. Must be an 
                actual mo of the episode. Since no check is made here, the 
                validity of the MO should be checked before this call. 
                However, the nomo property is checked.
        """
        _, end = occurrence
        if not self.last_nomo or end > self.last_nomo[1]:
            decay = max(0, window_length + self.last_nomo_end - end) / window_length
            self.approx_support = self.approx_support * decay + 1.0
            self.last_nomo = occurrence
            self.last_support_update = end

        self.last_mo = occurrence
        return

    def update_without_new_mo(self, updatetime):
        # TODO: allow support update without new occurrence (e.g. because a node becomes old)
        pass

class FELApprox:
    """
    Frequent episode lattice class, with approximate support computation

    Attributes:
        root: NodeApprox, the root of the lattice

    Support threshold attributes:
        freq_support: float, the support threshold spliting frequent episodes 
            from subfrequent episodes
        subfreq_support: float, the support threshold spliting subfrequent 
            episodes from rare episodes. Purpose: starts the investigation of 
            the children of a promissing subfrequent episode before it becomes 
            frequent, in order to propagate faster in the lattice.
        rare_support: float, the support threshold spliting interesting 
            episodes from non-interesting episodes. Purpose: remove nodes from 
            lattice, if their episode (which was once promissing) becomes too 
            rare
        window_length: datetime.timedelta, the time window of interest = the 
            recent past. Occurrences older than window_length are forgotten, 
            and approximate support follows timedelta_since_last_update / 
            window_length.
        max_ep_duration: datetime.timedelta, the maximal duration of the 
            episode occurrences. Mandatory in xED / xED_stream, should not be 
            necessary here thanks to the better memory management.
        max_depth: int, maximal depth of the lattice. If None, no bound is set.
    """

    def __init__(self, 
            freq_support        = 5.0, 
            subfreq_support     = 3.0, 
            rare_support        = 3.0, 
            window_length       = datetime.timedelta(days=7), 
            max_ep_duration     = datetime.timedelta(minutes=30),
            max_depth           = None):
        """
        FELApprox constructor. Builds the root of the lattice, stores the 
        mining parameters.

        Paramaters:
            freq_support
            subfreq_support
            rare_support
            window_length
            max_ep_duration
            max_depth
        """
        self.build_root()

        # Quelle struct? une liste, indexée sur les niveaux, avec à chaque fois une tête de liste?
        self.RMN_lists   = []

        self.freq_support       = freq_support
        self.subfreq_support    = subfreq_support
        self.rare_support       = rare_support
        self.window_length      = window_length
        self.max_ep_duration    = max_ep_duration
        self.max_depth          = max_depth

    def build_root(self):
        """
        (Re-)Initialize the root of the lattice
        """
        # FIXME: a bit useless now, but might prove useful in the future
        self.root = NodeApprox()

    def new_event(self, event):
        """
        Update the structures with the incoming event

        Parameter:
            event: dataset.Event object, the new event
        """
        label = event.label
        timestamp = event timestamp

        # 1. Update the length-1 episode
        if label not in self.root.children:
            child = self.root.build_child(label)
        else:
            child = self.root.child[label]
        if child.last_mo != (timestamp, timestamp):
            child.update_with_new_occurrence((timestamp, timestamp))
            # otherwise: twice the same timestamp. A bit useless.

        # 2. RMN traversal: try to extend the node. Don't forget to update the RMN linked list 
        for depth in itertools.irange(len(self.RMN)):
            RMnode = self.RMN[depth)
            while RMnode:
               
                # 2a. The node is too old -> check if becoming rare, update RMN
                # 2b. The node contains label -> skip
                # 2c. The node is (sub)frequent -> check its kids (FIXME, not sure what is happening exactly)
                # 2d. The node becomes rare -> delete it

                RMNnode = RMnode.RMN_next
        

if __name__ == '__main__':
    import argparse
    import MySQLdb
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # DATA FILE
    parser.add_argument('-d', '--dbname', 
            metavar = 'DATASET',
            required = True, 
            help = 'Read DATASET from mysql server')
    parser.add_argument('-t', '--table_name', default="EVENT", 
            help="Name of the table where the events are stored. Should have a 'timestamp' and a 'label' columns")

    parser.add_argument('-v', '--verbose', 
            action = 'store_true', 
            help = "Set logging to debub mode")

    parser.add_argument('--maximal_episode_duration', default = 1800, type=int, help="Maximal duration in seconds of the episode occurrences")
    parser.add_argument('--minimal_support', default = 5, type=int, help="Minimal count of NOMOs")
    parser.add_argument('--window_length', default=604800, type=int, help="Duration (in seconds) of the window")
    parser.add_argument('--maximal_depth', default=None, type=int, help="Maximal depth of the built lattice (NOT IMPLEMENTED YET!)") # TODO


    parser.add_argument('--ncomp', type=int, default=1,
            help="the initial number of components in the model")
    parser.add_argument("--max_ncomp", type=int, default=None,
            help="Sets an upper bound on the number of components in the mixture " +
            "model. Once the limit is reached, no new component can be created. " +
            "When it is set to None, the number of components is not constrained"
            )
    parser.add_argument("--merge_comp", default=1.5,
            nargs = '?', const=1.5, type=float,
            help="Set to enable the merging of components (highly recommended). " +
            "Default = Flag without value: merge if abs(m1-m2)<1.5*(s1+s2). " +
            "Flag with value: change the 1.5 coeff for MERGE_COMP.")
    parser.add_argument("--rm_comp", action='store_true', default=True,
            help="allows (or not) the deletion of components (NOT USED, FIXME)")
    parser.add_argument("--add_comp", default=1.0, const=1.0, nargs='?', type=float,
            help="Set to enable the addition of new components. " +
            "Also specifies how often new components are added: a random number in " +
            "[0.0, 1.0( is generated. If it is lower than ADD_COMP, then the new " + 
            "component is added. Default = Flag without value: every " + 
            "new datapoint can generate a new component (ADD_COMP = 1.0). ")
    parser.add_argument("--smart_add_only", action='store_true', default=True,
            help="When a new datapoint arrives, create a component for it only if it " +
            "is not close to any of the already existing components. A data " +
            "point x is close to a component (mean m, stddev s) if |m - x| < s. ")

    parser.add_argument("--lenXmax", type=int, default=None,
            help="The maximal length of the data in memory. Forces to drop old data " +
            "entries when too many new data points arrive. When it is equal to " +
            "None, there is no bound. ")

    parser.add_argument("--reset_rate", type=int, default=None,
            help="How often further investigation shoud be conducted (do as many EM " +
            "passes as necessary, instead of just one in the simple update)." +
            "When None, no bigger update is done")

    parser.add_argument("--toggle_plot_on", action="store_true", default=False,
            help="Plot the results of the mining")

    parser.add_argument("--period", default = 86400, type=int,
            help = "The period to consider, in seconds. Default: one day.")
    #parser.add_argument("--periods", nargs='+', default = [86400, ], 
    #       help = "The periods to consider, in seconds. Default: one day, if you want several periods to be analyzed, just list them, separated by days")

    parser.add_argument("--log_directory", default="/home/julie/Documents/xED/exec_results")

    ## READ THE PARAMETERS
    options = parser.parse_args()

    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
        logging.info(" DEBUG MODE ON")
    else:
        logging.basicConfig(level=logging.INFO)
        logging.info(" info MODE ON")

    logging.info("OPTIONS: \n%s" % 
            '\n'.join(['\t%s:\t%s' % (a[0], a[1]) 
                for a in vars(options).items()]))

    logpath = init_log.from_options(options, name='xED_stream')

    database = MySQLdb.connect(host="localhost",
            user="julie", passwd="julie", db=options.dbname)
    cursor = database.cursor()
    cursor.execute("SELECT timestamp, label FROM %s ORDER BY timestamp" % options.table_name)

    mme_params = {
            "ncomp": options.ncomp,
            "rm_comp": options.rm_comp,
            "add_comp": options.add_comp,
            "merge_comp": options.merge_comp,
            "max_ncomp": options.max_ncomp,
            "smart_add_only": options.smart_add_only,
            "lenXmax": options.lenXmax,
            "reset_rate": options.reset_rate,
            "toggle_plot_on": options.toggle_plot_on,
            }

    el = EpisodeLattice(
            minimal_support             = options.minimal_support, 
            maximal_episode_duration    = datetime.timedelta(seconds=options.maximal_episode_duration), 
            maximal_depth               = options.maximal_depth,
            window_length               = datetime.timedelta(seconds=options.window_length),
            mme_params                  = mme_params,
            period                      = options.period,
            log_directory               = logpath)

    i = 0
    for timestamp, label in cursor:
    #for event in dataset.events:
        i += 1
        event = Event(timestamp, str(label))
        if (i%1000) == 0:
            print "%d events (%s)" % (i, timestamp)
        #el.new_event_not_stream(event)
        el.new_event(event, eid=i)


    #el.build_lattice()
    #logging.info(" Lattice built")
    #logging.info(" %d frequent episodes" % el.n_fq_episodes)

    logging.info(" %d events" % i)
    #logging.info(" Streaming verion: %s nodes" % len(el))


    #import code
    #code.interact(local=locals())
#   el.check_apriori_property()
    #el.compare_results()

