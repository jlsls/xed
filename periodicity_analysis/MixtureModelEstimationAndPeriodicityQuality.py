import random, math
import itertools
import matplotlib as mpl
import numpy as np
import os.path
import logging
import datetime
import csv

class MixtureModelEstimator:
    def __init__(self, 
            ncomp            = 1, 
            merge_comp        = 1.5, 
            rm_comp            = False, 
            add_comp        = 1.0, 
            max_ncomp        = None, 
            smart_add_only    = True, 
            lenXmax            = None,
            reset_rate        = None,
            max_iter        = None,
            logging_rep        = "/tmp",
            toggle_plot_on    = False,
            colormap        = "jet",
            ):


        # Window
        self.lenXmax = lenXmax
        self._rawX = []
        self._X = []
        self._lenX = 0

        # Components
        self.comp_names = []
        self.means = {}
        self.stddevs = {}
        self.comp_attribution = []
        self.proportion = {}

        # Component number variation
        self.ncomp = ncomp
        self.max_ncomp = max_ncomp
        self._next_id = 0
        self.merge_comp = merge_comp
        self.rm_comp = rm_comp
        self.add_comp = add_comp
        self.smart_add_only = smart_add_only

        self.reset_rate = reset_rate
        self._time_since_reset = 1
        self.max_iter = max_iter

        # Quality measures
        self.accuracy = []
        self.recall = []
        self._as_exp = []
        self._missing = []

        self.toggle_plot_on = toggle_plot_on
        if self.toggle_plot_on:
            self.colors = {}
            self.colormap = mpl.cm.get_cmap(name="jet")
            # For plot drawing
            self.nbins = 25
            mpl.pyplot.ion()
            self._fig, self._ax = mpl.myplot.subplots(1, 1)

        # For quality measures
        self._nshifts = 0
        self._npts = 0

        self._c_attr = []
        self._t_per_c = []
        self._c_chars = {}
        self._c_start = {}

    def reset_component(self, data=[]):
        """
        Update the characteristics of the mixture model with data. 
        
        If it is the first time EM is applied, the components are initialized. 
        Otherwise, the new data is assumed to be very close to the previous one 
        (the same, without some old data points, and with a few new entries). 
        The characteristics of the components are tweeked until they converge. 
        Depending on the parameters set for deletion/merging of components, the 
        list of components is simplified. 

        Parameters:
            data: list of floats, optional, defaults de []
            The data points. They are stored in self._X. If nothing is set,
                the currently stored data is used.
        """

        #
        # 1. Handle the data 
        #
        if not data:
            data = self._X

        # If data is too long...
        lenX = len(data)
        if self.lenXmax and lenX > self.lenXmax:
            del data[:lenX-self.lenXmax] 

        self._rawX = data
        self._X = [datetime2reltime(x) for x in data]
        self._lenX = len(self._X)
        self.comp_attribution = [None for i in xrange(self._lenX)]

        #
        # 2. Initialization
        #
        if not self.means:
            # Pick at random ncomp vectors in X
            for i in random.sample(self._X, self.ncomp):
                self.create_component(i)

        #
        # 3. Iterate EM until it converges (or the maximal number of iterations is 
        # reached)
        #
        changed = True
        iteration = 0
        while changed and (not self.max_iter or iteration <= self.max_iter): #TODO: max niter
            iteration += 1
            changed = False

            # Expectation:
            self.evaluate()

            if self.toggle_plot_on:
                self.plot_histogram()

            # Maximization
            # estimate for mean: 1/n * sum(xi)
            # estimate for variance: 1/(n-1) * sum( (xi-mean) ** 2 )

            for i in self.comp_names[:]:
                mean, stddev = self.means[i], self.stddevs[i]
                self.maximize(i)
                
                if (i in self.comp_names and 
                        (mean != self.means[i] or stddev != self.stddevs[i])):
                    changed = True

        # 
        # 4. Apply the heuristics for the component number reduction
        #
        if self.merge_comp:
            self.merge_components()

        #if self.rm_comp:
        #    self.remove_components() # TODO

        if self.toggle_plot_on:
            self.plot_histogram()

        return

    def create_component(self, default_mean=random.randint(0, 86399)):
        """
        Initializes a new component, centrered on a default_mean
        """
        # components indexed on the successive values of self._next_id
        self.means[self._next_id] = default_mean
        self.stddevs[self._next_id] = 1e-2
        self.proportion[self._next_id] = 1.0/self.ncomp
        if self.toggle_plot_on:
            self.colors[self._next_id] = self.colormap(random.random())
        self.comp_names.append(self._next_id)

        self._next_id += 1

    def update_mixture(self, new_data=[], ndrop=0):
        """
        Update the characteristics of the mixture model with new_data. 
        
        The new data is assumed to be very close to the previous one (the same, 
        without some old data points, and with a few -nnew- new entries). If 
        add_comp is enabled, new components are tested, according to the  
        settings for new components. Only one pass of the EM algorithm is done.

        Parameters:
            new_data: list of floats, optional, defaults to []
                The new data points. They are appended at the end of self._X
            ndrop: int, optional, defaults to 0
                The number of nom obsolete data points. They are removed from 
                the head of the self._X list.
        """
        nnew = len(new_data)
        
        # drop outdated
        del self._X[:ndrop]
        del self._rawX[:ndrop]

        # drop extra items, if there are too many
        ndrop_extra = 0
        while self.lenXmax and self._lenX + nnew - ndrop - ndrop_extra > self.lenXmax:
            del self._X[0]
            del self._rawX[0]
            ndrop_extra += 1

        # add the new occurrences
        self._rawX.extend(new_data)
        self._X.extend(map(datetime2reltime, new_data))
        self._lenX += nnew - ndrop - ndrop_extra

        self._nshifts += ndrop + ndrop_extra
        self._npts = self._nshifts + self._lenX

        # Fast update of comp_attribution
        del self.comp_attribution[:ndrop+ndrop_extra]
        self.comp_attribution.extend([None for i in xrange(nnew)])

        if self.add_comp:
            for x in self._X[-nnew:]:
                if self.max_ncomp and self.max_ncomp <= self.ncomp:
                    # Already reached max component count
                    break
                if random.random() < self.add_comp:
                    if self.smart_add_only:
                        # Decide if it is "smart" to add a new component
                        for key in self.means:
                            m, s = self.means[key], self.stddevs[key]
                            if abs(m - x) < s:
                                ok = True
                                break
                        else:
                            ok = False
                    else:
                        ok = True

                    if ok:
                        self.create_component(x)
                        self.ncomp += 1
        
        if self.reset_rate and self._time_since_reset == self.reset_rate:
            self.reset_component()
            self._time_since_reset = 1

        else:
            self._time_since_reset += 1
            self.evaluate()
            
            for i in self.comp_names[:]: # Maximize peut supprimer des comp, donc on copie !
                self.maximize(i)

            if self.merge_comp:
                self.merge_components()

            #if self.rm_comp:
            #    self.remove_components()

            if self.toggle_plot_on:
                self.plot_histogram(spec=new_data)

        # Logging !
        for i in xrange(self._lenX):
            while i + self._nshifts >= len(self._c_attr):
                self._c_attr.append([None for j in xrange(self._nshifts+i)])
                self._t_per_c.append({})
            c = self.comp_attribution[i]
            self._c_attr[i+ self._nshifts].append(c)
            
            if c not in self._t_per_c[i + self._nshifts]:
                self._t_per_c[i + self._nshifts][c] = 1
            else:
                self._t_per_c[i + self._nshifts][c] += 1

        for c in self.comp_names:
            if c not in self._c_chars:
                self._c_chars[c] = []
                self._c_start[c] = self._nshifts + self._lenX
            self._c_chars[c].append((self.means[c], self.stddevs[c], self.comp_attribution.count(c)))


    def evaluate(self):
        """
        Attribute each data point to the component that is most likely to be 
        the one that generated it (the one with the closest average). 
        This function updates the self.comp_attribution list.
        """
        for i in xrange(self._lenX):
            x = self._X[i]
            if self.comp_attribution[i] != None:
                distance = abs(self.means[self.comp_attribution[i]] - x)
            for j in self.comp_names:
                mean = self.means[j]
                sd = self.stddevs[j]
                d = abs(mean-x)
                #d = -1.0/sd * math.exp(-0.5*(mean-x/sd)**2)

                if self.comp_attribution[i] == None or d < distance:
                    distance = d
                    self.comp_attribution[i] = j

    def maximize(self, comp_id):
        """
        Update the characeteristics (mean, stddev) of a component. Deletes empty components.

        Parameters:
            comp_id: int, the id of the component to update
        """
        X = [self._X[i] for i in xrange(self._lenX) 
                if self.comp_attribution[i] == comp_id]
        if len(X) == 0:
            del self.means[comp_id]
            #del self.colors[comp_id]
            del self.stddevs[comp_id]
            del self.proportion[comp_id]
            self.comp_names.remove(comp_id)
            self.ncomp -= 1
        elif len(X) == 1:
            self.means[comp_id] = X[0]
            self.stddevs[comp_id] = 1e-2
            self.proportion[comp_id] = 1.0 / self._lenX
        else:
            mean = float(sum(X)) / len(X)
            self.means[comp_id] = mean
            self.stddevs[comp_id] = math.sqrt(max(float(sum([(x-mean)**2 for x in X])) / (len(X) - 1), 1e-4))
            self.proportion[comp_id] = float(len(X)) / self._lenX

    def remove_components(self):
        assert self.rm_comp
        assert self.rm_comp_threshold

        #for comp in self.comp_names:
        #    if self.comp_attribution.count(comp) < self.rm_comp_threshold * self._lenX

    def merge_components(self):
        """
        Checks every pair of components and merges them if the merging criteria 
        are met.

        Two components are merged when the distance between their means is 
        small (less self.merge_comp times the sum for their stadard deviations).
        """
        for i, j in itertools.combinations(self.comp_names, 2):
            if i not in self.comp_names or j not in self.comp_names:
                # It was removed between the iterator creation and now
                continue

            if j < i: # That way, there is alwas i < j
                tmp = i
                i = j
                j = tmp

            m1, m2 = self.means[i], self.means[j]
            s1, s2 = self.stddevs[i], self.stddevs[j]
            if abs(m1 - m2) < self.merge_comp*sum([s1, s2]):
                mean = 0.0
                count = 0
                indexes = []
                for x in xrange(self._lenX):
                    if self.comp_attribution[x] == j:
                        self.comp_attribution[x] = i
                        mean += self._X[x]
                        count += 1
                        indexes.append(x)
                    elif self.comp_attribution[x] == i:
                        mean += self._X[x]
                        count += 1
                        indexes.append(x)
                
                mean = mean / count 
                stddev = math.sqrt(1.0/(len(indexes)-1) * sum([(self._X[x]-mean)**2 for x in indexes]))
                stddev = max([stddev, 1e-2])

                self.means[i] = mean
                self.stddevs[i] = stddev
                self.proportion[i] = float(count)/self._lenX
                
                del self.means[j], self.stddevs[j], self.proportion[j]
                #del self.colors[j]
                self.comp_names.remove(j)
                self.ncomp -= 1

#                if j == self._next_id - 1:
#                    self._next_id -= 1

                if self.toggle_plot_on:
                    self.plot_merging(m1, s1, m2, s2, mean, stddev, i)
                logging.debug("Merging, %d components left" % self.ncomp)

    def plot_histogram(self, spec=[]):
        """
        Draws the histograms of the data points (most likely to be) generated  
        by each component. Adds the gaussian curve representing the estimated  
        mixture model, and highlights the mean +/- standard deviation interval. 
        Each component is handled in a different call to draw_component. New 
        data points (in spec) are also represented (blue dot bellow the x axis). 
        """
        minX = min(self._X)
        maxX = max(self._X)
        binLen = float(maxX-minX) / self.nbins
        self._bins = [minX + i*binLen for i in xrange(self.nbins)] # 50 bins
        self._plot_x = np.arange(minX, maxX, float(maxX-minX)/150) # the points where functions are to be evaluated
        count = 0
        for i in self.comp_names:
            count += self.comp_attribution.count(i)
        
        self._ax.clear()
        y_total = [0.0 for i in xrange(len(self._plot_x))]
        for color in self.comp_names:
            y = self.draw_component(color)
            y_total = map(lambda a, b: a+b, y_total, y)

        #self._ax.plot(self._plot_x, y_total, 'k')
        #self._ax.set_ylim(ymax=6)

        map(lambda x: self._ax.scatter(x, -0.5), spec)
        
        if self.accuracy:
            self._ax.text(0.05, .95,'accuracy: %.2f\nrecall: %.2f' % (self.accuracy[-1], self.recall[-1]),
                    horizontalalignment='left',
                    verticalalignment='top',
                    transform = self._ax.transAxes)
        self._fig.canvas.draw()
        pass

    def draw_component(self, index):
        """
        Draws the hist of the data points in the component, the curve of the 
        corresponding gaussian density distribution, the mean +/- stddev 
        interval. 

        Parameter:
            index: int, the id of the component
        Returns:
            y: list of floats, the values of the density distribution for this 
            component. It is used by plot_histogram(), in order to draw the 
            total density distrib (sum of each mixture component).
        """
        npts = self.comp_attribution.count(index)
        if not npts:
            return [0.0 for i in xrange(len(self._plot_x))]

        color = self.colors[index]

        mean = self.means[index]
        stddev = self.stddevs[index]
        pts = [self._X[i] for i in xrange(self._lenX)  
                if self.comp_attribution[i] == index]

        # Vertical span: mean +/- stddev
        self._ax.axvspan(mean-stddev, mean+stddev, color=color, alpha=.5)

        # Histogram
        self._ax.hist(pts, bins=self._bins, color=color, alpha=.5, normed=False)

        # Gaussian curve
        y = [math.exp(-.5 * (x-mean/stddev)**2) / (math.sqrt(2*math.pi)*stddev) for x in self._plot_x]
        #self._ax.plot(self._plot_x, y, color=color, ls='-', lw=1)
        return y

    def plot_merging(self, m1, s1, m2, s2, m3, s3, name):
        """
        Highlights the changes a component merging brings. The two merged 
        components (mean +/- stddev intervals) are hatched // and \\. The data 
        points which are involved in the merging are drawn in blue. The 
        resulting component is dotted. 

        Parameters:
            m1, s1: the mean and stddev of the first merged component
            m2, s2: the mean and stddev of the other merged component
            m3, s3: the mean and stddev of the resulting component
            name: the id under which the resulting component is referenced
        """
        self.plot_histogram()

        # Original comps to merge
        self._ax.axvspan(m1-s1, m1+s1, hatch='//', fill=False)
        self._ax.axvspan(m2-s2, m2+s2, hatch='\\\\', fill=False)
        self._ax.axvspan(m3-s3, m3+s3, hatch='.', fill=False)

        indexes = filter(lambda x: self.comp_attribution[x] == name, range(self._lenX))
        for ind in indexes:
            self._ax.scatter(self._X[ind], 2)

        self._fig.canvas.draw()

    def log_results(self):
        # Comp attribution:
        for i in xrange(self._nshifts):
            self._comp_attrib_logging.write(';')
        for i in self.comp_attribution:
            self._comp_attrib_logging.write('%d;' % i)
        self._comp_attrib_logging.write('\n')


        # Comp char
        for comp_id in self.comp_names:
            self._comp_chars_logging.write('(%d,%f,%f,%d);' % 
                    (comp_id, self.means[comp_id], 
                        self.stddevs[comp_id], 
                        self.comp_attribution.count(comp_id))
                    )
        self._comp_chars_logging.write('\n')
    
    def plot_time_per_comp(self):
        """
        Plot histograms: 
        - the number of different components an element was attributed to
        - the percentage of time spent in the majoritary component
        """
        fig, (f1, f2) = mpl.pyplot.subplots(2, 1, sharey=True)
        n_diffcomp = []
        t_bestcomp = []
        for elem in self._t_per_c:
            n_diffcomp.append(len(elem))
            t_bestcomp.append(float(max([elem[i] for i in elem]))/sum([elem[i] for i in elem]))

        f1.hist(n_diffcomp)
        f1.set_title("Number of different components to which the data points have been attributed")
        f2.hist(t_bestcomp)
        f2.set_title("Percentage of time spent in the maj component")
        mpl.pyplot.draw()

    def plot_comp_evolution(self, c_id, f1, f2, annotation={}):
        """
        - Number of data points attributed to the component 
        - Mean and standard deviation
        """

        means = [None for i in xrange(self._c_start[c_id])]
        means_m_stddev = [None for i in xrange(self._c_start[c_id])]
        means_p_stddev = [None for i in xrange(self._c_start[c_id])]
        n_pts = [0 for i in xrange(self._c_start[c_id])]
        for m, s, npts in self._c_chars[c_id]:
            means.append(m)
            means_m_stddev.append(m - s)
            means_p_stddev.append(m + s)
            n_pts.append(npts)
#        while len(means) < self._npts:
#            means.append(None)
#            means_m_stddev.append(None)
#            means_p_stddev.append(None)
#            n_pts.append(None)


        c = self.colors[c_id]
        f1.plot(means, color=c, ls="-",label="Comp #%d" % c_id)
        f1.fill_between(range(len(n_pts)), means_m_stddev, means_p_stddev, color=c, alpha=.4)
        f1.plot(means_p_stddev, color=c, ls="-")
        f1.plot(means_m_stddev, color=c, ls="-")

        f2.plot(n_pts, color=c, ls='-', label="Comp #%d" % c_id)


    def plot_all_comp_evolution(self):
        fig, (f1, f2) = mpl.pyplot.subplots(2, 1)
        for c in self._c_chars:
            self.plot_comp_evolution(c, f1, f2)

        f2.legend()
        

    def plot_ncomp_evolution(self):
        fig, ax = mpl.pyplot.subplots(1)

        ncomp = []
        maxlen = len(self._c_attr[-1])
        print maxlen
        for t in xrange(maxlen): # the last is the longest
            comps = set([i[t] for i in self._c_attr if len(i) > t])
            if None in comps:
                ncomp.append(len(comps)-1)
            else:
                ncomp.append(len(comps))

        ax.plot(ncomp, color='r', ls='-')


        mpl.pyplot.draw()

    def update_quality_measures(self, annotation):
        gt_comp_names = list(set(annotation[self._nshifts: self._nshifts+self._lenX]))
        gt_comp_len = len(gt_comp_names)

        accuracy = .0
        recall = .0

        attributed_vs_gt = {}
        # Update the available rows/columns
        for attr_key in self.comp_names:
            attributed_vs_gt[attr_key] = {}
            for gt_key in gt_comp_names:
                attributed_vs_gt[attr_key][gt_key] = 0

        # Count the occurrences of each component
        attr_count = {}
        for attr_key in self.comp_names:
            attr_count[attr_key] = self.comp_attribution.count(attr_key)

        gt_count = {}
        for gt_key in gt_comp_names:
            gt_count[gt_key] = 0

        # Count the occurrences for all gt_key ^ attr_key
        for i in xrange(self._lenX):
            gt_key = annotation[self._nshifts+i]
            attr_key = self.comp_attribution[i]
            attributed_vs_gt[attr_key][gt_key] += 1
            gt_count[gt_key] += 1

        # The main difficulty comes from the 
        # fact there is no direct mapping betwween the ground truth annotations 
        # and the names of the attributed components. The choice that was made 
        # is to say that for each discovered component 'attr' is associated to 
        # the ground truth annotation 'gt' that maximizes the 'attr' ^ 'gt' count. 
        best_match = {}
        for attr_key in self.comp_names:
            max_match = 0
            for gt_key in gt_comp_names:
                if attributed_vs_gt[attr_key][gt_key] > max_match:
                    max_match = attributed_vs_gt[attr_key][gt_key] 
                    best_match[attr_key] = gt_key

        # Accuracy: correctly classified / total number of data points
        for attr_key in self.comp_names:
            accuracy += attributed_vs_gt[attr_key][best_match[attr_key]]
        accuracy /= self._lenX

        # The same, but with the inverse matching: for each searched (gt) 
        # component, associate the discovered (attr) component that fits best
        best_match = {}
        for gt_key in gt_comp_names:
            max_match = 0
            for attr_key in self.comp_names:
                if attributed_vs_gt[attr_key][gt_key] > max_match:
                    max_match = attributed_vs_gt[attr_key][gt_key]
                    best_match[gt_key] = attr_key
        # Recall
        for gt_key in gt_comp_names:
            recall += attributed_vs_gt[best_match[gt_key]][gt_key]
        recall /= self._lenX

        self.accuracy.append(accuracy)
        self.recall.append(recall)
        return accuracy, recall

    def plot_quality_measures(self):
        """
        Draws the evolution of the accuracy and recall measures over time
        """
        fig, ax = mpl.pyplot.subplots(1, 1)
        ax.plot(self.accuracy, color='r', label='accuracy')
        ax.plot(self.recall, color='b', label='recall')
        ax.legend(loc='upper left')
        ax.set_ylim(0, 1.1)


def parse_data_file(filename):
    """
    Parse a datafile, assuming it was generated by the MixtureModelGeneration 
    class. That is to say, that it is a csv file. Each row contains a data 
    point, and there are 2 columns: the first one contains the data itself, 
    and the 2nd identifies (if the information is available) the id of the 
    component that led to the generation of the data point. 
    """
    data = []
    annotations = []
    with open(filename, 'r') as f:
        reader = csv.reader(f)
        for d, a in reader:
            data.append(
                    datetime.datetime.strptime(d, "%Y-%m-%d %H:%M:%S")
                    )
            annotations.append(a)
    return data, annotations

def datetime2reltime(dt, period=datetime.timedelta(days=1)):
    """
    Compute the relative time from the original datetime.

    Parameters:
        dt: datetime.datetime, the original timestamp
        period: datetime.timedelta, optional, default = 1day. The period of interest

    Return:
        int, equals to dt % period
    """
    return (dt - datetime.datetime(1970,1,1)).total_seconds() % period.total_seconds()

    
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(
            description="Streaming, heuristic Expectation Maximisation",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter
            )
    parser.add_argument('-f', '--filename', required=True,  
            help=""" Data file, assumed to be generated by the 
                MixtureModelGeneration class. That is to say, that it is a csv 
                file, using semicolons. Each row contains a data point, and 
                there are 2 columns: the first one contains the data itself, 
                and the 2nd identifies (if the information is available) the 
                id of the component that led to the generation of the data point. 
                """)
    parser.add_argument('--ncomp', type=int, default=1,
            help="the initial number of components in the model")
    parser.add_argument("--max_ncomp", type=int, default=None,
            help="Sets an upper bound on the number of components in the mixture " +
            "model. Once the limit is reached, no new component can be created. " +
            "When it is set to None, the number of components is not constrained"
            )
    parser.add_argument("--merge_comp", default=False, 
            nargs = '?', const=1.5, type=float,
            help="Set to enable the merging of components (highly recommended). " +
            "Default: no merging. Flag without value: merge if abs(m1-m2)<1.5*(s1+s2). " +
            "Flag with value: change the 1.5 coeff for MERGE_COMP.")
    parser.add_argument("--rm_comp", action='store_true', default=False,
            help="allows (or not) the deletion of components (NOT USED, FIXME)")
    parser.add_argument("--add_comp", type=float, default=False, const=1.0, nargs='?',
            help="Set to enable the addition of new components. " +
            "Also specifies how often new components are added: a random number in " +
            "[0.0, 1.0( is generated. If it is lower than ADD_COMP, then the new " + 
            "component is added. Default: no new component. Flag without value: every " + 
            "new datapoint can generate a new component (ADD_COMP = 1.0). ")
    parser.add_argument("--smart_add_only", action='store_true', default=False,
            help="When a new datapoint arrives, create a component for it only if it " +
            "is not close to any of the already existing components. A data " +
            "point x is close to a component (mean m, stddev s) if |m - x| < s. ")

    parser.add_argument("--lenXmax", type=int, default=None,
            help="The maximal length of the data in memory. Forces to drop old data " +
            "entries when too many new data points arrive. When it is equal to " +
            "None, there is no bound. ")

    parser.add_argument("--reset_rate", type=int, default=None,
            help="How often further investigation shoud be conducted (do as many EM " +
            "passes as necessary, instead of just one in the simple update)." +
            "When None, no bigger update is done")
    parser.add_argument("--max_iter", type=int, default=None,
            help="When resetting a component, maximal number of EM iterations " +
                "(if it does not converge before that)"
            )

    plot_group = parser.add_argument_group("Plot drawing options")
    plot_group.add_argument("--toggle_plot_on", action="store_true", default=False,
            help="Plot the results of the mining")
    plot_group.add_argument("--colormap", default="jet", 
            help="Name of a matplotlib colormap. The colors of the components are " +
                "picked at random from the colormap"
            )

    parser.add_argument("-v", "--verbose", action="store_const", 
            const=logging.DEBUG, default=logging.INFO, 
            help="Verbose mode (set logging level to debug instead of info)")

    args = parser.parse_args()
    logging.basicConfig(level=args.verbose)

    data, annotations = parse_data_file(args.filename)

    mme = MixtureModelEstimator(
            ncomp            = args.ncomp,
            rm_comp            = args.rm_comp,
            add_comp        = args.add_comp,
            merge_comp        = args.merge_comp,
            max_ncomp        = args.max_ncomp,
            smart_add_only    = args.smart_add_only,
            lenXmax            = args.lenXmax,
            reset_rate        = args.reset_rate,
            max_iter        = args.max_iter,
            toggle_plot_on    = args.toggle_plot_on,
            colormap        = args.colormap,
            )

    mme.reset_component(data[:10])
    i = 0
    for d in data[10:]:
        i += 1
        if i % 100 == 0:
            print i
        mme.update_mixture(new_data=[d, ])
        mme.update_quality_measures(annotations)
    
    if args.toggle_plot_on:
        mme.plot_time_per_comp()
        mme.plot_all_comp_evolution()
        mme.plot_ncomp_evolution()
        mme.plot_quality_measures()
        raw_input("Press ENTER to quit")
