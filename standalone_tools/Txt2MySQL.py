#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import MySQLdb
import os
import logging
import re


def db_name(filename):
    basename = os.path.splitext(os.path.basename(filename))[0]
    return basename

def connect_mysqldb(host="localhost", user='julie', passwd='julie'):
    database = MySQLdb.connect(host=host,
                    user=user,
                    passwd=passwd)

    cursor = database.cursor()
    return database, cursor

def drop_previous_database(cursor, dbname):
    res = cursor.execute("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME=%s", dbname)
    if res:
        # dbname already exists
        cursor.execute("DROP DATABASE " + dbname)
        logging.debug(" Droped previous version of the dataset")
        return True
    else:
        return False

def create_db(cursor, dbname):
    cursor.execute("CREATE DATABASE " + dbname)
    logging.info(" Database %s created" % dbname)

    cursor.execute("USE " + dbname)
    logging.warning(" Side effect: the newly created db is the current db")

    cursor.execute("CREATE TABLE SENSOR (id INT, name VARCHAR(50) DEFAULT NULL, PRIMARY KEY (id))")
    cursor.execute("CREATE TABLE EVENT (timestamp DATETIME, sensor_id INT, value VARCHAR(30) DEFAULT NULL, FOREIGN KEY (sensor_id) REFERENCES SENSOR(id) ON DELETE CASCADE)")
    logging.info(" Created the tables SENSOR(id, name) and EVENT(timestamp, sensor_id, value)")

    cursor.execute ("CREATE VIEW EVENT_VIEW AS SELECT timestamp, name AS label FROM SENSOR JOIN EVENT ON id=sensor_id")
    logging.info(" Created view EVENT_VIEW(timestamp, label)")

def populate_db(cursor, filename):
    """
    Read the lines in filename. Extract the timestamp, label and value of each 
    record. Expected format:
        YYYY-MM-DD HH:MM:SS sensor label( sensor_value)?
    Populate the database: create the sensor corresponding to the labels, insert
    the events.
    """
    sensors = {}

    regex = re.compile("(?P<timestamp>\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}(\.\d+)?) (?P<label>.+)(?: (?P<value>[^ ]+))?$")

    i = 0
    nevents = 0
    with open(filename, 'r') as dataset:
        for line in dataset:
            line = line.strip()
            match = regex.match(line)
            if match:
                nevents += 1
                if nevents % 1000 == 0:
                    logging.debug(" %d event..." % nevents)
                timestamp = match.group("timestamp")

                label = match.group("label")
                if label not in sensors:
                    i += 1
                    cursor.execute("INSERT INTO SENSOR (id, name) VALUES (%s, %s)", (i, label))
                    logging.debug(" New sensor item: %s" % label)
                    sensors[label] = i
                sensor_id = sensors[label]

                if match.group("value"):
                    value = match.group("value")
                    cursor.execute("INSERT INTO EVENT (timestamp, sensor_id, value) VALUES (%s, %s, %s)", (timestamp, sensor_id, value))
                else:
                    cursor.execute("INSERT INTO EVENT (timestamp, sensor_id) VALUES (%s, %s)", (timestamp, sensor_id))

    logging.info(" Finished database population. %d events" % nevents)



if __name__ == '__main__':
    from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
    parser = ArgumentParser(description="Load .txt datafile into a SQL database", formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-f', '--filename', required=True,  
            help="Dataset to parse. Each line is expected to respect the format: "  
            + "YYYY-MM-DD HH:MM:SS sensor label( sensor_value)?")
    parser.add_argument('-v', '--verbose', help="More info", action="store_true")
    db_ap_group = parser.add_argument_group("Database options")
    db_ap_group.add_argument('-u', '--user', default='julie', help='MySQL user. Must be authorized to create databases')
    db_ap_group.add_argument('-p', '--passwd', default='julie', help="The MySQL user password")
    db_ap_group.add_argument('-H', '--host', default='localhost', help='MySQL server location')


    options = parser.parse_args()

    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    filename = options.filename
    host = options.host
    user = options.user
    passwd = options.passwd

    database, cursor = connect_mysqldb(host, user, passwd)
    dbname = db_name(filename)
    drop_previous_database(cursor, dbname)
    create_db(cursor, dbname)
    populate_db(cursor, filename)
    database.commit()
    database.close()

