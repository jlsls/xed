import os
import datetime
import logging

def from_options(options, name="algname"):
    """
    options must have:
        * log_directory: where data should be logged
        * dbname: the dataset name
    """
    # Is given destination path ok?
    if not os.access(options.log_directory, os.F_OK):
        os.makedirs(options.log_directory)

    today = str(datetime.date.today())
    if not os.access(os.path.join(options.log_directory, today), os.F_OK):
        os.makedirs(os.path.join(options.log_directory, today))

    # Execution specific dir
    path = os.path.join(options.log_directory, today, "%s_%s_" % (name, options.dbname))
    i = 0
    suffix = "%02d" % i

    # Optimal: create a dir with the name of the dataset. If such a dir already 
    # exists (created for a previous execution), suffix it with a number (the 
    # smallest integer > 1 that has not been used yet).
    while os.access(path + suffix, os.F_OK):
        i += 1
        suffix = "%02d" % i
    path = path + suffix
    os.makedirs(path)

    # Write the used parameters to file
    with open(os.path.join(path, "parameters.txt"), 'w') as f:
        f.write("OPTIONS: \n%s\n" % '\n'.join(['%s:\t%s' % (a[0], a[1]) for a in vars(options).items()]))

    logging.info(" Log written to %s" % path)
    return path
    


