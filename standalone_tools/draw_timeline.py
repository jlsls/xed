# -*- coding: utf-8 -*-

import os
import matplotlib.cm as cm

episode = {}
periodicities = []
offset_ = [i*86400 for i in range(7)]
days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
offset = {}
offset.update(zip(days, offset_))
print offset
with open('obtained_db.txt') as f:
    for line in f.readlines():
        if "### EPISODES" in line:
            in_eps = True
        elif "### PERIODIC EPISODES" in line:
            in_eps = False
            in_pereps = True
        elif "### EVENTS" in line:
            in_pereps = False
            in_db = True


        else:
            line = line.strip()
            if not line:
                continue
            if in_eps:
                print line
                id, cts = line.split(': ', 1)
                episode[id] = cts

            elif in_pereps:
                id, line = line.split(': ', 1)
                period, dstart, _, dend, _, line = line.split(' ', 5)
                yy, mt, dd = dstart.split('-')
                if dd == "01" or dd == "21" or dd=="31":
                    superscript = "st"
                elif dd == "02" or dd == "22" or dd=="32":
                    superscript = "nd"
                elif dd == "03" or dd=="23":
                    superscript = "rd"
                else:
                    superscript = "th"
                dstart = "%s, %s %1d\\textsuperscript{%s}" % (yy, months[int(mt)], int(dd), superscript)

                _, mt, dd = dend.split('-')
                if dd == "01" or dd == "21" or dd=="31":
                    superscript = "st"
                elif dd == "02" or dd == "22" or dd=="32":
                    superscript = "nd"
                elif dd == "03" or dd=="23":
                    superscript = "rd"
                else:
                    superscript = "th"
                dend = "%s %1d\\textsuperscript{%s}" % (months[int(mt)], int(dd), superscript)

                line, _, acc, _, _ = line.rsplit(' ', 4)
                acc = acc[:-1]
                line, _ = line.rsplit(' [', 1)
                line = line[2:-3]
                cts = line.split('),(')
                periodicity = []
                for ct in cts:
                    mean_, stddev = ct.split(', ')
                    if any(map(lambda x: x in mean_, 'MTWFS')):
                        day, mean = mean_.split(' ', 1)
                        hh, mm, ss = mean.split(':')
                        mean = int(hh)*3600+int(mm)*60+int(ss)
                        mean += offset[day]

                    else:
                        hh, mm, ss = mean_.split(':')
                        mean = int(hh)*3600+int(mm)*60+int(ss)
                    
                    periodicity.append((mean, float(stddev), mean_))
                periodicities.append({'id': id, 'dstart': dstart, 'dend': dend,
                    'periodicity': periodicity, 'period': int(period), 'accuracy': acc})
    print periodicities

colormap = cm.get_cmap(name='RdYlGn')
colors = [colormap(float(i)/len(episode)) for i in range(len(episode))]
color_per_id = {}
color_per_id.update(zip(episode.keys(), map(lambda x: '%.2f,%.2f,%.2f' % (x[0],x[1],x[2]), colors)))

f = open('sample.tex', 'w')
f.write("""
\\documentclass{article}

\\usepackage{tikz}
\\usepackage{color}
\\usepackage{multirow}
\\usepackage{array}
\\usepackage{footnote}

\\usetikzlibrary{fadings}
""")
for c in color_per_id:
    f.write("\\definecolor{c%s}{rgb}{%s}\n" % (str(c).replace('-', '_'), color_per_id[c]))

f.write("""

\\begin{document}

\\tikzfading[name=fade out, inner color=transparent!0, outer color=transparent!75]
\\begin{figure}
    \\centering
    \\begin{tikzpicture}
        \\footnotesize
""")

# Pour le choix des ordonnées
rank = episode.keys()
dperiodicities = [p['id'] for p in periodicities if p["period"] == 86400]
daily_rank = [i for i in rank if i in dperiodicities]
 

# Proportion de la journée où des habitudes ont été repérées
mindate = 0
mindate2 = int(min([p['periodicity'][0][0] for p in periodicities if p["period"] == 86400]))/3600
maxdate = int(max([p['periodicity'][0][0] for p in periodicities if p["period"] == 86400]))/3600 +1
print mindate, maxdate


# Axe
f.write("\t\t% Axe\n")
f.write("\t\t\\draw [thick, ->] (-0.5, %f) -- (10.5, %f);\n\n" 
        % (.3*len(daily_rank), .3*len(daily_rank))
        )


# Graduations (gris clair, pointillés)
f.write("\t\t% Graduations\n")
last = 0
for x in range(mindate, maxdate + 1):
    xx = float((x - mindate) * 3600) * 10 / ((maxdate-mindate)*3600)
    f.write("\t\t\\draw [gray, dashed, thick] (%f, -.5) -- (%f, %f+0.2);\n" %
            (xx, xx, .3*len(daily_rank)))
    # Un label sur 4 seulement
    if last % 4 == 0:
        last += 1
        f.write("\t\t\\draw [thick] (%f, %f - 0.1) -- (%f, %f + .2); \n"
                % (xx, .3*len(daily_rank), xx, .3*len(daily_rank)))
        f.write("\t\t\\node at (%f, %f + .5) {%d:00}; \n" %  
                (xx, .3*len(daily_rank), x))
    else:
        last += 1

# Les habitudes !
for p in periodicities:
    if p['period'] == 604800:
        continue

    for pp in p['periodicity']:
        x = float(pp[0] - mindate*3600) * 10 / ((maxdate-mindate)*3600)
        xr = float(2*pp[1]) * 10 / ((maxdate-mindate)*3600)
        yr = .3
        y = 0.3*(daily_rank.index(p['id']))

        # Zone ombrée
        f.write("""
            \\fill[c%s!80!orange, path fading=fade out] 
                (%f, %f) ellipse [x radius=%f, y radius=%f];
            """ % (p['id'].replace('-', '_'), x, y, xr, yr))
        # Contour (à enlever)
#        f.write("\t\t\\draw[c%s, path fading=fade out]"
#            "(%f, %f) ellipse [x radius=%f, y radius=%f];\n"  
#            % (p['id'].replace('-', '_'), x, y, xr, yr))
for e in daily_rank:
    y = 0.3 * daily_rank.index(e)

    # Episode label
    f.write("\t\t\\node [right, ] at (-0.5, %f) {%s};\n" 
            % (y, episode[e]))

f.write("""
    \\end{tikzpicture}
\\end{figure}

\\begin{table}
    \\centering
    \\resizebox{\\textwidth}{!}{
        \\small
        \\input{result_tabular}
    }
    \\caption{First 10 periodic episodes (out of %d periodic episodes)}
\\end{table}
\\footnotetext{Accuracy} 
\\end{document}
""" % len(periodicities))

f.close()



# Le tableau qui va bien
f = open('result_tabular.tex', 'w') 

f.write("""
\\begin{tabular}{|c|m{2.3cm}|l@{$\\rightarrow$}l@{,~}l@{,~}l|c|}
    \\hline
    \\multirow{2}{*}{\\#} & 
    \\multirow{2}{*}{\\bf Episode} & 
    \\multicolumn{4}{l|}{\\bf Periodicity} &
    \\multirow{2}{*}{\\bf A\\footnotemark} \\\\ 

    & & Start & End & Period & Description & \\\\ \\hline
""")

i = 1
pref = ""
for periodicity in periodicities:
    f.write("\n\t%s%d & \n" % (pref, i))
    i += 1

    f.write("\t%s%s & \n" % (pref, episode[periodicity['id']]))

    if periodicity['period'] == 86400:
        period = "1 day"
    else:
        period = "1 week"
    f.write("\t%s%s & %s & %s &\n" 
            % (pref, periodicity['dstart'], periodicity['dend'], period))
    
    f.write("\t%s\\begin{tabular}{@{}l@{}}\n" % pref)
    for _, s, m in periodicity['periodicity']:
        f.write("\t%s\t(%s, %ds)\\\\\n" % (pref, m, s))
    f.write("\t%s\\end{tabular} & \n"% pref)

    f.write("\t%s%d~\\%% \\\\ \\hline\n" % (pref, int(100*float(periodicity['accuracy']))))

    if i > 10:
        pref = "%"


f.write("\\end{tabular} \n")
f.close()
os.system("pdflatex sample.tex") 
