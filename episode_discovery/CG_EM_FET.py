#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys 
import logging
import time
import os

import dataset.parser
from   dataset import Database, Event, Episode

from episode_discovery.CandidateGenerator import CandidateGenerator

class CandidateGenerator_EM_FET(CandidateGenerator):
    def generate_candidates(self):
        event_types = list(self.database.items)
        emfet = CG_EM_FET( event_types,
                episode_length = self.episode_length,
                episode_capacity = self.episode_capacity,
                minimum_support = self.minimal_frequency,
                offline = True,
                storing_prefix = self.storing_prefix)
        for event in self.database.events:
            emfet.new_event(event)

        emfet.create_tree()

        # Get the occurrences of the episodes
        # TODO !
        candidates = []
        to_be_seen = [emfet.root, ]
        while to_be_seen:
            node = to_be_seen.pop(0)
            seen.append(node)

            if node == emfet.root:
                pass
            else:
                episode_contents = emfet.episode(node)
                episode = Episode(episode_contents)
                candidates.append(episode)
                self.occurrences[episode] = node['TQ'] 

            to_be_seen.extend(node['children'])
        return candidates



class CG_EM_FET:
    def __init__(self, event_types, 
            episode_length=1200, minimum_support=5, memory_length=None,
            episode_capacity=None,
            offline=False, storing_prefix="."):
        """
        Streaming version of the Episode Matrix & Frequent Episode Tree algorithm.

        The algorithm maintains mostly 2 structures. An Episode Matrix (EM), 
        and a Frequent Episode Tree (FET). 

        Parameters
        ----------
        `event_types`: list
            The list of the event types to consider. 
            FIXME: I don't check that all events have a valid event type...
        `episode_length`: int, optional
            The maximum time interval in seconds between any 2 events in an 
            episode instance. Defaults to 1200 (20 minutes).
        `minimum_support`: int, optional
            The minimum support (= number of DMOs) of an episode for it to be
            considered as frequent. Frequent episodes that fall bellow this 
            frequency threshold are not considered as frequent anymore, and
            they are removed from the FET.
            Defaults to 5.
        `memory_length`: int, optional
            The "expiration date" of the data. Any effet of an event older 
            than `memory_length` seconds is discarded. 
            Defaults to None (events are never outdated).
        `episode_capacity`: int, optional
            The maximal number of event types in an episode. It constrains the 
            height of the FET. Defaults to None (episodes of any length are 
            accepted)
        `offline`: bool, optional
            Don't update the tree when new events arrive/old events are 
            deleted. Instead, a call to `create_tree` builds the FET (it is the 
            one called `self.tree`). Defaults to False (online)
        `storing_prefix`: str, optional
            Folder where the logfiles should be stored. Needs to be a writable
            location. Defaults to the current directory.

        Attributes
        ----------
        `event_types`: list
        `episode_length`: int
        `minimum_support`: int
        `memory_length`: int
        `episode_capacity`: int
        `offline`: bool
        `nb_event_types`: int
            The number of different event types (the length of `event_types`)
        `current_event`: general_tools.dataset.Event
            The last event added in the database so far.
        `window`: list
            Stores the events currently investigated (those more recent than 
            `memory_length`.
        `em`: dict of dicts, shape (`nb_event_types`, `nb_event_types`)
            The Episode Matrix. The rows and columns are indexed on event 
            types. An element em[i][j] corresponds to the length-2 episode, 
            starting with an instance of event type i, followed by an instance 
            of event type j. 
            These elements are themselves dict objects, with 3 keys:
                'count': the support count of the 2-episode
                'TQ': the time queue of this episode (ie. the list of the start 
                    and end times of the DMOs of the 2-episode)
                'lock': a boolean, specifying if an instance of the event type 
                    i, susceptible to lead to the observation of episode 
                    (i, j), was seen.
        `last_events`: dict, indexed on the event types
            Each item in `last_events` is itself a dict object, with 2 keys:
                'count': the support count of the event type, ie this  of the
                    length-1 episode
                'TQ': the time queue of the 1-episode. Since start times and 
                    end times are identical, the items are simply timestamps
        `fet`: dict
            It is the Frequent Episode Tree. It is used to store and update the 
            frequent episodes in the current window (shared-prefix-tree), where 
            each node corresponds to the sequential (frequent) episode 
            containing all the event types from the root to the node. Each node 
            in the tree is stored using a dict structure. The valid keys are :
                'event type': the last event type in the episode described in 
                    this node
                'count': int. The support count of the episode
                'TQ': list. The time queue of the episode
                'parent': dict. A ref to the dict used to describe the parent 
                    of the node.
                'children': dict, indexed on the event types of children nodes. 
        `tree`: dict
            Exactly the same structure than `self.fet`, except that this one 
            isn't updated with incoming events. However, it can be (re)built 
            with `self.create_tree`.
        `recently_modified`: list
            It contains references to nodes in `fet`, that were recently (ie 
            less than `episode_length` ago) modified. These episodes are those 
            susceptible to be extended to form longer episodes when a new event 
            is recorded. These potential longer episodes are either already 
            frequent (the node in `recently_modified` already has a child with 
            the same 'event type' than this of the new event), or not (the node 
            does not exist yet). 
        `modified`: list of 2-uples
            For each item in `modified`, the first element is a timestamp, and 
            the second is the number of nodes that were appended to 
            `recently_modified` when the structures were updated with the event 
            occurring at the given timestamp. Thus, when the timestamp becomes 
            too old for the nodes to participate in the construction of longer 
            episodes (ie the were observed more than `episode_length` ago), the 
            corresponding number of nodes are removed from the head of 
            `recently_modified`.
        `storing_prefix`
        `logfile`: str
            The path to the file where logs should be stored. 
            FIXME: No log written for now
        """
        self.event_types = event_types 
        self.nb_event_types = len(event_types)

        self.episode_length = episode_length
        self.episode_capacity = episode_capacity
        self.minimum_support = minimum_support
        self.memory_length = memory_length
        self.offline = offline

        self.storing_prefix = storing_prefix
        self.logfile = os.path.join([self.storing_prefix, 'candidates', 'candidate_generation.txt'])

        self.current_event = None
        self.window = [] # Pour le stockage des events actuellement dans la 
        # fenêtre de temps considérée
        # FIXME Une solution autre serait très préférable. Parce que là, ça 
        # m'empêche d'avoir une grande valeur de memory_length, surtout si 
        # forte densité d'events

        # Episode matrix
        self.em = {} # Matrice de taille N×N, avec N = self.nb_event_types
        for et in self.event_types:
            self.em[et] = {}
            for et2 in self.event_types:
                self.em[et][et2] = {}
                self.em[et][et2]['lock'] = True
                self.em[et][et2]['count'] = 0
                self.em[et][et2]['TQ'] = []

        # Les left 1-episodes
        self.last_events = {}
        for et in self.event_types:
            self.last_events[et] = {}
            self.last_events[et]['count'] = 0
            self.last_events[et]['TQ'] = []


        # L'arbre des épisodes fréquents
        self.fet = {'event type': "__ROOT__", 
                'level': 0, 
                'children': {}, 
                'parent': None,
                }
        self.nnodes = 1
        self.tree_depth = 1
        self.tree = {'event type': "__ROOT__", 
                'level': 0, 
                'children': {}, 
                'parent': None,
                }
        self.recently_modified = []
        self.modified = []

    def new_event(self, event):
        """
        Update the structures when a new event is observed.

        The function updates first the EM. Then, it propagates the new 
        information to update the knowledge on frequent episodes (record new 
        occurrences, and detect emerging patterns). For that, the obsolete 
        nodes in `recently_modified` are removed. Indeed, the only concerned 
        nodes in FET update are the children (or future children) of recently 
        modified nodes. Finally, outdated (ie. older than `memory_length`) 
        information is removed from EM, and the changes are once again 
        propagated in FET.

        Parameters
        ----------
        `event`: general_tools.database.Event
            The recently observed event. This new information is used to update 
            the knowledge base.

        Returns:
        --------
        Nothing!
        """
        logging.debug("")
        logging.debug(" New event: %s" % event)
        self.current_event = event

        _result = {}

        if (self.last_events[event.label]['TQ'] and
                self.last_events[event.label]['TQ'][-1] == event.timestamp):
            logging.debug(" Nothing to do: event already seen!")
            # il y a 2 fois exactement le même événement dans les données...
            return _result
       
        self.window.append(event)

        need_update = self.update_em_with_new_event(event)

        if self.offline:
            pass
        else:
            # Rm old data in self.modified and self.reently_modified
            logging.debug(" Cleaning self.modified, self.recently_modified")
            while (self.modified and 
                    self.modified[0][0] + self.episode_length < event.timestamp):
                _, nbnodes = self.modified.pop(0)
                del self.recently_modified[:nbnodes]

            # Mise à jour du FET
            self.update_fet_with_new_event(event, need_update, _result)

        # Gestion des événements devenus obsolètes
        while (self.memory_length and 
                self.window[0].timestamp < event.timestamp-self.memory_length):
            e = self.window.pop(0)
            self.remove_event(e, _result)

        return _result

    def update_em_with_new_event(self, event):
        """
        Update the EM when a new event `event` is observed

        The column of EM corresponding to that event (ie the column containing 
        data about the 2-episodes ending with the event's label) is first 
        investigated. If a cell's lock is opened, it means that an instance of 
        the event type beginning the episode was observed, and that so far, no 
        instance of the 2nd event type was observed after (an occurrence of the 
        2-episode was started, but not finished). If the timestamp of the new 
        event allows the creation of an occurrence for the episode (ie it 
        happens less than `episode_length` after the first event), then we have 
        a new DMO! 
        Whether the episode length constrain is met or not, the cell is locked 
        afterwards (in both cases, we need to seen a new instance of the 1st 
        event type.

        Then, the row corresponding to `event.label` is updated. The row stores 
        data about the 2-episodes beginning with the event type `event.label`. 
        Each locked cell is unlocked (`event` begins a new occurrence of the 
        episode). Already unlocked cells are not modified. However, the event 
        considered as the beginning of the episode is replaced by `event` 
        (thanks to update of `last_events`, see right bellow).

        Finally, the value corresponding to the 1-episode (`event.label`, ) in 
        `last_events` is updated.

        Parameters
        ----------
        `event`: general_tools.dataset.Event
            The new event, used to update the EM

        Returns
        -------
        `need_update`: the event types beginning 2-episodes updated during the 
            current call to `self.update_em_with_new_event(event)`.
        """
        logging.debug(" Update EM with new event")
        need_update = []
        logging.debug("   Column...")
        for et in self.event_types:
            if self.em[et][event.label]['lock'] == False:
                assert len(self.em[event.label][et]['TQ']) == self.em[event.label][et]['count']
                assert self.last_events[et]['TQ'] != []
                if event.timestamp - self.last_events[et]['TQ'][-1] <= self.episode_length:
                    self.em[et][event.label]['count'] += 1
                    self.em[et][event.label]['TQ'].append((self.last_events[et]['TQ'][-1], event.timestamp))
                    need_update.append(et)
                self.em[et][event.label]['lock'] = True

        logging.debug("   Row...")
        for et in self.event_types:
            if self.em[event.label][et]['lock'] == True:
                self.em[event.label][et]['lock'] = False
        
        assert (not self.last_events[event.label]['TQ'] or 
                self.last_events[event.label]['TQ'][-1] != event.timestamp)
        self.last_events[event.label]['count'] += 1
        self.last_events[event.label]['TQ'].append(event.timestamp)

        return need_update


    def remove_event(self, event, _result={}):
        """
        Update the data structures when an outdated event needs to be removed

        First, le `last_events` dict is updated. Then, it's EM:
        The row corresponding to `event.label` is investigated (the 2-episodes 
        beginning with `event`.label). If its TQ starts with `event`.timestamp, 
        its head is removed, and the occurrence count decremented. Finally, the
        FET is updated (see remove_event_in_fet)

        Parameters
        ----------
        `event`: general_tools.dataset.Event
            The outdated event, that needs to be removed

        Returns
        -------
        Nothing!
        """
        logging.debug("")
        logging.debug(" Removing event %s" % event)
        assert self.last_events[event.label]['TQ'][0] == event.timestamp
        self.last_events[event.label]['TQ'].pop(0)
        self.last_events[event.label]['count'] -= 1

        logging.debug(" Update row in EM")
        for et in self.event_types:
            if (self.em[event.label][et]['count'] and 
                    self.em[event.label][et]['TQ'][0][0] == event.timestamp):
                self.em[event.label][et]['TQ'].pop(0)
                self.em[event.label][et]['count'] -= 1
            assert (self.em[event.label][et]['count'] == 0 or 
                    self.em[event.label][et]['TQ'][0][0] != event.timestamp)
        
        if self.last_events[event.label]['count'] == 0:
            # It was the last remaining occ of this event type. Lock the row, 
            # since it's not going to be the beginning of a new episode anytime 
            # soon
            for et in self.event_types:
                self.em[event.label][et]['lock'] = True

        if self.offline:
            pass
        else:
            # Call to the func that is going to deal with the update of the FET
            self.remove_event_in_fet(event, _result)


    def remove_event_in_fet(self, event, _result={}):
        """
        Update the FET, when `event` is outdated.

        The episodes beginning with the event type `event.label` may loose an 
        occurrence (their oldest occ). These episodes are stored in the branch 
        starting at level 1 with the node `event.label`.
        The update is done by CG_EM_FET.remove_event_in_node(...), and uses 
        recursive calls in a DFS-manner: if a node needs an update, because a 
        DMO of the episode needs to be removed, then its children are also 
        investigated.

        Parameters
        ----------
        `event`: general_tools.dataset.Event
            The outdated event, whose effect needs to be propagated

        Returns
        -------
        Nothing!
        """
        if event.label in self.fet['children']:
            self.remove_event_in_node(self.fet['children'][event.label], 
                    event.timestamp, _result)


    def remove_event_in_node(self, node, ts, _result={}):
        """
        Update `node` when the event happening at `ts` is outdated.

        Check if the current episode occurred at the time, and if true, remove 
        the head of its TQ, and update its support count. 
        Episodes becoming rare are detached from the tree. A consequence is 
        that its children (they are also rare !) are not accessible anymore 
        either.
        Otherwise, investigate its children (recursive call).

        Parameters
        ----------
        `node`: dict
            The node of FET that needs an update because an old event is 
            removed (either because it is the node corresponding to the 
            1-episode, or because the node's parent was updated by this same 
            function a bit earlier.
        `ts`: int
            The timestamp of the removed event. Only the node (ie. episode) 
            whose TQ starts with (ie. first occurrence) `ts` need an update.

        Returns
        -------
        Nothing!
        """
        logging.debug(" Update node %s : rm ts %d" % (self.episode(node), ts))
        _result.setdefault(self.episode(node), {})

        _rnode = _result[self.episode(node)]
        _rnode.setdefault('is_new', False)
        _rnode.setdefault('became_rare', False)
        _rnode.setdefault('new_occurrences', [])
        _rnode.setdefault('noutdated', 0)
        # FIXME FIXME maintenant que c'est créé, faut l'utiliser !
        
        if node['TQ'][0][0] == ts:
            # The first occurrence became too old
            node['TQ'].pop(0)
            node['count'] -= 1
            _rnode['noutdated'] += 1

            assert len(node['TQ']) == node['count']
            if node['count'] < self.minimum_support:
                # The episode has just become rare...
                _rnode['became_rare'] = True
                def say_children_became_rare(N):
                    """
                    Recursive (DFS) help function, adding the children of a 
                    node that just became rare in the _result dict. 

                    Parameters
                    ----------
                    `N`: dict, the node that became rare
                    """
                    for child in N['children']:
                        self.nnodes -= 1
                        child = N['children'][child]
                        _result.setdefault(self.episode(N), {})
                        _rnode = _result[self.episode(N)]
                        _rnode.setdefault('is_new', False)
                        _rnode['became_rare'] = True
                        _rnode.setdefault('new_occurrences', [])
                        _rnode.setdefault('noutdated', 0)
                        # Recursive call
                        say_children_became_rare(child)

                say_children_became_rare(node)
                self.nnodes -= 1

                del node['parent']['children'][node['event type']]
                # les noeuds fils (qui sont donc également rares) ne sont 
                # plus accessibles depuis la racine...
                node['parent'] = None
                # ... et la racine n'est plus accessible depuis le nœud.


            else:
                for child in node['children'].keys():
                    self.remove_event_in_node(node['children'][child], ts, _result)


    def get_count_for_extended_ep(self, TQ1, TQ2):
        """
        Get the count and time queue of the extended (n+1)-episode, derived 
        from an n-episode and a 2-episode, sharing respectively their first and 
        last event types

        A DMO of the (n+1)-episode may be observed whenever occurrences of the 
        2-episode are observed in the time interval between 2 occurrences o1 
        and o2 of the n-episode. This potential DMO is the one starting at the 
        beginning of o1, and ending at the end of the earliest occurrence of 
        the 2-episode in the interval. If this DMO meets the `episode_length` 
        constraint, then yes, there is a DMO.
        
        Parameters
        ----------
        `TQ1`: list of 2-uples
            The time queue of the n-episode
        `TQ2`: list of 2-uples
            The time queue of the 2-episode

        Returns
        -------
        `occurrences`: dict 
            It describes the DMOs of the (n+1)-episode. It has 2 keys:
            'count': int, the support count of the (n+1)-episode
            'TQ': list of 2-uples, the time queue, containing the DMOs of the 
                (n+1)-episode.
        """
        count = 0
        TQ = []
        L1 = len(TQ1)
        L2 = len(TQ2)
        i, j = 0, 0
        # i (resp. j) is the index on TQ1 (resp. TQ2)

        while (i < L1 and j < L2):
            # Find the first j such that TQ2[j] starts AFTER TQ[i]
            while (j < L2 and TQ2[j][0] < TQ1[i][1]):
                j += 1
            if j == L2:
                break

            # Find i such that TQ2[j] happens AFTER TQ1[i], but BEFORE 
            # TQ1[i+1]
            while (i+1 < L1 and TQ2[j][0] >= TQ1[i+1][1]):
                i += 1
            if TQ2[j][1] - TQ1[i][0] <= self.episode_length:
                # `episode_length` constraint is met: we have a new DMO!
                assert TQ2[j][1] - TQ1[i][0] >= 0
                TQ.append((TQ1[i][0], TQ2[j][1]))
                count += 1

            # Do it all over again, with the next value in TQ1
            i += 1

        assert len(TQ) == count
        return {'count': count, 'TQ': TQ}



    def update_fet_with_new_event(self, event, need_update, _result={}):
        """
        Update the FET, when a new event comes in.

        The new event is used to form new DMOs of episodes ending with that 
        event type. It can be a new occurrence of the 1-episode `event.label` 
        (which is dealt with by the function `update_root_with_new_event`, or 
        it can extend another frequent episode (dealt with in 
        `update_child_with_new_event`). The event can extend a frequent 
        episode only if a DMO of the frequent episode was recently observed, 
        and the 2-episode used for the extention has a new DMO (ie the last 
        event type of the n-episode is in `need_update`). This is was only the 
        nodes whose event type in need_update and that are in 
        `recently_modified` are investigated. 
        Whenever a new DMO of an episode is observed, `recently_modified` is 
        updated. In the end, `modified` is updated too (append the 2-uple 
        `event.timestamp`, number of modified nodes)

        Parameters
        ----------
        `event`: general_tools.dataset.Event
            The new event
        `need_update`: list
            The row indexes of EM that were modified (ie. for which a new DMO 
            was recorded) right before the call to this func.

        Returns
        -------
        Nothing!
        """
        logging.debug(" Update the FET with new event")
        nrecently_modified = len(self.recently_modified)
        nmod_nodes = 0
        nmod_nodes += self.update_root_with_new_event(event, _result)
        seen_nodes = []
        # On passe par l'indice, car recently_modified est alongée en cours de route
        for node_i in xrange(nrecently_modified):
            node = self.recently_modified[node_i]
            # Seuls les (futurs) noeuds fils de ces noeuds peuvent être concernés
            if node['event type'] in need_update and self.attached_to_root(node):
                nmod_nodes += self.update_child_with_new_event(node, event, _result)
        print self.nnodes, nrecently_modified, len(self.recently_modified), nmod_nodes
        self.modified.append((event.timestamp, nmod_nodes))
        return


    def update_child_with_new_event(self, node, event, _result={}):
        """
        Update the children of a node, when a new event comes in.

        The episode in `node` is extended with the new event. Two cases: 
            - the extended episode is already frequent (ie. there is already a 
            child node). In this case, we just need to check if there is a new 
            DMO for the extended episode
            - it wasn't recorded as frequent yet. The extended episode's 
            support count and TQ is then computed using 
            `get_count_for_extended_ep`. If the episode is frequent, a new node 
            is created in the tree (new child for node). 
        
        Parameters
        ----------
        `event`: general_tools.dataset.Event
        `node`: dict
            A recently modified node. We checking if the episode represented by 
            node may be extended with the new event. 

        Returns
        -------
        `nmod_nodes`: the number of nodes that were modified (and added in 
            `recently_modified`)
        """
        # Le noeud courant est à la profondeur maximale: on s'arrete là
        if self.episode_capacity and self.episode_capacity == node['level']:
            return 0

        nmod_nodes = 0
        node_et = node['event type']
        new_et = event.label
        new_ts = event.timestamp
        if new_et in node['children']:
            child = node['children'][new_et]
            if child['TQ'][-1][1] == new_ts:
                # The node was several times in recently modified, and has already been updated
                #print self.recently_modified.count(node)
                return nmod_nodes
            episode = self.episode(child)
            logging.debug(" Ep %s already frequent. New occ?" % ", ".join(episode))
            # l'épisode est déjà fréquent. A-t-on une occurrence 
            # supplémentaire ? Si oui, elle doit impérativement faire 
            # intervenir le dernier élément de la TQ du 2-episode 
            # {dernier event type de node, new_et}, et alors, 
            # seule la dernière occurrence de l'ep caractérisé par 
            # node est candidate pour extension. Reste juste à vérifier 
            # que la contrainte de durée est respectée. 
            assert self.em[node_et][new_et]['count'] >= self.minimum_support
            assert self.em[node_et][new_et]['TQ'][-1][1] == new_ts
            if node['TQ'][-1][0] ==  child['TQ'][-1][0]:
                # Cette occurrence de episode(node) a déjà servi pour 
                # constituer une occurrence de node U event (ça peut arriver 
                # si on a 2 instances distinctes de event.label, qui ont toutes 
                # les 2 lieu moins de episode_length après le début de la 
                # dernière occ de node).
                logging.debug("    Non.")

            else:
                assert node['TQ'][-1][1] >= child['TQ'][-1][1]
                assert node['TQ'][-1][0] > child['TQ'][-1][0] 
                # Obj : étendre la dernière occ de l'ep en node avec la 
                # dernière occ du 2-ep (node['et'], new_et)
                s_time = node['TQ'][-1][0] 
                if new_ts - s_time <= self.episode_length:
                    # On a bien une nouvelle occurrence
                    child['TQ'].append((s_time, new_ts))
                    child['count'] += 1
                    assert len(child['TQ']) == child['count']
                    self.recently_modified.append(child)
                    nmod_nodes += 1
                    logging.debug("    Oui.")
                    assert self.verify_apriori_property(child)

                    _result[episode] = {'is_new': False,
                            'became_rare': True,
                            'new_occurrences': [(s_time, new_ts), ],
                            'noutdated': 0}
                else:
                    logging.debug("    Non.")
        else:
            # l'épisode n'est pas (encore) fréquent.
            logging.debug(" Ep %s U %s pas encore frequent. Newly fq ?" 
                    % (self.episode(node), new_et))
            if self.em[node_et][new_et]['count'] >= self.minimum_support:
                # The extended episode may be frequent
                d = self.get_count_for_extended_ep(node['TQ'], 
                        self.em[node_et][new_et]['TQ'])
                if d['count'] >= self.minimum_support:
                    # It is indeed frequent!
                    # New node
                    self.nnodes += 1
                    child = {'event type': new_et, 'children': {},  
                            'level': node['level'] + 1, 'parent': node,
                            }
                    episode = self.episode(child)
                    child.update(d)
                    node['children'][new_et] = child
                    logging.debug("    Oui.")
                    logging.debug("    Episode %s (%d) has become frequent. " 
                            % (episode, child['count']))
                    self.recently_modified.append(child)
                    nmod_nodes += 1
                    assert self.verify_apriori_property(child)

                    _result[episode] = {'is_new': True,
                            'became_rare': False,
                            'new_occurrences': child['TQ'][:],
                            'noutdated': 0
                            }
                else:
                    logging.debug("    Non.")
            else:
                logging.debug("    Non (2-ep pas fq).")
        return nmod_nodes

    def update_root_with_new_event(self, event, _result={}):
        """
        Update the FET with the 1-episode `event.label`

        2 possibilities: the 1-episode was already frequent, and we just have 
        to add a new DMO. Or it wasn't, and we need to check if it has become 
        frequent. If so, the root of the FET gets a new child.

        Parameters
        ----------
        `event`: general_tools.dataset.Event
            The new event

        Returns
        -------
        `nmod_nodes`: int
            The number of nodes that were modified (either 0 or 1)
        """
        logging.debug(" Update root")
        # Presque pareil que la fn au dessus, avec quelques adaptations pour 
        nmod_nodes = 0
        new_et = event.label
        if new_et in self.fet['children']:
            node = self.fet['children'][new_et]
            s_time = self.last_events[new_et]['TQ'][-1]
            # Le 1-épisode est déjà fréquent. On a une occurrence supplémentaire. 
            # Simple mise à jour du noeud.
            assert self.last_events[new_et]['count'] >= self.minimum_support
            assert self.last_events[new_et]['TQ'][-1] == event.timestamp
            
            node['TQ'].append((s_time, s_time))
            node['count'] += 1
            self.recently_modified.append(node)
            nmod_nodes += 1
            logging.debug(" Episode %s already frequent. Yes, new occ. (%d)" 
                    % (new_et, node['count']))

            _result[self.episode(node)] = {
                    'is_new': False,
                    'became_rare': False,
                    'new_occurrences': [(s_time, s_time), ],
                    'noutdated': 0
                    }
        else:
            logging.debug(" 1-ep %s pas encore frequent. Newly fq ?" % new_et)
            # l'épisode n'est pas (encore) fréquent. 
            if self.last_events[new_et]['count'] >= self.minimum_support:
                # Oui, il est frequent
                # New node
                logging.debug("    Oui.")
                node = {'event type': new_et, 
                        'count': self.last_events[new_et]['count'],
                        'TQ': [(i, i) for i in self.last_events[new_et]['TQ']], 
                        'children': {}, 
                        'level': 1,
                        'parent': self.fet,
                        }
                self.nnodes += 1
                self.fet['children'][new_et] = node
                logging.debug("    Episode %s (%d) has become frequent." 
                        % (self.episode(node), node['count']))

                self.recently_modified.append(node)
                nmod_nodes += 1

                _result[self.episode(node)] = {
                        'is_new': True,
                        'became_rare': False,
                        'new_occurrences': node['TQ'][:],
                        'noutdated': 0
                        }
            else:
                logging.debug("    Non.")
        return nmod_nodes

    def create_tree(self):
        """
        Find the frequent episodes, based on data from EM.

        This is not the stream version. The 1-episodes are found in last_events, 
        the 2-episodes in em, and longer episodes are derived from the 
        2-episodes, just like in the original publication of EM & FET.
        The tree is built recursuvely, in a DFS manner, thanks to recursive 
        calls to `self.generate_children`.

        TODO: Speed tests: Regular update VS Rebuilding of the tree from scratch

        Parameters & Returns
        --------------------
        Nothing!
        """

        self.tree = {'event type': "__ROOT__", 
                'level': 0, 
                'children': {}, 
                'parent': None}
        for et in self.event_types:
            if self.last_events[et]['count'] >= self.minimum_support:
                node = {'event type': et, 
                        'count': self.last_events[et]['count'],
                        'TQ': [(i, i) for i in self.last_events[et]['TQ']], 
                        'children': {}, 
                        'level': 1,
                        'parent': self.tree,
                        }
                self.tree['children'][et] = node
                self.generate_children(node)
        return


    def generate_children(self, node):
        """
        Build the subepisodes of the episode described in `node`. Build the 
        corresponding subtree.

        Every frequent 2-episode starting with the same event type than `node` 
        is tested to extend the episode described in node. The TQ of the 
        extended episode is computed (`get_count_for_extended_ep`). If the 
        extended is frequent, a new node is created for it, and this episode is 
        tested for extensions too (rec call). 

        Parameters
        ----------
        `node`: dict
            A node in `self.tree` (describing a frequent episode). This 
            function finds its frequent subepisodes.

        Returns
        -------
        Nothing!
        """
        # The maximal episode capacity is reached: no child episode can be 
        # generated. We stop here
        if self.episode_capacity and self.episode_capacity == node['level']:
            return 

        last_e = node['event type']
        assert not node['children']
        for et in self.event_types:
            if self.em[last_e][et]['count'] >= self.minimum_support:
                # The extended episode may be frequent
                d = self.get_count_for_extended_ep(node['TQ'], self.em[last_e][et]['TQ'])
                if d['count'] >= self.minimum_support:
                    # It is indeed frequent!
                    node2 = {'event type': et, 'children': {},  
                            'level': node['level'] + 1, 'parent': node}
                    node2.update(d)
                    node['children'][et] = node2
                    self.generate_children(node2)
        return

    def print_em(self):
        """
        Print the current support counts in EM
        """
        t = "Episode matrix"
        print "\n%s\n%s" % (t, '='*len(t))

        print "\t\t" + "\t".join(self.event_types)
        for e1 in self.event_types:
            s = "%25s(%d)\t" % (e1, self.last_events[e1]['count'])
            for i in self.event_types:
                s += "%d\t" % self.em[e1][i]['count']
            print s

    def print_tree(self):
        """
        Print the fet `self.tree` (the one built only during calls to 
        `create_tree`)

        The node traversal is done using DFS. Each node data is printed using 
        `print_node`
        """
        t = "Frequent episode tree"
        print "\n%s\n%s" % (t, '='*len(t))
        self.print_node(self.tree, root=True)

    def print_fet(self):
        """
        Print the fet `self.fet` (the one that is automatically updated (unless 
        `self.offline` is set)

        The node traversal is done using DFS. Each node data is printed using 
        `print_node`
        """
        t = "Frequent episode tree FET"
        print "\n%s\n%s" % (t, '='*len(t))
        self.print_node(self.fet, root=True)

    def print_node(self, node, root=False):
        """
        Print the node, and its children. Call the `print_node` function on the 
        children.

        Parameters
        ----------
        `node`: dict
            The node to print. Needs the 'level', 'event type', 'level', 
            'count', 'children'
        `root`: bool, optional
            Set it to True when the node to print is the root of the tree.
        """
        if root:
            print "%s%s" % ("\t" * node['level'], node['event type'])
        else:
            print "%s%s: %d" % ("\t" * node['level'], node['event type'], node['count'])
        for child in node['children'].values():
            self.print_node(child)


    def attached_to_root(self, node):
        """
        Check if there a path from `node` to a '__ROOT__' node

        Parameters
        ----------
        `node`: dict
            Needs to have 'event type' and 'parent' keys.

        Returns
        -------
        `attached`: bool
            True is there is a path to __ROOT__, False otherwise
        """
        if node['event type'] == '__ROOT__':
            return True
        parent = node['parent']
        while parent:
            if parent['event type'] == '__ROOT__':
                return True
            parent = parent['parent']
        return False


    def episode(self, node):
        """ 
        Build the episode described in `node`. 
        
        The node has to be linked to the root of the tree it is supposed to 
        belong to. The prefix of the node is traversed, inserting the seen 
        nodes event types in the built episode.

        Parameters
        ----------
        `node`: dict
            The node attached to the episode we want to build. It needs 
            'parent' and 'event types' keys

        Returns
        -------
        `ep`: list
            The episode, as an (ordered) list of event types
        """
        assert self.attached_to_root(node)
        ep = [node['event type'], ]
        n = node
        while n['parent']['event type'] != '__ROOT__':
            n = n['parent']
            ep.insert(0, n['event type'])
        return tuple(ep)


    def verify_apriori_property(self, child):
        """
        Verification function. Given a child node and its parent, checks that 
        the apriori property between the 2 episodes (one is a subepisode of the 
        other) is respected, ie that the subepisode is less frequent than its 
        super episode.

        Parameters 
        ----------
        `child`: dict
        """
        parent = child['parent']
        logging.debug("    Vérif de apriori pour %s (%d) U %s: fq tot %d" 
                % (self.episode(parent), parent['count'], 
                    child['event type'], child['count']))
        
        # Episode FILS : TQ normalement plus courte, avec systématiquement une 
        # correspondance dans la TQ père.
        for tq in range(len(child['TQ'])):
            assert parent['TQ'][tq][0] <= child['TQ'][tq][0]
            assert parent['TQ'][tq][1] <= child['TQ'][tq][1]
        
        # Pour chaque occ de l'ep fils, il existe une occ de l'ep père, tel que 
        # même début, et fin peu de temps après.
        for x in child['TQ']:
            match = False
            for y in parent['TQ']: # y: occ de l'ep pere
                if y[0] == x[0] and y[1] <= x[1]:
                    match = True
            assert match
        
        assert child['count'] <= parent['count']
        return True

if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()

    # DATA FILE
    parser.add_argument('-f', '--filename', 
            metavar = 'DATASET',
            required = True, 
            help = 'Read DATASET dump file')

    parser.add_argument('-v', '--verbose', 
            action = 'store_true', 
            help = "Set logging to debub mode")

    general_tools.frequent_episode_search.argument_parser_for_frequent_episodes(parser)
    parser.add_argument('-o', '--offline',
            action='store_true', default=False,
            help= ("Don't update the FET with incoming events, just EM. " +
                "The FET is then built manually"))


    ## READ THE PARAMETERS
    options = parser.parse_args()

    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    print ("OPTIONS: \n%s" % 
            '\n'.join(['\t%s:\t%s' % (a[0], a[1]) 
                for a in vars(options).items()]))

    from general_tools.dataset.parser import read_dataset
    with open(options.filename, 'r') as f:
        dataset = read_dataset(f)

    cg = CG_EM_FET([et for et in dataset.items], 
            episode_length=options.episode_length,
            minimum_support = options.minimal_frequency,
            episode_capacity = options.episode_capacity,
            memory_length = options.memory_length,
            offline = options.offline)

    i = 0
    for event in dataset.events:
        i += 1
        if (i % 1000) == 0:
            print "%d events" % i
        cg.new_event(event)

    cg.print_em()
    cg.print_fet()

    if options.offline:
        cg.create_tree()
        cg.print_tree()
