from Import import *

def datafilter(dataset, tolerance=10):
    continuous = {}
    events = []
    for event in dataset.events:
        if event.label in continuous:
            if event.timestamp - continuous[event.label][1].timestamp <= tolerance:
                continuous[event.label][1] = event
            else:
                if ' start' in event.label:
                    events.append(continuous[event.label][0])
                elif ' end' in event.label:
                    events.append(continuous[event.label][1])
                continuous[event.label] = [event, event]
        else: 
            continuous[event.label] = [event, event]
    for k, v in continuous.items():
        if ' start' in event.label:
            events.append(v[0])
        elif ' end' in event.label:
            events.append(v[1])
    events.sort()
    logging.info(' Finished filtering events with delta_t = %ds. %d events left.' % (tolerance, len(events)))
    return events

if __name__ == "__main__":
    parser = Parser()

    parser.add_argument('-f', '--file', dest = 'dbname', 
            metavar = 'DATABASE', help = 'load DATABASE')
    parser.add_argument('-d', '--destination')
    parser.add_argument('-t', '--tolerance', type=int, default=10)
    arguments = parser.parse_args()
    with open(arguments.dbname, 'r') as f:
        data = general_tools.dataset.parser.read_dataset(f)

    e = datafilter(data, tolerance=arguments.tolerance)


    events = []
    import time
    for E in e:
        events.append('%s %s' % (time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(E.timestamp)), E.label))

    if not arguments.destination:
        arguments.destination = arguments.dbname.rsplit('/', 1)[0]
    filename = '%s/%s_filtered%ds.txt' % (arguments.destination, data.name, arguments.tolerance)
    with open(filename, 'w') as f:
        f.write('\n'.join(events))


