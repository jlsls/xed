# -*- coding: utf-8 -*-

from Import import *
class Occurrence:
    def __init__(self, episode, l):
        self.episode = episode
        self.events = l#frozenset(e for e in l)

    def __len__(self):
        return len(self.events)

    def __eq__(self, occurrence):
        #return self.events == occurrence.events
        return self.episode.id == occurrence.episode.id and \
                self.events == occurrence.events

    def __hash__(self):
        return hash(frozenset(self.events))

    def __str__(self):
        l = list(self.events)
        l.sort(cmp=lambda x,y: cmp(x.timestamp, y.timestamp))
        return str(l)

    def __repr__(self):
        return str(self)

    def __cmp__(self, occurrence):
        return cmp(self.beginning(), occurrence.beginning())
#        from time import mktime, localtime
#        import logging
#        #logging.basicConfig(level=logging.DEBUG)
#        logging.basicConfig(level=logging.INFO)
#        t = mktime(localtime())
#        evts = [e for e in self.events]
#        evts.sort()
#        evts_ = [e for e in occurrence.events]
#        evts_.sort()
#        logging.info(" . timing CMP - evts: " +
#                str(mktime(localtime())-t) + "s. ")
#
#        for i in range(min(len(evts), len(evts_))):
#            if cmp(evts[i], evts_[i]):
#                logging.info(" . timing CMP :" +
#                        str(mktime(localtime())-t) + "s. ")
#                return cmp(evts[i], evts_[i])
#        # Si une occurrence est plus longue, ça craint...    
#        logging.info(" . timing CMP :" +
#                str(mktime(localtime())-t) + "s. ")
#        return 0

    def beginning(self):
        return min(self.events)

    def end(self):
        return max(self.events)

    def get_ith_event(self, i):
        evts = [e for e in self.events]
        evts.sort()
        return evts[i]

    def collision(self, event_list):
        is_in = lambda x: x in event_list
        are_in = map(is_in, self.events)
        if any(are_in):
            return True
        else:
            return False
