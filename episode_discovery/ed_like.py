#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging 
import itertools
import time

from dataset import Event, Database, Episode
import dataset.maximal_episodes as maximal_episodes

class ED_like_episode_miner:
    def __init__(self, minimal_frequency = 5):
        self.minimal_frequency = minimal_frequency
        self.episodes = set()
        self.rare_episodes = []
        self.occurrences = {}

        pass

    def mine(self, maximal_episodes):
        t = time.clock()

        for me in maximal_episodes:
            self.episodes.add(Episode([e.label for e in me]))
        # intersections of maximal episodes
        for me1, me2 in itertools.permutations(maximal_episodes, 2):
            intersec = [e.label for e in me1 
                    if e.label in [e2.label for e2 in me2]]
            if intersec:
                self.episodes.add(Episode(intersec))

        # difference between a maximal episode and an interesting episode
        for me, ie in itertools.product(maximal_episodes, self.episodes):
            diff = [e.label for e in me if e.label not in ie.contents]
            if diff:
                self.episodes.add(Episode(diff))

        logging.info(" finished building the set of candidate episodes. "
                "%d episodes, in %.2fs" 
                % (len(self.episodes), time.clock()-t))
        
        # Retrieving the occurrence times
        rm = 0
        for episode in self.episodes:
            self.occurrences[episode] = []
            length = len(episode.contents)
            for me in maximal_episodes:
                # Check if me contains the searched episode
                labels = [e.label for e in me]
                if any(map(lambda x: x not in labels, episode.contents)):
                    continue

                if self.occurrences[episode]:
                    # last timestamp in the last occurrence of the episode
                    last = self.occurrences[episode][-1][-1].timestamp 
                else:
                    last = 0

                occ = []
                seen = []
                i = 0
                for e in me:
                    if e.timestamp <= last:
                        continue
                    if e.label in episode.contents and e.label not in seen:
                        occ.append(e)
                        seen.append(e.label)
                        i += 1

                    # The occurrence is complete, store it and start the  
                    # construction of a new one
                    if i == length:
                        self.occurrences[episode].append(occ)
                        last = occ[-1].timestamp
                        seen = []
                        occ = []
                        i = 0
            
            # Extract the rare episodes
            if len(self.occurrences[episode]) < self.minimal_frequency:
                rm += 1
                self.rare_episodes.append(episode)
        logging.info(" finished retrieving the occurrence times, "
                "and extracting the rare episodes. "
                "%d frequent episodes found in a total of %.2fs"  
                % (len(self.episodes)-len(self.rare_episodes), 
                    time.clock() - t))


if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO)

    from argparse import ArgumentParser
    
    from dataset.parser import read_dataset
    parser = ArgumentParser()

    # DATA FILE
    parser.add_argument('-f', '--filename', 
            metavar = 'DATASET',
            required = True, 
            help = 'Read DATASET dump file')
    # OTHER PARAMS (episode characteristics + frequency constraints)
    general_tools.frequent_episode_search.\
            argument_parser_for_frequent_episodes(parser)

    ## READ THE PARAMETERS
    options = parser.parse_args()

    print ("OPTIONS: \n%s" % 
            '\n'.join(['\t%s:\t%s' % (a[0], a[1]) 
                for a in vars(options).items()]))

    with open(options.filename, 'r') as f:
        dataset = read_dataset(f)

    meb = maximal_episodes.MaximalEpisodesBuilder(
            episode_length = options.episode_length,
            episode_capacity = options.episode_capacity,
            memory_length = options.episode_length)
    
    edl = ED_like_episode_miner()
    edl.mine(meb.build_maximal_episodes(dataset))

