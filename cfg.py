## Counters, as global variables
COUNTER_iteration = 0


## CONFIG PREANALYSIS
# Parametres d'apparence
conf_destination="."
conf_hist_color='r'
conf_hist_linewidth=10
conf_xticks_rotation=25
conf_xticks_fontsize='small'

# Autres
conf_is_punctual=180

