#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Heierman version
'''

import itertools
import math
import logging
import pickle
from numpy import mean, std, median
import os

from DB_Utils import db_connection, list_event_types
from Utils import datetime2unix
import utils
import utils.init_log


def ed(events, episode_length=1800, episode_capacity=None, allow_non_periodic=False, log_path='/tmp'):
	""" 
	Original verion of EpisodeDiscovery, Heirerman et al., 2004
	(implementation based on the details given in Heierman's Ph.D)

	Params:
	-------
	episode_length: maximal duration of the episode occurrences 
	episode_capacity: maximal length (number of labels) of the episodes
	"""
	found_interesting_episode = True

	# Build the maximal episodes
	max_episodes = build_max_episodes(events, episode_length, episode_capacity)
	print len(max_episodes), "max episodes"
	
	# Build the first set of candidate episodes
	candidates = build_candidates(max_episodes)
	print len(candidates), "candidate symbolic patterns"

	periodic_episodes = []
	headers_fact = ('support', 'length', 'min_duration', 'max_duration', 'mean_duration', 'median_duration', 'std_duration', 'type (0:fine, 1:coarse)', 'error_count', 'error_proportion', 'interval_count')
	headers_cand = ('support', 'length', 'min_duration', 'max_duration', 'mean_duration', 'median_duration', 'std_duration')
	round_id = 0
	
	with open(os.path.join(log_path, 'ED_factorizations.csv'), 'w') as f:
		f.write('episode,' + ','.join(headers_fact) + '\n')

	while found_interesting_episode:
		round_id += 1
		log_path_this_round = initialize_logs_for_new_round(log_path, round_id)
		with open(os.path.join(log_path_this_round, 'ED_candidates.csv'), 'w') as f:
			f.write('episode,' + ','.join(headers_cand) + '\n')
		found_interesting_episode = False

		best_descriptions = {} # compression ration > 1.0
		dataset_length = len(events)*3 # previous length
		max_episodes = build_max_episodes(events, episode_length, episode_capacity)

		# Study the candidates:
		i = 1
		for candidate in candidates:
			i += 1
			occurrences, dict_draft = find_occurrences(candidate, max_episodes)
			if not occurrences:
				continue
			
			info = {}
			info['support'] = len(occurrences)
			durations = map(lambda o: o[-1]['date'] - o[0]['date'], occurrences)

			### 
			### Log info on candidate
			###
			if durations:
				info['min_duration'] = min(durations)
				info['max_duration'] = max(durations)
				info['mean_duration'] = mean(durations)
				info['median_duration'] = median(durations)
				info['std_duration'] = std(durations)
				info['length'] = len(dict_draft['pattern symbols'])
			with open(os.path.join(log_path_this_round, 'ED_candidates.csv'), 'a') as f:
				f.write(';'.join(candidate))
				for key in headers_cand:
					f.write(',' + str(info[key]))
				f.write('\n')
			
			###
			### FINE GRAINED PERIODIC DESCRIPTION
			###

			# Init the descriptive structure
			d_fg = empty_finegrained_periodic_descr()
			d_fg['pattern length'] = len(candidate)
			d_fg.update(dict_draft)
			d_fg['symbolic pattern count'] = len(d_fg['pattern symbols'])
			d_fg['occurrence count'] = len(occurrences)
			d_fg['initial timestamp'] = occurrences[0][0]['date']
			# Compute the periodic description
			intervals = compute_intervals(occurrences, step=3600)
			if len(intervals) == 0:
				continue
			compute_periodic_description(
					d_fg, 
					intervals, 
					occurrences, 
					tolerance=3600)
			# Compute the compression rate
			descr_size = compute_descr_size(d_fg)
			new_size = float(dataset_length - 
					3 * d_fg['pattern length'] * d_fg['occurrence count'] + 
					descr_size)
			d_fg['compression rate'] = float(dataset_length) / new_size


			###
			### COARSE GRAINED PERIODIC DESCRIPTION
			###
			
			# Init the descriptive structure
			d_cg = empty_coarsegrained_periodic_descr()
			d_cg.update(dict_draft)
			d_cg['symbolic pattern count'] = len(d_cg['pattern symbols'])
			d_cg['pattern length'] = len(candidate)
			d_cg['occurrence count'] = len(occurrences)
			d_cg['initial timestamp'] = occurrences[0][0]['date']
			# Compute the periodic description
			intervals = compute_intervals(occurrences, step=86400)
			if len(intervals) == 0:
				continue
			compute_periodic_description(
					d_cg, intervals, occurrences, tolerance=86400)
			# Compute the compression rate
			descr_size = compute_descr_size(d_cg)
			new_size = float(dataset_length \
					- 3 * d_cg['pattern length'] * d_cg['occurrence count'] \
					+ descr_size)
			d_cg['compression rate'] = float(dataset_length)/new_size


			###
			### NON-PERIODIC DESCRIPTION
			###
			d_np = empty_non_periodic_descr()
			d_np.update(dict_draft)
			d_np['symbolic pattern count'] = len(d_np['pattern symbols'])
			d_np['pattern length'] = len(candidate)
			d_np['occurrence count'] = len(occurrences)
			descr_size = compute_nonperiodic_descr_size(d_np)
			new_size = float(dataset_length - 
					3 * d_np['pattern length'] * d_np['occurrence count'] + 
					descr_size)
			d_np['compression rate'] = dataset_length / new_size

			# Store the best representation
			cr_fg = d_fg['compression rate']
			cr_cg = d_cg['compression rate']
			cr_np = d_np['compression rate']
			best_compr = 1
			best_descr = None
			if cr_fg > best_compr:
				best_compr = cr_fg
				best_descr = d_fg
			if cr_cg > best_compr:
				best_compr = cr_cg
				best_descr = d_cg
			if allow_non_periodic and cr_np > best_compr:
				best_compr = cr_np
				best_descr = d_np
			if best_descr:
				found_interesting_episode = True
				best_descriptions[candidate] = (best_descr, occurrences, info)
				assert all(map(lambda x: x in events, itertools.chain(*occurrences)))

				info['interval_count'] = best_descr['interval count']
				info['error_count'] = best_descr['mistake count']
				if best_descr['type'] == "fine grained":
					info['type'] = 0
				else:
					assert best_descr['type'] == "coarse grained"
					info['type'] = 1
				info['error_proportion'] = float(info['error_count']*info['interval_count']) / info['support']


		if not best_descriptions:
			print "That's the end: no good episode was found"
			found_interesting_episode = False
			break

		# Sort the descripitons. Choose the best. 
		the_very_best = None
		cand, descr = sorted(best_descriptions.items(), key=lambda d: d[1][0]['compression rate'], reverse=True)[0]
		#print
		#print_dict(descr[0])
		the_very_best = cand
		to_be_rm_occurrences = descr[1]
		assert all(map(lambda x: x in events, itertools.chain(*to_be_rm_occurrences)))
		for maxep in max_episodes:
			maxepset = set([e['label'] for e in maxep])
			if the_very_best < maxepset:
				diffset = maxepset - the_very_best
				candidates.add(frozenset(diffset))
		candidates.remove(cand)
		assert all(map(lambda x: x in events, itertools.chain(*to_be_rm_occurrences)))
		for evt in itertools.chain(*to_be_rm_occurrences):
			events.remove(evt)
		
		with open(os.path.join(log_path, 'ED_factorizations.csv'), 'a') as f:
			f.write(';'.join(cand))
			for k in headers_fact:
				f.write(',' + str(descr[2][k]))
			f.write('\n')
		periodic_episodes.append(descr[0])
	return periodic_episodes


def empty_description():
	d = {
			'pattern length':				0,
			'symbolic pattern count':		0,
			'pattern symbols':				[],
			'occurrence count':				0,
			'initial timestamp':			0, # timestamp value
			'pattern variation entries':	[],
			'event time intervals':			[],
			'compression rate':				1.,
			}
	return d

def empty_finegrained_periodic_descr():
	d = empty_description()
	d.update({
			'type':							'fine grained',
			'interval count':				0,
			'interval sequence values':		[],
			'interval sequence validity':	[], # not in original ED, but necessary 
			'pattern start times':			[], # not in original ED, but necessary
			'mistake count':				0,
			'mistakes':						[],
			})
	return d

def empty_coarsegrained_periodic_descr():
	d = empty_description()
	d.update({
			'type':							'coarse grained',
			'interval count':				0,
			'interval sequence values':		[],
			'interval sequence validity':	[], # not in original ED, but necessary 
			'pattern start times':			[],
			'mistake count':				0,
			'mistakes':						[],
			})
	return d

def empty_non_periodic_descr():
	d = empty_description()
	d.update({
			'type':							'non periodic',
			'occurrence start timestamps':	[],
			})
	return d

def compute_periodic_description(
		d, 
		intervals, 
		occurrences, 
		beta_cv=0.01, beta_ac=.9, 
		tolerance=3600):
	
	length = float(len(intervals))
	mean = sum(intervals)/length
	sample_variance = sum(map(lambda x: (x-mean)**2, intervals)) / length
	coeff_of_variation = math.sqrt(sample_variance) / length

	if coeff_of_variation <= beta_cv:
		# Interval sequence of length 1
		update_descr(d, 1, intervals, occurrences, tolerance)

	else:
		divisor = sum(map(lambda x: (x-mean) ** 2, intervals))
		max_tau = 0
		max_k = 1
		for k in xrange(1, len(intervals)/4):
			tau_k = sum(map(
				lambda x, y: (x-mean)*(y-mean), intervals[:-k], intervals[k:]
				))
			if tau_k > max_tau and tau_k >= beta_ac:
				max_k = k
				max_tau = tau_k
		update_descr(d, max_k, intervals, occurrences, tolerance)


def update_descr(d, interval_count, intervals, occurrences, tolerance=3600):
	d['interval count'] = interval_count
	occurrence_start_times = [o[0]['date'] for o in occurrences]
	start_time = d['initial timestamp']
	occurrence_count = len(occurrences)
	occurrence_index = 0

	while occurrence_index < occurrence_count:
		if occurrence_index + interval_count < occurrence_count:
			# There are enough occurrences left: we can define a new interval 
			# sequence
			current_interval_sequence = intervals[
					occurrence_index:occurrence_index + interval_count]
			pos_in_interv_seq = 0
			# Fit as many occurrences as possible
			while (occurrence_index < occurrence_count - 1 and
					intervals[occurrence_index] == \
							current_interval_sequence[ 
								pos_in_interv_seq % interval_count]):
				assert abs(start_time-occurrence_start_times[occurrence_index] 
						< tolerance)
				start_time += current_interval_sequence[
						pos_in_interv_seq % interval_count]
				d['pattern start times'].append(
						occurrence_start_times[occurrence_index] - start_time)
				assert d['pattern start times'][-1] < tolerance
				pos_in_interv_seq += 1
				occurrence_index += 1
			# Save the interval sequence
			d['interval sequence values'].append(current_interval_sequence)
			d['interval sequence validity'].append(pos_in_interv_seq)

			if occurrence_index == occurrence_count:
				# All occurrences have been described. Nothing left to do
				return
			else:
				# There was a mistake
				d['mistakes'].append(occurrence_start_times[occurrence_index])
				d['mistake count'] += 1
				start_time = d['mistakes'][-1]
			pass
		else:
			# Not enough occurrences left: all the remaining occurrences are 
			# considered as mistakes
			d['mistakes'].append(occurrence_start_times[occurrence_index])
			d['mistake count'] += 1
			start_time = d['mistakes'][-1]
			assert abs(start_time - occurrence_start_times[occurrence_index] 
					< tolerance)
			occurrence_index += 1

			#TODO
			pass



def build_max_episodes(events, episode_length, episode_capacity):
	"""
	Exactly the paper algorithm
	"""
	w = [] # event folding window
	max_episodes = [] 
	for event in events:
		created = False
		if episode_capacity and len(w) == episode_capacity:
			max_episodes.append(w[:])
			created = True
			w.pop(0)

		ts = event['date'] - episode_length
		while w and w[0]['date'] < ts:
			if created == False:
				max_episodes.append(w[:])
				created = True
			w.pop(0)
		w.append(event)
	max_episodes.append(w[:])
	return max_episodes


def build_candidates(max_episodes):
	candidates = set()

	# The original candidates: extracted from the max episodes
	for max_ep in max_episodes:
		items = set()
		for event in max_ep:
			items.add(event['label'])
		candidates.add(frozenset(items))

	# Extra candidates: the non empty intersections of max episodes
	for e1, e2 in itertools.combinations(candidates, 2):
		intersec = e1 & e2
		if intersec:
			candidates.add(intersec)
	
	return candidates

def find_occurrences(candidate, max_episodes):
	occurrences = []
	pattern_variations = []
	pattern_variation_entries = []
	event_time_intervals = []
	seen_ts = []
	event_time_intervals = []
	candidate_length = len(candidate)

	for max_ep in max_episodes:
		# Occurrences are subsequences of the maximal episodes
		found_new_occurrence = True
		while found_new_occurrence:
			found_new_occurrence = False
			items = []
			evts = []
			pattern_variation = []
			event_interv = []
			last_date = 0
			i = 0 # length of the occurrence under construction
			for evt in max_ep:
				if (evt['label'] not in candidate # not the right label
						or evt['label'] in pattern_variation # label already used for the occ under construction
						or evt['date'] in seen_ts # event already used
						):
					continue
				evts.append(evt)
				pattern_variation.append(evt['label'])
				i += 1
				if last_date:
					event_interv.append(evt['date'] - last_date)
				last_date = evt['date']
				if i == candidate_length:
					# Occurrence is complete
					found_new_occurrence = True
					seen_ts.extend([e['date'] for e in evts])
					occurrences.append(evts)
					if pattern_variation not in pattern_variations:
						pattern_variations.append(pattern_variation)
					pattern_variation_entries.append(
							pattern_variations.index(pattern_variation))
					event_time_intervals.append(event_interv)
					break


	return occurrences, {'pattern symbols': pattern_variations, 
			'pattern variation entries': pattern_variation_entries, 
			'event time intervals': event_time_intervals}


def compute_intervals(occurrences, step=3600):
	intervals = []
	last_ts = (occurrences[0][0]['date']/step)*step
	for i in xrange(1, len(occurrences)):
		new_ts = (occurrences[i][0]['date']/step)*step
		intervals.append(new_ts - last_ts)
		last_ts = new_ts
	return intervals

		 
def print_dict(d):
	print 
	for k, v in d.items():
		print "%s:\t%s" % (k, v)

def compute_descr_size(d):
	descr_size = (1 + #pattern length |\lambda|
			1 + #symbolic pattern count |\Lambda|
			d['pattern length'] * d['symbolic pattern count'] + 
					# pattern symbols
			1 + # occurrence count |\psy|
			2 + # initial timestamp
			d['occurrence count'] + # pattern variation entries  
					#FIXME ED says |\psy| - |\alpha|
			d['occurrence count'] * (d['pattern length'] - 1) +  
					# event time interval  
					#FIXME ED says (|\psy| - |\alpha|) - (|\lambda| - 1)
			1 + # interval count
			2 * d['interval count'] * (d['mistake count']-1) +  
					# interval sequence values
			len(d['pattern start times']) + # 0 si fine-grained
			1 + # mistake count
			d['mistake count']) #mistakes
	return descr_size

def compute_nonperiodic_descr_size(d):
	descr_size = (1 + #pattern length |\lambda|
			1 + #symbolic pattern count |\Lambda|
			d['pattern length'] * d['symbolic pattern count'] +  
					# pattern symbols
			1 + # occurrence count |\psy|
			2 + # initial timestamp
			d['occurrence count'] + # pattern variation entries  
					#FIXME ED says |\psy| - |\alpha|
			d['occurrence count'] * (d['pattern length'] - 1) +  
					# event time interval  
					#FIXME ED says (|\psy| - |\alpha|) - (|\lambda| - 1)
			2 * len(d['occurrence start timestamps']))
	return descr_size

def initialize_logs_for_new_round(log_path, round_id):
	path = os.path.join(log_path, 'iteration%02d' % round_id)
	os.makedirs(path)
	return path

if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('-d', '--dbname', help="MySQL database", required=True)
	parser.add_argument('-t', '--table_name', default="EVENT", 
			help="Name of the table where the events are stored. Should have a 'timestamp' and a 'label' columns")
	parser.add_argument('--log_directory', default='/home/julie/xED/Documents/exec_results')
	
	parser.add_argument('--episode_length', default=1800, type=int)
	parser.add_argument('--episode_capacity', default=None, type=int)
	parser.add_argument('--allow_non_periodic', action='store_true', help="Also consider frequent, non periodic patterns")
	parser.add_argument("-v", "--verbose", action="store_true", help="Verbose")

	options = parser.parse_args()
	
	if options.verbose:
		logging.basicConfig(level=logging.DEBUG)
	else:
		logging.basicConfig(level=logging.INFO)
	
	log_path = utils.init_log.from_options(options, name="ED")

	logging.info("\n\t".join([" Parameters used", ] + 
		["%s: %s" % (k, str(v)) for k, v in options.__dict__.items()]))
	cursor = db_connection(options.dbname)
	cursor.execute("SELECT timestamp, label FROM %s ORDER BY timestamp" % options.table_name)
	
	events = []
	for timestamp, label in cursor:
		events.append(
				{'date': datetime2unix(timestamp), 
					'label': label}
				)

	print len(events), "events"
	periodic_episodes = ed(events, 
			episode_length = options.episode_length,
			episode_capacity = options.episode_capacity,
			allow_non_periodic = options.allow_non_periodic,
			log_path = log_path)
	i = 0
	for pe in periodic_episodes:
		i += 1
		print
		print "%dth episode:" % i
		print_dict(pe)

	with open(os.path.join(log_path, "ED_results.csv"), "w") as f:
		f.write("\n".join(["%s;%s;%s" % 
				(','.join(pe['pattern symbols'][0]), 
					pe['compression rate'],
					pe['type']) for pe in periodic_episodes]))
	with open(os.path.join(log_path, "ED_results.dump"), "w") as f:
		pickle.dump(periodic_episodes, f)

