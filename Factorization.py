# -*- coding: utf-8 -*-
COUNTER_fac_id = 0
from numpy import mean, std, median
import logging
import matplotlib.pyplot as plt
import pylab as pl
from Utils import str_rel_time, epoch2date

def get_obs_times(occurrences):
    """
    get the start times of the occurrences (an occurrence == a list of events
    """
    start_times = []
    for occurrence in occurrences:
        first_evt = min(occurrence)
        start_times.append(first_evt.timestamp)
    return start_times

class FactorizeEpisode:
    def __init__(self, episode, occurrences):
        global COUNTER_fac_id
        self.episode = episode
        self.occurrences = occurrences
        self.concerned_events = []
        COUNTER_fac_id += 1
        self.fac_id = COUNTER_fac_id


    def __eq__(self, factor):
        return self.episode == self.episode \
                and self.occurrences == self.occurrences

    def __str__(self):
        s = "\nFactorization by %d: %s\n" % (self.episode.id, self.episode.contents)
        s += "--------------------\n"
        s += str(self.episode.periodicity)
        s += "\n\nPattern Quality:\n"
        s += str(self.episode.pattern_quality) + "\n"
        return s
        #return "factorization by: " + str(self.episode)

    def __repr__(self):
        return str(self)

    def plot_factor(self, ep_length, position):
        ep = self.episode

        # plot occurrences
        for o in self.episode_occurrences:
            mini = epoch2date(o.beginning().timestamp)
            maxi = epoch2date(o.end().timestamp)
            pl.plot_date([mini,maxi], [position,position], 'gx-')
        return
        
    def collides(self, factor):
        is_in = lambda x: x in factor.concerned_events
        return any(map(is_in, self.concerned_events))

        

class FactorizePeriodicEpisode(FactorizeEpisode):
    def __init__(self, episode, epi_occs):
        global COUNTER_fac_id
        self.episode_occurrences = epi_occs
        self.episode = episode
        self.concerned_events = []
        self.missing_characterization = []
        
        COUNTER_fac_id += 1
        self.fac_id = COUNTER_fac_id
        #rq: episode has a pat_qual attribute
        self._info = {}

    def __eq__(self, factor):
        return self.episode_occurrences == factor.episode_occurrences

    def stats(self):
        self._info = {'len': len(self.episode), 'count': len(self.episode_occurrences)}
        durations = map(lambda o: o[-1].timestamp -o[0].timestamp, self.episode_occurrences)

        self._info['min_duration'] = min(durations)
        self._info['max_duration'] = max(durations)
        self._info['mean_duration'] = mean(durations)
        self._info['std_duration'] = std(durations)
        self._info['median_duration'] = median(durations)
        self._info['ncomp'] = len(self.episode.periodicity.description)
        self._info['as_expected_count'] = len(self.episode.pattern_quality.as_expected)
        self._info['extra_count'] = len(self.episode.pattern_quality.extra)
        self._info['missing_count'] = len(self.episode.pattern_quality.missing)
        self._info['expected_count'] = self._info['as_expected_count'] + self._info['missing_count']
        self._info['accuracy'] = float(self._info['as_expected_count'])/self._info['expected_count']
        self._info['description_coverage'] = float(self._info['as_expected_count'])/self._info['count']
        self._info['period'] = self.episode.periodicity.period 
        return self._info

    def plot_histogram(self, path='', form='png', interactive=False, plot_number=(1,1,1), bin_width=1800):
        print "Factorization: %s" % self
        print "Pat qual: %s" % self.episode.pattern_quality
        assert isinstance(self.episode.pattern_quality.offset, float)
        offset = self.episode.pattern_quality.offset

        period = self.episode.periodicity.period
        bins = range(0, period+bin_width, bin_width)
        
        print self.episode_occurrences[0][0]
        start_times = get_obs_times(self.episode_occurrences)
        start_times = [i + offset for i in start_times]
        period = self.episode.periodicity.period

        episode_start = self.episode.periodicity.start
        episode_end = self.episode.periodicity.end
        relative_times = [t%period for t in start_times if t>= episode_start and t<= episode_end]
        min_stddev = min(i[1] for i in self.episode.periodicity.description)
        if min_stddev:
            nb_bins = min(int(period)/(min_stddev/5), 100)
        else:
            nb_bins = 50

        logging.info(" Printing histogram for episode " +str(self.episode))
        for i in self.episode.periodicity.description:
            plt.axvspan((i[0]+offset-2*i[1])%period, (i[0]+offset+2*i[1])%period, 
                    color='LightSalmon')
        
        n, bins, patches = pl.hist(relative_times, bins, histtype ='bar',
                color='b', range=(0,period), edgecolor='b', 
                )
        ticks = []
        ticks_labels = []
        for i in self.episode.periodicity.description:
            print i, offset, i[0] + offset
            time_label = str_rel_time(i[0], period)[:-3]
            ticks.append((i[0]+offset)%period)
            ticks_labels.append(time_label)
            plt.axvline((i[0]+offset)%period, color='r')
            

        _xmin = min(relative_times)
        time_label = str_rel_time(_xmin-offset, period)[:-3]
        ticks.append(_xmin)
        ticks_labels.append(time_label)

        legend=True
        
        _xmax = max(relative_times)
        time_label = str_rel_time(_xmax-offset, period)[:-3]
        ticks.append(_xmax)
        ticks_labels.append(time_label)
        
        pl.xticks(ticks, ticks_labels, 
                horizontalalignment='right',
                verticalalignment='top',
                rotation=30,
                size='x-large')
        pl.yticks(pl.arange(20), size='x-large')
        s = ""
        for c in self.episode.contents:
            s += c + ", "
        s = s[:-2]
        if period == 604800:
            p = '1 week'
        else:
            p = '1 day'
        pl.title('\n'.join([
            "Distibution of episode {%s}\n" % ', '.join([i for i in self.episode.contents]), 
            "period = %s" % p, 
            ])
        )
        pl.ylabel("#occurrences", size='x-large')
        pl.legend()
        if len(self.episode.periodicity.description) > 1:
            pl.xlim(xmin=_xmin, xmax=_xmax)
        else:
            pl.xlim(xmin=_xmin*.8, xmax=_xmax*1.2)
        pl.ylim(ymin=0, ymax=max(n))
        # pgfplots
        print """
        \\begin{tikzpicture}
        \\tikzset{
            hatch distance/.store in=\\hatchdistance,
            hatch distance=15pt,
            hatch thickness/.store in=\\hatchthickness,
            hatch thickness=2pt
        }

        \\makeatletter
        \\pgfdeclarepatternformonly[\\hatchdistance,\\hatchthickness]{flexible hatch}
        {\\pgfqpoint{0pt}{0pt}}
        {\\pgfqpoint{\\hatchdistance}{\\hatchdistance}}
        {\\pgfpoint{\\hatchdistance-1pt}{\\hatchdistance-1pt}}%%
        {
            \\pgfsetcolor{\\tikz@pattern@color}
            \\pgfsetlinewidth{\\hatchthickness}
            \\pgfpathmoveto{\\pgfqpoint{0pt}{0pt}}
            \\pgfpathlineto{\\pgfqpoint{\\hatchdistance}{\\hatchdistance}}
            \\pgfusepath{stroke}
        }

        \\begin{axis}[x = 45mm/%d,
            y = 3cm/%d,
            %%ylabel = {\\# occurrences},
            xticklabels = {%s},
            xtick = {%s},
            axis on top,
            ymin = 0,
            ymax = %d,
            x tick label style={rotate=40, anchor=east, align=center, scaled x ticks = false},
            area legend, 
            legend cell align=left,
            ]
        """ % (period, max(n)+1, ', '.join(ticks_labels), ', '.join([str(i) for i in ticks]), max(n)+1)

        for m, sd in self.episode.periodicity.description:
            mps = (offset + m + 2*sd) % period
            mms = (offset + m - 2*sd) % period
            print """
            \\addplot [hatch distance = 5pt, 
                hatch thickness = 1pt,
                domain = %d:%d,
                pattern = flexible hatch,
                draw = white,
                ] {%d} \\closedcycle;
            """ % (mms, mps, max(n)+1)

        coords = ""
        for date, height in zip(bins, n):
            coords += "(%d, %d)\n" % (date, height)
        print """
        \\addlegendentry{$\\mu \\pm 2 \\cdot \\sigma$}

        \\addplot [ybar interval, draw=blue, fill=blue!25, ]
            coordinates {
                %s
            };
        \\addlegendentry{occurrences}
        """ % coords
        for m, sd in self.episode.periodicity.description:
            print """
            \\draw [red!80!black, line width=1.5pt] (axis cs:%d, 0) -- (axis cs:%d, %d); 
            """ % ((m+offset)%period, (m+offset)%period , max(n)+1)

        print """
        \\addlegendimage{line legend,red}
        \\addlegendentry{$\\mu$}
        \\end{axis}
        \\end{tikzpicture}
        """
        
        if interactive:
            plt.show()
            print n, bins, patches
            raw_input("\nPress any key to continue")
            plt.close()


        return 


    def plot_factor(self, ep_length, position):
        pe = self.episode

        # plot expected
        start = pe.periodicity.start
        end = pe.periodicity.end
        loop = -1
        count = -1
        descr = pe.periodicity.description
        period = pe.periodicity.period
        s = start - period 

        def next_(count, loop):
            count = (count + 1) % len(descr)
            if count == 0:
                loop += 1
            return descr[count][0], descr[count][1], count, loop

        while True:
            m, v, count, loop = next_(count, loop)
            if count == 0:
                s += period
            if s >= end:
                break
            x = [ epoch2date(s+m-2*v), epoch2date(s+m+2*v) ]
            pl.plot_date(x, [position, position], fmt='r-', linewidth=4)

        # plot correct occurrences
        for o in pe.pattern_quality.as_expected:
            pl.plot_date([epoch2date(o)], [position], 'bo', markersize=10)
        
        for o in pe.pattern_quality.shifted:
            pl.plot_date([epoch2date(o)], [position], 'g^', markersize=10)
        
        # plot extra occurrences
        for o in pe.pattern_quality.extra:
            pl.plot_date([epoch2date(o)], [position], 'g^', markersize=10)


    def plot_occurrence_time(self, path, form='png', interactive=False, xlim=None):
        pl.figure()

        as_exp = [i for i in self.episode.pattern_quality.as_expected]
        shifted = [i for i in self.episode.pattern_quality.shifted]
        extra = [i for i in self.episode.pattern_quality.extra]

        start = self.episode.periodicity.start
        end = self.episode.periodicity.end
        pl.axvline(epoch2date(start), color='k', 
                linestyle='--', label='Start/End of pattern')
        pl.axvline(epoch2date(end), color='k', linestyle='--')

        ticks = [[],[]]
        period = self.episode.periodicity.period
        legend=True
        for m, v in self.episode.periodicity.description:
            if legend:
                pl.axhspan(m-2*v, m+2*v, color="LightSalmon", label="Intervals in which the occurrences\nare expected to be") 
                pl.axhline(m, color='r', label='Mean expected occurrence time')
                legend=False
            else:
                pl.axhspan(m-2*v, m+2*v, color="LightSalmon") 
                pl.axhline(m, color='r')

            time_s = str_rel_time(m, period)
            ticks[0].append(m)
            ticks[1].append(time_s)
        
        ticks[0].append(period)
        ticks[1].append(str(period))
        pl.axhline(period, color='k',
                linestyle='-.')
        
        legend=True
        for t in extra:
            a = epoch2date(t)
            r = (t%period)
            if legend:
                legend=False
                pl.plot_date(a, r, 'gx', label='Extra, unexpected occurrence')
            else:
                pl.plot_date(a, r, 'gx')
        legend=True
        for t in shifted:
            a = epoch2date(t)
            r = (t%period)
            if legend:
                legend=False
                pl.plot_date(a, r, 'bx', label='Expected occurrence, but a bit shifted')
            else:
                pl.plot_date(a, r, 'bx')
        legend=True
        for t in as_exp:
            a = epoch2date(t)
            r = (t%period)
            if legend:
                legend=False
                pl.plot_date(a, r, 'kx', label='Occurrence happenning as expected')
            else:
                pl.plot_date(a, r, 'kx')

        d = 0.02*(end-start)
        pl.yticks(ticks[0], ticks[1])
        xt = plt.xticks(horizontalalignment = 'right', 
                verticalalignment = 'top',
                rotation = 30)
        pl.ylim(0, self.episode.periodicity.period*1.02)
        if xlim:
            pl.xlim(xlim[0], xlim[1])
        else:
            pl.xlim(epoch2date(start-d), epoch2date(end+d))
        dec = lambda x: x.decode('utf-8')
        text = ", ".join(map(dec, self.episode.contents))
        pl.title("Occurrence times for the episode: " + text)
        pl.legend(loc=9, bbox_to_anchor=(0.5, -0.15), fancybox=True, shadow=True)

        if interactive:
            #raw_input("\nPress any key to continue")
            pass
        else:
            name = "occurrences_fac%d_ep%d.%s" % (self.fac_id, self.episode.id, form)
            fname = '/'.join([path, name])
            pl.savefig(fname, bbox_inches='tight')
