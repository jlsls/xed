# -*- coding: utf-8 -*-
#!/usr/bin/python

import xml.dom.minidom as dom
import ast
#import data.Parameters

class Parameters:
    def __str__(self):
        s = ''
        for k, v in self.__dict__.items():
            s += '\n%s:\n----------\n' % k
            for a,b in v.items():
                s += '%s:\t%s\n' % (a, str(b))
            s += "\n"
        return s
    pass

def build_variable_name(full_name):
    return full_name.lower().replace(' ', '_')

def parse_config_file(config):
    xml_tree = dom.parse(config)
    p = Parameters()
    categories = xml_tree.getElementsByTagName('category')
    for category in categories:
        cname = category.getAttribute('variable')
        if not cname:
            cname = build_variable_name(category.getAttribute('name'))

        p.__dict__[cname] = {}
        items = category.getElementsByTagName('item')
        for item in items:
            name = item.getAttribute('variable')
            if not name:
                name = build_variable_name(item.getAttribute('name'))

            type_ = item.getAttribute('type')
            raw_value = item.getAttribute('default')
            if raw_value == "None":
                value = None
            else:
                if type_ == "dict" or type_ == "bool":
                    value = ast.literal_eval(raw_value)
                else:
                    print type_, raw_value, name
                    
                    print __builtins__[type_]
                    value = __builtins__[type_](raw_value)

            p.__dict__[cname][name] = value

    print '\n'.join([str(i) for i in p.__dict__.items()])
    return p

if __name__=="__main__":
    parse_config_file("data/parameters.xml")
