#import MySQLdb
import sys
import datetime

import logging

from episode_discovery.CG_EL import EpisodeLattice
from dataset import Event
from DB_Utils import db_connection, list_event_types


def episode_user_query(sensors):
        """
        Very simple command line interface for the choice of the considered episode
        """

        print
        print "-" * 60
        print "Available event labels:"
        print "-" * 60
        for id in sensors:
                print "%d: %s" % (id, sensors[id])
        et_ids = raw_input("\nChoose the event types composing the searched episode. Use the event types ids, separated by spaces. Will raise an error if the specified ids do not correspond to ids in the event type list\n")
        print

        et_ids = map(int, et_ids.split(" "))
        for et in et_ids:
                assert et in sensors

        logging.info("\n\t".join([" Successful event choice:", ] + ["%d: %s" % (et, sensors[et]) for et in et_ids]))
        
        return et_ids

def exec_select_relevant_events_from_labels(cursor, episode, tabname="EVENT_VIEW"):
    """
    Select the events having a label in `episode`

    Parameters:
    -----------
        cursor: valid MySQLDB connection
        episode: a list of event labels
        tabname: the name of the MySQL table where the events are stored. This 
            table needs to have a 'timestamp' and a 'label' column (optional, 
            default: EVENT_VIEW)
    """
    format_string = ", ".join(['%s',] * len(episode))
    cursor.execute( """ SELECT timestamp, label FROM EVENT_VIEW
                        WHERE label IN (%s)
                        ORDER BY timestamp
                    """ % format_string, tuple(episode))


def exec_select_relevant_events(cursor, et_ids):
    format_string = ','.join(['%s'] * len(et_ids))
    cursor.execute( """ SELECT timestamp, name AS label
                        FROM EVENT JOIN SENSOR
                            ON sensor_id = SENSOR.id
                        WHERE sensor_id IN (%s)
                        ORDER BY timestamp
                    """ % format_string, tuple(et_ids))



def get_occurrences(cursor, maximal_episode_duration):
    """
    Retrieve the occurrences of the requested episode

    It is assumed that a request to get the relevant events has already been 
    executed (either exec_select_relevant_events_from_labels() or 
    exec_select_relevant_events()) but that the result iterator has not been 
    used yet. The occurrences are retrieved thanks to an offline Episode 
    Lattice (package ..CG_EL) fed with the events.

    Parameters:
    -----------
        cursor: valid MySQLDB connection
        maximal_episode_duration: maximal duration of the episode occurrences
    """

    EL = EpisodeLattice(maximal_episode_duration=datetime.timedelta(seconds=1800),
                    minimal_support = 0,
                    window_length = None)

    logging.debug(" Searching for the occurrences of the episode...")
    episode = []
    for timestamp, label in cursor:
            # TODO: Permettre a CG_EL de ne pas avoir de window_length
            # Appliquer avec CG_EL sans window length, et min_support=0.

            event = Event(timestamp, label)
            if label not in episode:
                episode.append(label)
            EL.new_event(event)

    TQ = [i[0] for i in EL.get_occurrences(episode)]
    logging.info(" Finished occurrence search. %d ccurrences retrieved" % len(TQ))
    return TQ
        
def datetime2unix(dt):
        epoch = datetime.datetime.utcfromtimestamp(0)
        delta = dt - epoch
        return delta.total_seconds()

if __name__ == "__main__":
        from argparse import ArgumentParser
        parser = ArgumentParser()

        parser.add_argument("-d", "--dataset", help="(mysql) dataset to be used for the data generation", required=True)
        parser.add_argument("-t", "--tofile", help="destination file", default="/tmp/MME_data.txt")

        parser.add_argument("--maximal_episode_duration", type=int, default=1800)
        parser.add_argument("--period", type=int, default=86400)

        parser.add_argument("-v", "--verbose", action="store_true", help="Verbose")

        group = parser.add_argument_group("Choose the data format between relative and absolute (required)")
        group = group.add_mutually_exclusive_group(required=True)
        group.add_argument("-R", "--relative", action="store_true", help="Do the modulo operation")
        group.add_argument("-A", "--absolute", action="store_true", help="Keep the datetime object")
        
        options = parser.parse_args()

        if options.verbose:
                logging.basicConfig(level=logging.DEBUG)
        else:
                logging.basicConfig(level=logging.INFO)

        logging.info("\n\t".join([" Parameters used", ] + ["%s: %s" % (k, str(v)) for k, v in options.__dict__.items()]))
        cursor = db_connection(options.dataset)

        sensors = list_event_types(cursor)
        et_ids = episode_user_query(sensors)

        exec_select_relevant_events(cursor, et_ids)
        TQ = get_occurrences(cursor, options.maximal_episode_duration)
        with open(options.tofile, 'w') as f:
                if options.relative:
                        f.write(";\n".join([str(datetime2unix(i)%options.period) for i in TQ]))
                else:
                        f.write(";\n".join(map(str, TQ)))
                        
                f.write(";")

        logging.info(" Relative occurrence times written to %s" % options.tofile)


