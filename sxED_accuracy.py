#!/usr/bin/python
# -*- coding: utf-8 -*-


import sys
import os
import logging 
import time
import datetime
import random
import csv
import code


from dataset import Database, Event, Episode
from periodicity_analysis.MixtureModelEstimation import MixtureModelEstimator
from PeriodicityQuality import PeriodicityQualitySpecAcc
import Utils
import init_log

class Node:
    def __init__(self, mme_params, episode=tuple(), level=0, period=datetime.timedelta(days=1)):
        """
        Node in the lattice. Each node belongs to a level, is linked to the upper 
        level via its parents, and to the lower level via its children. Each node 
        corresponds to an episode, thus containing its time queue (TQ, list of 
        minimal occurrences), and the corresponding support (count of non 
        overlapping minimal occurrences). 

        Parameters:
            episode: the corresp. episode
            level: the depth of the node in the lattice. The root is at level 0.

        Variables:
            episode, TQ, support: characteristics of the episode
            parent: dict, linking the node to its parents
            child: dict, linking the node to its children
            level
        """
        self.episode = episode
        self.id = hash(episode)
        self.TQ = []
        self.support = 0

        self.parent = {}
        self.child = {}
        self.level = level

        self.mme = MixtureModelEstimator(**mme_params)
        self.periodicity_quality = PeriodicityQualitySpecAcc(self.mme, self.TQ, period)
        
        logging.debug(" NE %d %s" % (self.id, ','.join(self.episode)))

    def link_node_to_other_parents(self, known_parent, label):
        """
        Knowing one parent (linked to the current node via a known label), find all 
        the others. The known parent is not required to be linked with the current 
        node yet.

        Parameters:
            known_parent: the known parent
            label: the diference between the known_parent and the current node

        Return: Nada

        Note: All the parents of the current node are available via 
        self.parent[label].parent[e].child[label] for e in episode (e != label)
        """

        assert self.level >= 2 
        # useless otherwise: __ROOT__ has no parents, and the nodes at level 1 
        # have only one parent: __ROOT__

        for e in self.episode:
            if e == label:
                # Link to known_parent
                known_parent.child[label] = self
                self.parent[label] = known_parent
            else:
                grand_parent = known_parent.parent[e]
                other_parent = grand_parent.child[label]
                other_parent.child[e] = self
                self.parent[e] = other_parent

    def is_periodic(self, minimal_accuracy=0.7):
        return any(filter(lambda x: x >= minimal_accuracy, self.periodicity_quality.accuracy))

    def detach(self, minimal_accuracy=0.7, count=0, per_count=0):
        """
        Used when episode become too rare. The current node, and all of its children, 
        are removed from the lattice (their links with their parents are cut). 
        DFS recursivity.

        count is just a sensor, used for counting the number of detached nodes. 
        Its value is updated, and eventually returned. 
        """
        if self.parent:
            count += 1
            if self.is_periodic(minimal_accuracy=minimal_accuracy):
                per_count += 1
            logging.debug(" DE %d" % self.id)
        for parent in self.parent.keys():
            del self.parent[parent].child[parent]
            del self.parent[parent]

        for child in self.child.keys():
            count, per_count = self.child[child].detach(minimal_accuracy=minimal_accuracy, count=count, per_count=per_count)
        
        return count, per_count
    
    def recompute_support(self):
        """
        Support: non overlapping occurrences count.

        FIXME: naive, does not take advantage of previous knowledge, just recomputes the hole thing.
        Recompute the support (ie count of non overlapping occurrence) based on the 
        time queue TQ.
        """
        last_end = datetime.datetime.min
        support = 0
        for start, end in self.TQ:
            if start > last_end:
                last_end = end
                support += 1
        self.support = support
        return support

    def print_TQ(self):
        """
        Debugging purpose only. Fancy prints the time queue.
        """
        print "TQ for", self.episode
        for ts, te in self.TQ:
            date = time.strftime("%x", time.gmtime(ts))
            s = time.strftime("%X", time.gmtime(ts))
            e = time.strftime("%X", time.gmtime(te))
            print "%s, %s -> %s" % (date, s, e)

    def compute_relative_times(self, period):
        times = []
        for t_start, _ in self.TQ:
            times.append(Utils.datetime2relativeposition(t_start, period))
            #times.append( ((t_start - datetime.datetime.min).seconds + period.total_seconds()/2) % period.total_seconds() )
        return times

class EpisodeLattice:
    def __init__(self, 
            maximal_episode_duration=datetime.timedelta(seconds=1800), 
            minimal_support=5,
            maximal_depth=None, 
            minimal_accuracy=0.7,
            window_length=datetime.timedelta(weeks=1),
            mme_params={},
            log_directory = "/tmp",
            period = 86400,
            breakpoints=[]):
        """
        Frequent episode lattice. Can be built offline (calls to 
        new_event_not_stream for each incoming event, followed by a call to 
        build_lattice when the lattice is needed), or online (calls to new_event 
        for each incoming event). 

        Parameters:
            maximal_episode_duration: int, maximal duration of an occurrence, defaults to 1800
            minimal_support: int, support threshold for an episode to be considered as 
                frequent, defaults to 5
            maximal_depth: int, maximal depth of the lattice (ie maximal length 
                of the episodes), defaults to None
            window_length: int, duration of the "recent past" sliding window. All events 
                and episode occurrences older than window_length are deleted. Defaults to 
                one week

        Variables:
            root: the root of the offline lattice
            ROOT: the root of the online lattice
            RMN: layered list of the recently modified nodes: the only nodes that can 
                possibibly be extended with an incoming event
        """

        self.maximal_episode_duration = maximal_episode_duration
        self.minimal_support = minimal_support
        self.maximal_depth = maximal_depth
        self.window_length = window_length

        # Some quality/complexity sensors
        self.n_events = 0
        self.n_frequent = 0     # Fq episode count. FIXME: not safe yet
        self.n_periodic = 0     # Periodic episode count. FIXME: not safe yet
        self._nnew_fq = 0   # Reinit for each new event
        self._nnew_rare = 0
        self._nnew_per = 0
        self._nnew_np = 0
        self._nrmd_evt = 0

        # For periodicity computation
        self.mme_params = mme_params
        self.period = datetime.timedelta(seconds=period)#86400
        self.minimal_accuracy = minimal_accuracy


        # Struct for the streaming version
        self.ROOT = Node(self.mme_params, period=self.period)
        self.RMN = [set(), ]
        self.RMNheight = 1
        
        self.log_directory = log_directory
        self.logf = csv.writer(open(os.path.join(self.log_directory, 'event_updates.csv'), "w"))
        self.perepis = csv.writer(open(os.path.join(self.log_directory, 'periodic_episodes.csv'), "w"))
        self.logf.writerow(["timestamp", "n_events", "n_rmd_events", "n_newfq", "n_newrare", "n_frequent", "n_newper", "n_newnonper", "n_per", "n_nonper", "time"])
        self.perepis.writerow(["timestamp", "prev_nper", "post_nper", "episode", "status"])

        # For result investigation
        self.breakpoints = breakpoints

        self.start_time = time.clock()
        
    def new_event(self, event, eid=0):
        """
        Lattice update, streaming version. 
        1. Add the new MO to the length 1 episode
        2. Add the new MOs to the longer episodes
        3. Remove outdated information
        Events are assumed to arrive in chronological order

        Parameter:
            event: Event object, the new event
        """
        logging.debug("\tNEV\t%s\t%s" % (event.timestamp, event.label))
        label = event.label
        timestamp = event.timestamp

        if self.breakpoints and timestamp > self.breakpoints[0]:
            logging.info(" Requested break point: %s" % self.breakpoints[0])
            code.interact(local=locals())
            self.breakpoints.pop(0)

        self._nnew_fq = 0   # Reinit for each new event
        self._nnew_rare = 0
        self._nnew_per = 0
        self._nnew_np = 0
        self._nrmd_evt = 0
        self._newper = []
        self._newnp = []

        # 1. Add the new MO the event participates in the 1-episode
        if label not in self.ROOT.child:
            # First time the label is seen in the current window
            self.ROOT.child[label] = Node(
                    self.mme_params,
                    episode = (label, ), 
                    level = 1, 
                    period = self.period
                    )
            # Link new node to ROOT
            self.ROOT.child[label].parent[label] = self.ROOT

        node = self.ROOT.child[label]
        if node.TQ and timestamp == node.TQ[-1][0]:
            # Another event with the same label and same timestamp has already been 
            # recorded -> nothing to do
            return  # EXIT FUNCTION HERE

        self.n_events += 1

        node.TQ.append((timestamp, timestamp))
        node.support += 1 # 1-episode: every occurrence is a minimal occurrence
        logging.debug("\tNOC\t%d" % node.id)
        
        if node.support == self.minimal_support:
            # the 1-episode has just become frequent
            relative_times = node.compute_relative_times(self.period)
            node.mme.reset_component(relative_times)
            self._nnew_fq += 1
            self.n_frequent += 1
            self.update_node_accuracy(node)

        elif node.support > self.minimal_support:
            # it was already frequent
            node.mme.update_mixture([Utils.datetime2relativeposition(timestamp, self.period), ])
            # FIXME NCO / FCO
            self.update_node_accuracy(node)

        if node.support >= self.minimal_support:
            # The node was modified: add it to RMN
            self.RMN[0].add(node)

            # 2. Add the new MOs of the episodes the event participates in, as the extension of an RMN
            for length in xrange(self.RMNheight):
                # BFS traversal (level by level)
                for node in list(self.RMN[length]): #copy: the list may change during traversal
                    if not node.parent:
                        # Node not in the lattice anymore (became rare a some time ago, less than maximal episode duration ago)
                        self.RMN[length].remove(node)
                        continue
                    if label in node.episode:
                        # Event label is part of the node -> cannot be extended with it!
                        continue

                    # Can node be extended with label on timestamp?
                    if label in node.child:
                        # The extended ep is already frequent. New MO?
                        child = node.child[label]
                        if child.TQ[-1][0] == node.TQ[-1][0]:
                            # node.TQ[-1] was already used to build an occurrence starting in 
                            # node.TQ[-1][0]. Since there can be only one, nothing to do.
                            pass
                        else:
                            if not self.maximal_episode_duration or timestamp - node.TQ[-1][0] < self.maximal_episode_duration:
                                # Does this occurrence make the last occurrence null and void? This happens only when last.end == end
                                # |-----|  last_occ
                                #       || new_occ (tstart = tend)
                                if child.TQ[-1][1] == timestamp:
                                    assert child.TQ[-1][0] <= node.TQ[-1][0]
                                    child.TQ.pop()

                                # New MO !
                                child.TQ.append((node.TQ[-1][0], timestamp))
                                logging.debug("\tNOC\t%d" % child.id)
                                child.recompute_support()
                                child.mme.update_mixture([Utils.datetime2relativeposition(node.TQ[-1][0], self.period), ])
                                # FIXME NCO / FCO
                                self.update_node_accuracy(child)
                                self.RMN[length+1].add(child)

                            else:
                                self.RMN[length].remove(node)

                    else:
                        # The extended episode wasn't frequent so far. 
                        # Has it just become frequent ?
                        if all(map(lambda e: label in node.parent[e].child, node.episode)):
                            TQ, support = self.merge_TQs(node.TQ, self.ROOT.child[label].TQ)
                            if support >= self.minimal_support:
                                #assert support == self.minimal_support
                                child = Node(self.mme_params, node.episode + (label, ), level=node.level+1, period=self.period)
                                child.TQ.extend(TQ)
                                child.support = support
                                child.link_node_to_other_parents(node, label) # Finish child_node's wiring 
                                map(lambda x: logging.debug("\tNOC\t%d" %child.id), xrange(support))
                                
                                self._nnew_fq += 1
                                self.n_frequent += 1
                                
                                child.mme.reset_component([Utils.datetime2relativeposition(i[0], self.period) for i in TQ])
                                # FIXME NCO / FCO
                                self.update_node_accuracy(child)
                            
                                if self.RMNheight < child.level:
                                    # Beginning of a new level in the lattice
                                    self.RMN.append(set())
                                    self.RMNheight += 1
                                self.RMN[length+1].add(child)
        

        # Remove outdated occurrences
        threshold = timestamp - self.window_length # occurrences older than threshold are outdated
        to_be_investigated = [self.ROOT, ]
        # BFS traversal. Outdated occurrences are removed. If the episode becomes rare, the branch is detached
        while to_be_investigated:
            node = to_be_investigated.pop(0)
            for label in node.child.keys():
                child = node.child[label]
                if child.level == 1:
                    already_rare = child.support < self.minimal_support
                ndrop = 0
                while child.TQ and child.TQ[0][0] < threshold: # the first occurrence is too old
                    if child.level == 1:
                        # One less event in the window
                        self.n_events -= 1
                        self._nrmd_evt += 1
                    logging.debug("\tDOC\t%d" % child.id)
                    child.TQ.pop(0)
                    ndrop += 1
                if ndrop:
                    # The node was modified: need support update
                    if not child.TQ:
                        # No occurrence left
                        if child.level == 1 and already_rare:
                            self._nnew_rare -= 1
                            # FIXME : c'est correct ce truc là?
                            if child.is_periodic(minimal_accuracy = self.minimal_accuracy):
                                self._nnew_np -= 1
                        self._nnew_rare, self._nnew_np = child.detach(minimal_accuracy = self.minimal_accuracy, count=self._nnew_rare, per_count=self._nnew_np)
                        
                    else:
                        child.recompute_support()
                        if child.support < self.minimal_support:
                            # Rare
                            if child.level > 1:
                                self._nnew_rare, self._nnew_np = child.detach(minimal_accuracy=minimal_accuracy, count=self._nnew_rare, per_count=self._nnew_np)
                            else:
                                # Don't remove len1 episodes (information loss), but it's not frequent anymore.
                                if already_rare:
                                    pass
                                else:
                                    self._nnew_rare += 1
                                    if child.is_periodic(minimal_accuracy = self.minimal_accuracy):
                                        logging.debug("\tDPE\t%d" % child.id)
                                        self._nnew_np += 1
                                        self._newnp.append(','.join(child.episode))
                                pass
                        else:
                            # Changed, but is still frequent
                            child.mme.update_mixture(ndrop=ndrop)
                            self.update_node_accuracy(child)
                            to_be_investigated.append(child)

        self.n_frequent -= self._nnew_rare
        self.n_periodic -= self._nnew_np

        self.logf.writerow([event.timestamp, self.n_events, self._nrmd_evt, 
            self._nnew_fq, self._nnew_rare, self.n_frequent, 
            self._nnew_per, self._nnew_np, self.n_periodic, 
            self.n_frequent - self.n_periodic, 
            time.clock()-self.start_time])
        
        for ep in self._newper:
            self.perepis.writerow([event.timestamp, 
                self.n_periodic - self._nnew_per + self._nnew_np, self.n_periodic, 
                ep, 'PER'])
        for ep in self._newnp:
            self.perepis.writerow([event.timestamp, 
                self.n_periodic - self._nnew_per + self._nnew_np, self.n_periodic, 
                ep, 'nonPER'])
        return


    def __len__(self):
        """
        Returns the number of frequent episodes 
        """
        seen = []
        to_be_explored = [self.ROOT, ]
        i = 0
        while to_be_explored:
            node = to_be_explored.pop(0)
            seen.append(node)
            if node.recompute_support() >= self.minimal_support:
                i += 1
            else:
                assert node.level <= 1
            for child in node.child:
                if node.child[child] not in seen and node.child[child] not in to_be_explored:
                    to_be_explored.append(node.child[child])
        return i


    def merge_TQs(self, TQ1, TQ2):
        """
        Build the time queue and compute the support of the episode union of 
        the two episodes whose time queues are in input. 
        
        Parameters:
            TQ1, TQ2: list of 2-uples. The start and end times of the MOs of 
            the 2 subepisodes to merge.

        Returns:
            TQ: the union time queue
            support: the NOMOs count
        """
        last_end = datetime.datetime.min # For non overlapping logging
        i, j = 0, 0 # indexes on TQ1, TQ2
        L1, L2 = len(TQ1), len(TQ2)

        # Searched results
        TQ = []
        support = 0

        while (i < L1 and j < L2):
            if TQ1[i][1] >= TQ2[j][1]:
                # TQ1[i] finishes after TQ2[j]: an MO finishing in TQ1[i][1]?
                # Find the greatest j such that TQ2[j] finishes before TQ1[i]
                while (j+1 < L2 and TQ2[j+1][1] <= TQ1[i][1]):
                    j += 1
                if TQ1[i][0] <= TQ2[j][0]:
                    # TQ1[i] starts first
                    # Note: occurrence cannot be too long, otherwise item 
                    # i wouldn't be in TQ1 in the first place
                    # => We have a MO
                    TQ.append((TQ1[i][0], TQ1[i][1]))
                    if TQ1[i][0] > last_end:
                        # Count only if NOMO
                        support += 1
                        last_end = TQ1[i][1]
                    # increment the index of the suboccurrence starting first
                    if TQ1[i][0] == TQ2[j][0]:
                        j += 1
                    i += 1
                else:
                    # TQ2[j] starts first
                    if not self.maximal_episode_duration or TQ1[i][1] - TQ2[j][0] <= self.maximal_episode_duration:
                        # Max length: check!
                        # => We have an MO
                        TQ.append((TQ2[j][0], TQ1[i][1]))
                        if TQ2[j][0] > last_end:
                            # Count only of NOMO
                            support += 1
                            last_end = TQ1[i][1]
                    # increment the index of the suboccurrence starting first
                    j += 1

            else:
                # TQ2[j] finishes last: an MO finishing in TQ2[j][1]?
                # Find the greatest i such that TQ1[i] finishes before TQ2[j]
                while (i+1 < L1 and TQ1[i+1][1] <= TQ2[j][1]):
                    i += 1
                if TQ2[j][0] <= TQ1[i][0]:
                    # TQ2[j] starts first
                    # Note: cannot be too long, otherwise item j wouldn't be in TQ2 in the first place
                    TQ.append((TQ2[j][0], TQ2[j][1]))
                    if TQ2[j][0] > last_end:
                        # Count only of NOMO
                        support += 1
                        last_end = TQ2[j][1]
                    # increment the index of the suboccurrence starting first
                    if TQ1[i][0] == TQ2[j][0]:
                        i += 1
                    j += 1
                else:
                    #TQ1[i] starts first
                    if not self.maximal_episode_duration or TQ2[j][1] - TQ1[i][0] <= self.maximal_episode_duration:
                        # Max length: check!
                        # => MO
                        TQ.append((TQ1[i][0], TQ2[j][1]))
                        if TQ1[i][0] > last_end:
                            support += 1
                            last_end = TQ2[j][1]
                    # increment the index of the suboccurrence starting first
                    i += 1
        return TQ, support

    def periodic_episode_iterator(self):
        explore = [self.ROOT, ]
        seen = []
        while explore:
            current_node = explore.pop(0)
            if current_node in seen:
                continue
            seen.append(current_node)
            if current_node.is_periodic(minimal_accuracy = self.minimal_accuracy):
                yield current_node
            for child in current_node.child.itervalues():
                explore.append(child)
    
    def print_perepis(self):
        for pe in self.periodic_episode_iterator():
            print "%s & %d & %s & %s \\%% \\\\ \\hline" % (
                    ', '.join(pe.episode),
                    pe.support,
                    ', '.join(map(str, 
                        zip(
                            map(lambda x: str(datetime.timedelta(seconds=x)), 
                                pe.periodicity_quality.means), 
                            map(lambda x: str(datetime.timedelta(seconds=x)), 
                                pe.periodicity_quality.stddevs)
                            )
                        )),
                    ', '.join(map(str, [100 * acc for acc in pe.periodicity_quality.accuracy]))
                    )

            
        

    def check_apriori_property(self):
        logging.info(" Apriori, dynamic lattice")
        to_be_explored = [self.ROOT, ]
        seen = []
        while to_be_explored:
            n = to_be_explored.pop(0)
            seen.append(n)
            self.check_node_apriori_property(n)
            for child in n.child:
                to_be_explored.append(n.child[child])

    def check_node_apriori_property(self, node):
        # Node is more frequent than its children. 
        if node.level==0:
            return

        node.recompute_support()

        for c in node.child:
            node.child[c].recompute_support()
            if node.child[c].support > node.support:
                # MAYDAY MAYDAY!
                print "parent:", node.episode, node.support
                node.print_TQ()
                print
                print "child: ", node.child[c].episode, node.child[c].support
                node.child[c].print_TQ()
                print
                assert False

    def update_node_accuracy(self, node):
        """
        Update node peirodicity accuracy, and log the results
        """
        prev_acc = node.is_periodic(minimal_accuracy = self.minimal_accuracy)
        node.periodicity_quality.compute_accuracy()
        new_acc = node.is_periodic(minimal_accuracy = self.minimal_accuracy)
        if not prev_acc and new_acc:
            logging.debug("\tNPE\t%d" % node.id)
            self.n_periodic += 1
            self._nnew_per += 1
            self._newper.append(','.join(node.episode))
        elif prev_acc and not new_acc:
            logging.debug("\tDPE\t%d" % node.id)
            self._nnew_np += 1
            self._newnp.append(','.join(node.episode))

if __name__ == '__main__':
    import argparse
    import MySQLdb
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # DATA FILE
    parser.add_argument('-d', '--dbname', 
            metavar = 'DATASET',
            required = True, 
            help = 'Read DATASET from mysql server')
    parser.add_argument('-t', '--table_name', default="EVENT_VIEW", 
            help="Name of the table where the events are stored. Should have a 'timestamp' and a 'label' columns")

    parser.add_argument('-v', '--verbose', 
            action = 'store_true', 
            help = "Set logging to debub mode")

    parser.add_argument('--maximal_episode_duration', default = 1800, type=int, help="Maximal duration in seconds of the episode occurrences")
    parser.add_argument('--minimal_support', default = 5, type=int, help="Minimal count of NOMOs")
    parser.add_argument('--window_length', default=604800, type=int, help="Duration (in seconds) of the window")
    parser.add_argument('--maximal_depth', default=None, type=int, help="Maximal depth of the built lattice (NOT IMPLEMENTED YET!)") # TODO
    parser.add_argument('--minimal_accuracy', default=0.7, type=float, help="Minimal accuracy")


    parser.add_argument('--ncomp', type=int, default=1,
            help="the initial number of components in the model")
    parser.add_argument("--max_ncomp", type=int, default=None,
            help="Sets an upper bound on the number of components in the mixture " +
            "model. Once the limit is reached, no new component can be created. " +
            "When it is set to None, the number of components is not constrained"
            )
    parser.add_argument("--merge_comp", default=1.5,
            nargs = '?', const=1.5, type=float,
            help="Set to enable the merging of components (highly recommended). " +
            "Default = Flag without value: merge if abs(m1-m2)<1.5*(s1+s2). " +
            "Flag with value: change the 1.5 coeff for MERGE_COMP.")
    parser.add_argument("--rm_comp", action='store_true', default=True,
            help="allows (or not) the deletion of components (NOT USED, FIXME)")
    parser.add_argument("--add_comp", default=1.0, const=1.0, nargs='?', type=float,
            help="Set to enable the addition of new components. " +
            "Also specifies how often new components are added: a random number in " +
            "[0.0, 1.0( is generated. If it is lower than ADD_COMP, then the new " + 
            "component is added. Default = Flag without value: every " + 
            "new datapoint can generate a new component (ADD_COMP = 1.0). ")
    parser.add_argument("--smart_add_only", action='store_true', default=True,
            help="When a new datapoint arrives, create a component for it only if it " +
            "is not close to any of the already existing components. A data " +
            "point x is close to a component (mean m, stddev s) if |m - x| < s. ")

    parser.add_argument("--lenXmax", type=int, default=None,
            help="The maximal length of the data in memory. Forces to drop old data " +
            "entries when too many new data points arrive. When it is equal to " +
            "None, there is no bound. ")

    parser.add_argument("--reset_rate", type=int, default=None,
            help = """How often further investigation shoud be conducted (do as 
            many EM passes as necessary, instead of just one in the simple 
            update). When None, no bigger update is done""")

    parser.add_argument("--toggle_plot_on", action="store_true", default=False,
            help="Plot the results of the mining")

    parser.add_argument("--period", default = 86400, type=int,
            help = "The period to consider, in seconds. Default: one day.")
    #parser.add_argument("--periods", nargs='+', default = [86400, ], 
    #       help = "The periods to consider, in seconds. Default: one day, if you want several periods to be analyzed, just list them, separated by days")

    parser.add_argument("--log_directory", default="/home/julie/Documents/xED/exec_results")
    parser.add_argument("--break_points", default=None, 
            help= """File containing timestamps at which the FEL should be 
            investigated. One timestamp per line, datetime format: 
            YYYY-MM-DD HH:MM:SS. Uses code.interact. Try not to mess with the 
            data...""")

    ## READ THE PARAMETERS
    options = parser.parse_args()

    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
        logging.info(" DEBUG MODE ON")
    else:
        logging.basicConfig(level=logging.INFO)
        logging.info(" info MODE ON")

    logging.info("OPTIONS: \n%s" % 
            '\n'.join(['\t%s:\t%s' % (a[0], a[1]) 
                for a in vars(options).items()]))

    logpath = init_log.from_options(options, name='sxED_perCompAcc')

    database = MySQLdb.connect(host="localhost",
            user="julie", passwd="julie", db=options.dbname)
    cursor = database.cursor()
    cursor.execute("SELECT timestamp, label FROM %s ORDER BY timestamp" % options.table_name)

    breakpoints = []
    if options.break_points:
        with open(options.break_points, 'r') as f:
            for line in f:
                line = line.strip()
                breakpoints.append(Utils.parse_time(line))

    mme_params = {
            "ncomp": options.ncomp,
            "rm_comp": options.rm_comp,
            "add_comp": options.add_comp,
            "merge_comp": options.merge_comp,
            "max_ncomp": options.max_ncomp,
            "smart_add_only": options.smart_add_only,
            "lenXmax": options.lenXmax,
            "reset_rate": options.reset_rate,
            "toggle_plot_on": options.toggle_plot_on,
            }

    el = EpisodeLattice(
            minimal_support             = options.minimal_support, 
            maximal_episode_duration    = datetime.timedelta(seconds=options.maximal_episode_duration), 
            maximal_depth               = options.maximal_depth,
            minimal_accuracy            = options.minimal_accuracy,
            window_length               = datetime.timedelta(seconds=options.window_length),
            mme_params                  = mme_params,
            period                      = options.period,
            log_directory               = logpath,
            breakpoints                 = breakpoints )

    i = 0
    for timestamp, label in cursor:
    #for event in dataset.events:
        i += 1
        event = Event(timestamp, str(label))
        if (i%1000) == 0:
            print "%d events (%s)" % (i, timestamp)
        #el.new_event_not_stream(event)
        el.new_event(event, eid=i)


    #el.build_lattice()
    #logging.info(" Lattice built")
    #logging.info(" %d frequent episodes" % el.n_fq_episodes)

    logging.info(" %d events" % i)
    #logging.info(" Streaming verion: %s nodes" % len(el))


    #import code
    #code.interact(local=locals())
#   el.check_apriori_property()
    #el.compare_results()

