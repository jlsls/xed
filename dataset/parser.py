import logging

from dataset import Event, Database

def read_dataset(f):
    from time import strptime, mktime
    lines = f.readlines()
    _, datasetname = f.name.rsplit('/', 1)
    datasetname, _ = datasetname.rsplit('.', 1)
    dataset = Database(name=datasetname)
    for line in lines:
        line = line.rstrip()
        date, time, label = line.split(' ', 2)
        timestamp = mktime(strptime(
            '%s %s' % (date, time), '%Y-%m-%d %H:%M:%S'))
        dataset.new_event(Event(timestamp, label))
        
    dataset.sort_events()
    logging.info((" Database '%s' loaded. %d events, %d different labels" % 
        (datasetname, len(dataset.events), len(dataset.items))))
    return dataset

if __name__ == '__main__':

    import pickle
    from argparse import ArgumentParser
    
    logging.basicConfig(level=logging.INFO)

    parser = ArgumentParser()
    parser.add_argument('-f', '--filename', 
            metavar = 'DATASET',
            required = True, 
            help = 'Read DATASET dump file')
    parser.add_argument('-d', '--destination',
            default = 'dataset.dump')

    ## READ THE PARAMETERS
    options = parser.parse_args()

    print ("OPTIONS: \n%s" % 
            '\n'.join(['\t%s:\t%s' % (a[0], a[1]) 
                for a in vars(options).items()]))

    with open(options.filename, 'r') as f:
        dataset = read_dataset(f)

    with open(options.destination, 'w') as f:
        pickle.dump(dataset, f)


