#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
import logging
import itertools

from dataset import Episode
from episode_discovery.CandidateGenerator import CandidateGenerator, MaximalEpisodesBuilder

class CG_FPgrowth(CandidateGenerator):
	"""
	One class per recursive call of the CG_FPgrowth algorithm. Designed 
	to work with timestamped non-transactional data. The transactions
	are here only for emphasizing on with events can be part of a given
	episode

	Parameters:
	===========
		minimal_support: Minimum number of occurrences of an episode, 
			for it to be considered as frequent
		maximal_episode_length: The maximal number of items in an episode
		occurrence_definition: Choose the definition you want to use 
			for occurrence, between 1, 2 and 3.
			1 All the combinations of events matching the episode 
			  (events can participate in several occurrences). 
			  Slow, memory consuming, but exhaustive (though a bit 
			  useless, since anyway an event is only considered to be 
			  part of a single episode). In order to (try to) constrain 
			  the complexity, an episode is investigated only if each 
			  of its events is frequent.
			2 Occurrences for an episode don't have common events. The 
			  time intervals covered by the occurrences may overlap 
			  though. We always choose the earliest event combination 
			  when building occurrences. Fast. Good first approach. But 
			  not the definition used in FrequentEpisodeGraph. 
			3 The intervals covered by the occurrences of the episode 
			  don't overlap. Same definition as in FrequentEpisodeGraph.
		prefix (optional): the suffix for the current conditional call

	Attributes:
	===========
		transactions: the transactions used for the current call. Each 
			transaction is a 2-uple: 
			- first item is a transaction id. It refers to the original 
			  transaction, from with the current trans is derived.
			- 2nd item is a list of events (a sublist of the events in 
			  the corresponding original transaction)
		original_transactions: the actual observations. They're not 
			modified during mining, and are transfered from one call the 
			the next recursive call
		frequent_episodes: stores the known frequent episodes
	"""

	def __init__(self, dataset, 
			minimal_support				= 5, 
			maximal_episode_length		= None, 
			occurrence_definition		= 2, 
			#prefix 					=  [], 
			maximal_episode_duration	= 1800, 
			storing_prefix				= '', 
			#method						= 'fpgrowth'
			):

		CandidateGenerator.__init__(self, dataset,
			minimal_support				= minimal_support,
			maximal_episode_length		= maximal_episode_length,
			#occurrence_definition		= occurrence_definition,
			maximal_episode_duration	= maximal_episode_duration,
			storing_prefix				= storing_prefix,
			)

		self.occurrence_definition = occurrence_definition
		#self.prefix = prefix
		
		self.original_transactions = []
		self.frequent_episodes = []
		self.occurrences = {}
		
	def generate_candidates(self):
		t = time.clock()

		# 1. Compute the maximal episodes
		meb = MaximalEpisodesBuilder(
				maximal_episode_duration = self.maximal_episode_duration,
				maximal_episode_length = self.maximal_episode_length,
				)
		self.maximal_episodes = meb.build_maximal_episodes(self.dataset)


		fpg = FPgrowthMiner(minimal_support = self.minimal_support)
		fpg.build_fptree(self.maximal_episodes)
		fpg.mine_fptree()
		self.frequent_episodes = fpg.frequent_episodes
		for episode in self.frequent_episodes:
			self.occurrences[episode] = fpg.occurrences[episode]
		return self.frequent_episodes

class FPG_Counter:
	def __init__(self):
		self.rec_calls = 0
		self.nb_nodes = 0
		self.node_traversal_constr = 0
		self.node_traversal_mining = 0
		self.nb_comp = 0
		self.nb_fq_episodes = 0
		self.nb_nodes_here = []
	
	def store_logs(self, destfile):
		with open(destfile, 'a') as f:
			f.write("\t<fpgresults>\n")
			for c, v in self.__dict__.items():
				f.write("\t\t<result name='%s' value='%s'/>\n" % (c, v))
			f.write("\t</fpgresults>\n")



class FPgrowthMiner:
	def __init__(self, minimal_support=5, counter=FPG_Counter()):
		self.minimal_support = minimal_support

		self.frequent_episodes = []
		self.occurrences = {}
		self.counter = counter

	def build_initial_transactions(self, maximal_episodes):
		transactions = []
		for maxep in maximal_episodes:
			transactions.append(build_fpg_transaction(maxep))
		return transactions

	def build_fptree(self, maximal_episodes):
		self.counter.rec_calls += 1
		transactions = self.build_initial_transactions(maximal_episodes)

		self.tree = FPG_Tree(counter = self.counter)
		items = self.frequent_item_order(transactions)

		# Tree construction
		for transaction in transactions:
			self.tree.add_transaction(transaction, items)

		#self.tree.export_to_graphviz('tree')
		logging.info(' Finished building the tree. %d nodes' 
				% (self.tree.last_id+1))


	def frequent_item_order(self, transactions):
		""" Frequency table construction """
		ftable = {}
		for transaction in transactions:
			for event in transaction['events']:
				ftable.setdefault(event.label, set()).add(event.timestamp)
		items = [i for i in ftable.keys() 
				if len(ftable[i]) >= self.minimal_support]
		items.sort(cmp = lambda x, y: cmp(len(ftable[y]), len(ftable[x])))
		return items


	def mine_fptree(self):
		for item, nodes in self.tree.htable.items():
			cpb = [] # Conditional pattern base
			support = {}
			for node in nodes:
				self.counter.node_traversal_mining += 1
				support.update((k,[[vv] for vv in v]) 
						for k,v in node.support.items())
				condpattern = {}
				currentnode = node
				while currentnode.parent.name != 'FPG_ROOT':
					self.counter.node_traversal_mining += 1
					currentnode = currentnode.parent
					for k in node.support.keys(): #k is a transaction id
						condpattern.setdefault(k,{'id': k})
						condpattern[k].setdefault('events', []).\
								extend(currentnode.support[k])
				cpb.append(condpattern)

			fq = self.episode_is_frequent(support)
			if fq['is_frequent']:
				self.frequent_episodes.append(fq['episode'])
				self.counter.nb_fq_episodes += 1
				if len(self.frequent_episodes) % 200 == 0:
					print '%d episodes' % len(self.frequent_episodes)
				self.occurrences[fq['episode']] = fq['occurrences']
				self.build_conditional_fptree([item, ], support, cpb)

		# Filter the episodes, to remove episodes e1 such that, there exists
		# another episode e2 such that e1 c e2, and f(e2) == f(e1) (i.e. e2 
		# is more precise than e1 and e2 is just as frequent; it is thus more
		# interesting than e1, and e1 doesn't need to be investigated
		removed = []
		for e1 in self.frequent_episodes:
			for e2 in self.frequent_episodes:
				if (e1.contents < e2.contents and 
						len(self.occurrences[e1]) == len(self.occurrences[e2])):
					removed.append(e1)
					break # get out of inner loop, try another e1

		map(self.frequent_episodes.remove, removed)
		map(self.occurrences.pop, removed)
		logging.info(' Finished mining the tree. %s frequent episodes' 
				% len(self.frequent_episodes))

	def build_conditional_fptree(self, prefix, support, cpb):
		self.counter.rec_calls += 1
		for k,v in support.items():
			assert v
		transactions = []
		for cp in cpb:
			transactions.extend(cp.values())
		items = self.frequent_item_order(transactions)
		tree = FPG_Tree(counter = self.counter)
		# Tree construction
		for transaction in transactions:
			tree.add_transaction(transaction, items)
		
		for item, nodes in tree.htable.items():
			cpb = [] # Conditional pattern base
			nsupport = {}
			for node in nodes:
				self.counter.node_traversal_mining += 1
				nsupport.update(node.support)

			merged_support = self.merge_supports(support, nsupport)
			fq = self.episode_is_frequent(merged_support)
			if fq['is_frequent']:
				self.frequent_episodes.append(fq['episode'])
				self.counter.nb_fq_episodes += 1
				if len(self.frequent_episodes) % 200 == 0:
					print '%d episodes' % len(self.frequent_episodes)
				self.occurrences[fq['episode']] = fq['occurrences']
				for node in nodes:
					self.counter.node_traversal_mining += 1
					condpattern = {}
					currentnode = node
					while currentnode.parent.name != 'FPG_ROOT':
						currentnode = currentnode.parent
						for k in node.support.keys():
							condpattern.setdefault(k,{'id': k})
							condpattern[k].setdefault('events', []).\
									extend(currentnode.support[k])
					cpb.append(condpattern)
				nprefix = prefix + [item,]
				self.build_conditional_fptree(nprefix, merged_support, cpb)

	def merge_supports(self, s1, s2):
		# !! the keys in s2 MUST be present in s1
		assert s1
		support = {}
		for k, v2 in s2.items():
			assert k in s1
			occurrences_iter = itertools.product(s1[k], v2)
			support[k] = []
			for old, new in occurrences_iter:
				occurrence = insert_into(old, new)
				support[k].append(occurrence)

			support[k].sort()
		return support

	def episode_is_frequent(self, support):
		all_occs = [o for i in itertools.chain(support.values()) 
				for o in itertools.chain(i)]
		assert len(set(map(lambda x: len(x), all_occs))) == 1
		all_occs.sort()
		non_overlapping = remove_overlapping(all_occs)
		if len(non_overlapping) >= self.minimal_support:
			return {'is_frequent': True,
					'episode': Episode([e.label for e in all_occs[0]]),
					'occurrences': non_overlapping 
					}
		return {'is_frequent': False}




def insert_into(occurrence, new_event, counter = FPG_Counter()):
	"""
	Returns a new list of events (ie an occurrence), which is sorted
	"""
	o = occurrence[:]
	te = new_event.timestamp
	for i in xrange(len(occurrence)):
		counter.nb_comp += 1
		to = occurrence[i].timestamp
		if to > te:
			o.insert(i, new_event)
			return o
	o.append(new_event)
	return o


def remove_overlapping(occurrences):
	non_overlapping = []
	last = 0
	for o in occurrences:
		if o[0].timestamp > last:
			non_overlapping.append(o)
			last = o[-1].timestamp
	return non_overlapping


def cmp_tuple(x, y, counter = FPG_Counter()):
	#assert len(x) == len(y)
	for x1, y1 in itertools.izip(x, y):
		counter.nb_comp += 1
		tx = x1.timestamp
		ty = y1.timestamp
		if tx < ty:
			return -1
		elif tx > ty:
			return 1
	return 0


def build_fpg_transaction(maximal_episode):
	return {'id': min(maximal_episode).timestamp,
			'events': maximal_episode}


class FPG_Node:
	def __init__(self, name = 'FPG_ROOT', parent = None, id=0):
		self.name = name
		self.parent = parent
		self.children = []
		self.id = id
		
		#The occurrences of self.name events that reached this node
		self.support = {} 
	
	def add_support(self, transaction):
		self.support[transaction['id']] = \
				[e for e in transaction['events'] if e.label == self.name]

	def get_child(self, childname):
		for child in self.children:
			if child.name == childname:
				return child
		return None

	def new_child(self, childname, id=0):
		#!! Make sure the current node doesn't already have this child
		child = FPG_Node(name=childname, parent=self, id=id)
		self.children.append(child)
		return child

class FPG_Tree:
	def __init__(self, prefix = [], counter = FPG_Counter()):
		self.root = FPG_Node()
		self.counter = counter
		self.counter.nb_nodes += 1
		self.prefix = prefix
		self.htable = {}
		self.last_id = 0
		self.counter.nb_nodes_here.append(1)

		
	def add_transaction(self, transaction, items):
		current_node = self.root
		self.counter.node_traversal_constr += 1
		titems = [e.label for e in transaction['events']]
		seenitems = []
		for item in [i for i in items if i in titems]:
			child = current_node.get_child(item)
			if not child:
				self.counter.nb_nodes += 1
				self.counter.nb_nodes_here[-1] += 1
				self.last_id += 1
				child = current_node.new_child(item, id = self.last_id)
				self.htable.setdefault(item, []).append(child)
			self.counter.node_traversal_constr += 1
			child.add_support(transaction)
			current_node = child
		return

	def export_to_graphviz(self, filename='fptree.dot'):
		edges = []
		to_be_explored = [self.root, ]
		while to_be_explored:
			n = to_be_explored.pop(0)
			for c in node.children:
				to_be_explored.append(c)
				edges.append((
					'%s_%d'% (
						n.name.replace(' ', '').replace('-', '_'), n.id), 
					'%s_%d' % (
						c.name.replace(' ', '').replace('-', '_'), c.id)))


		fileDot = open  (filename+".dot",'w')
		
		## beginning of the dot file
		fileDot.write("graph G {\n")
						
		for id1, id2 in edges:
			## we add the edge id1 -- id 2
			edgeString=id1+" -- "+id2
			edgeString += " [dir=forward];\n"
			fileDot.write(edgeString)
		## end of the dot file 
		fileDot.write("}\n")			
		fileDot.close()


		import os
		os.system ("neato "+filename+".dot -Tpng -o "+filename+".png")
		logging.debug(' Exported the tree to neato')
if __name__ == '__main__':
	from argparse import ArgumentParser
	parser = ArgumentParser()

	# DATA FILE
	parser.add_argument('-f', '--filename', 
			metavar = 'DATASET',
			required = True, 
			help = 'Read DATASET dump file')

	general_tools.frequent_episode_search.argument_parser_for_frequent_episodes(parser)

	#FIXME pas du tout utilisé
	parser.add_argument('--occurrence_definition',  
			type = int,  
			default = 2,
			help = ("Choose the definition you want to use for occurrence "
				"-1- All the combinations of events matching the episode " 
				"(events can participate in several occurrences). " 
				"Slow, memory consuming, but exhaustive (though a bit useless, "
				"since anyway an event is only considered to be part of a "
				"single episode). In order to (try to) constrain the " 
				"complexity, an episode is investigated only if each of its "
				"events is frequent. "

				"-2- Occurrences for an episode don't have common events. "  
				"The time intervals covered by the occurrences may overlap "
				"though. We always choose the earliest event combination " 
				"when building occurrences. Fast. Good first approach. " 
				"But not the definition used in FrequentEpisodeGraph. " 

				"-3- The intervals covered by the occurrences of the episode "
				"don't overlap. Same definition as in FrequentEpisodeGraph. "),
			choices = [1, 2, 3])

	## READ THE PARAMETERS
	options = parser.parse_args()

	print ("OPTIONS: \n%s" % 
			'\n'.join(['\t%s:\t%s' % (a[0], a[1]) 
				for a in vars(options).items()]))

	from general_tools.dataset.parser import read_dataset
	with open(options.filename, 'r') as f:
		dataset = read_dataset(f)

	## Mine the stream...
	fpg = CG_FPgrowth(dataset, minimal_support = options.minimal_support, 
			maximal_episode_length = options.maximal_episode_length, 
			occurrence_definition = options.occurrence_definition,
			maximal_episode_duration = options.maximal_episode_duration)

	fpg.generate_candidates()
	if len(fpg.frequent_episodes) < 100:
		for fe in fpg.frequent_episodes:
			print fe
	logging.info(" Finished candidate generation. %d episodes" % len(fpg.frequent_episodes))

	import pickle
	with open("episodes_fp.dump", "w") as f:
		pickle.dump(fpg, f)
