import datetime
from Utils import datetime2relativeposition, datetime2roundeddatetime

"""
Used in the streaming version
"""

class PeriodicityQuality:
    def __init__(self, mme, tq, period):
        self.mme = mme
        self.tq = tq
        self.period = period

        # Updated thanks to get_comps() (also called in compute_accuracy())
        self.means = []
        self.stddevs = []
        
        # Updated thanks to compute_accuracy()
        self.accuracy = 0.0

    def __str__(self):
        return "%s -> %s (period:%s): %d occs, %d comp" % (self.tq[0][0], self.tq[-1][1], self.period, len(self.tq), len(self.means))

    def get_comps(self):
        m_i = self.mme.means.items()
        m_i.sort(cmp=lambda x, y: cmp(x[1], y[1]))
        self.means = [i[1] for i in m_i]
        self.stddevs = []
        for i,_ in m_i:
            self.stddevs.append(self.mme.stddevs[i])

    def compute_expected_occurrence_times(self):
        assert self.tq
        start     = self.tq[0][0]
        end     = self.tq[-1][0]

        # //: floor division
        t_pstart = datetime2roundeddatetime(start, self.period)

        expected = []
        while t_pstart < end:
            for mean in self.means:
                expected.append(t_pstart + datetime.timedelta(seconds=mean))
                pass
            t_pstart += self.period
        return expected

    def compute_accuracy(self):
        self.get_comps()
        t_exp = self.compute_expected_occurrence_times()
        if not t_exp:
            print self
        assert t_exp

        i, j = 0, 0
        ci = 0
        L_exp = len(t_exp)
        L_tq = len(self.tq)
        as_exp = 0.0

        while i < L_exp and j < L_tq:
            while i < L_exp and j < L_tq and self.tq[j][0] <= t_exp[i] + datetime.timedelta(seconds=self.stddevs[ci]):
                if abs(t_exp[i]-self.tq[j][0]) <= 2*datetime.timedelta(seconds=self.stddevs[ci]):
                    # Cool!
                    as_exp += 1.0
                    i += 1
                    ci = i % self.mme.ncomp
                j += 1
            
            i += 1

        self.accuracy = as_exp/L_exp
        self.as_exp = as_exp
        self.n_exp = L_exp

        return self.accuracy

class PeriodicityQualitySpecAcc(PeriodicityQuality):
    def __init__(self, mme, tq, period):
        PeriodicityQuality.__init__(self, mme, tq, period)
        self.accuracy = []
    def compute_accuracy(self):
        self.get_comps()
        ncomp = len(self.means)
        t_exp =  self.compute_expected_occurrence_times()
        if not t_exp:
            print self
        assert t_exp

        i, j = 0, 0
        ci = 0
        L_exp = len(t_exp)
        L_tq = len(self.tq)
        as_exp = [0.0, ] * ncomp
        # i iterates over t_exp,  j over self.tq

        while i < L_exp and j < L_tq:
            while i < L_exp and j < L_tq and self.tq[j][0] <= t_exp[i] + datetime.timedelta(seconds=self.stddevs[ci]):
                if abs(t_exp[i]-self.tq[j][0]) <= 2*datetime.timedelta(seconds=self.stddevs[ci]):
                    # Cool!
                    as_exp[ci] += 1.0
                    i += 1
                    ci = i % self.mme.ncomp
                j += 1
            
            i += 1

        self.accuracy = [e*ncomp/L_exp for e in as_exp]
        self.as_exp = as_exp
        self.n_exp = L_exp

        return self.accuracy

