import time
import calendar
import datetime
import logging

class Event:
	def __init__(self, timestamp, label, characterizes_missing=False):
		self.timestamp = timestamp
		self.label = label
		self.characterizes_missing = characterizes_missing
		pass

	def __cmp__(self, event):
		return cmp(self.timestamp, event.timestamp)
	def __str__(self):
		return '%s %s' % (self.timestamp, self.label)
	def __repr__(self):
		return str(self)
	def __hash__(self):
		return int(self.timestamp)
	def __eq__(self, event):
		return self.timestamp == event.timestamp and self.label == event.label

class Database:
	def __init__(self, name=''):
		self.name = name
		self.date = time.localtime()
		
		self.events = []
		self.episodes = []
		self.periodic_episodes = []
		self.labels = set()

		#FIXME don't remember how it's used in ED_Miner
		self.rare_events = []

	def new_event(self, event):
		self.events.append(event)
		if event.label not in self.labels:
			self.labels.add(event.label)

	def sort_events(self):
		self.events.sort()

	def new_episode(self, episode):
		self.episodes.append(episode)

	def new_periodic_episode(self, episode):
		self.periodic_episodes.append(episode)

def build_dataset_from_db(cursor, name='name'):
	data = Database(name)
	nevents = 0
	for ts, label in cursor:
		nevents += 1
		data.new_event(Event((ts - datetime.datetime(1970,1,1)).total_seconds(), label))
	
	logging.info(" Database %s built. %d events, %d labels" % (name, nevents, len(data.labels)))
	return data

class Episode:
	def __init__(self, contents):
		assert contents
		self.contents = frozenset(contents)
		self.id = hash(self.contents)

	def __hash__(self):
		return self.id
	def __eq__(self, episode):
		return self.contents == episode.contents

	def __str__(self):
		return '%d: %s' % (self.id, ', '.join(self.contents))

	def __len__(self):
		return len(self.contents)

	def __iter__(self):
		for c in self.contents:
			yield c

	def intersection(self, episode):
		intersec = self.contents.intersection(episode.contents)
		if intersec:
			return Episode(intersec)
