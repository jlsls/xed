_params = [
        {'id': 1,
            'from': {
                'date': 0,
                'mean': 5.,
                'stddev': .5,
                'weight': 1.
                },
            'to': {
                'date': 50,
                'mean': 10.,
                'stddev': .5,
                'weight': 1.
                },
        },
        {
            'id': 2,
            'from': {
                'date': 15,
                'mean': 15.,
                'stddev': 5.,
                'weight': 0.,
                },
            'to': {
                'date': 50,
                'mean': 15.,
                'stddev': .01,
                'weight': 1.
                } 
        }
        ]

print "Pickling distribution characteristics..."
import pickle
with open("mmg_example.dump", "w") as f:
    pickle.dump(_params, f)
