class DVSM:
	def init(self, alpha, C, Cv, dataset):
		self.alpha = alpha
		self.C = C
		self.Cv = Cv
		self.dataset = dataset
		
	def mine(self):
		self.patterns = {}
		# 1. Create reduced dataset Dr containing top-alpha most frequent events
		event_frequency = {}
		for event in self.dataset:
			if event.label not in event_frequency:
				event_frequency[event.label] = 1
			else:
				event_frequency[event.label] += 1

		event_frequency = sorted(event_frequency.items(), key=lambda x: -x[1])
		if self.alpha < len(event_frequency):
			del event_frequency[alpha:]
		frequent_enough_event_labels = [ef[0] for ef in event_frequency]

		Dr = [e for e in dataset.events if e.label in frequent_enough_event_labels]

		# 2. Slide a size-2 window on Dr (find the 2-patterns)
		# TODO

		# 3. Extend the patterns with their prefix and suffix events
		# At the end of each iteration, prune the infrequent variations and the infrequent general patterns
		# TODO


	def distance(a, b):
		# Levenstein (edit) distance
		n, m = len(a), len(b)
		if n > m:
			# Make sure n <= m, to use O(min(n,m)) space
			a,b = b,a
			n,m = m,n
			
		current = range(n+1)
		for i in range(1,m+1):
			previous, current = current, [i]+[0]*n
			for j in range(1,n+1):
				add, delete = previous[j]+1, current[j-1]+1
				change = previous[j-1]
				if a[j-1] != b[i-1]:
					change = change + 1
				current[j] = min(add, delete, change)
				
		return current[n]

	def similarity(self, a, b):
		return 1 - self.distance(a, b) / max(len(a), len(b))


	def description_length(self, a):
		# TODO
		pass

	def is_interesting_gp(self, a, Dr):
		# FIXME: Dr|a ???
		return self.description_length(Dr) / (self.description_length(a) + self.description_length(Dr|a)*(1 - a.continuity()) > self.C

	def is_interesting_variation(self, ai, a, Dr):
		# FIXME: Dr|ai, Dr|a ???
		return self.description_length(Dr|ai)*ai.continuity() / (self.description_length(Dr|a)*a.continuity()) > self.Cv


	


	pass

class Pattern:
	def __init__(self):
		self.variations = []
		self.prevalent_variation = None
		self.events = []
		self.continuity

	def description_length(self):
		# TODO ???

		pass

	def continuity(self):
		# des événements d'une instance, des instances, des variations, du pattern
		# TODO
		pass


	pass

class Clussifier:
	pass


