#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging
import os

import dataset
from dataset import build_dataset_from_db
import init_log
import Param
import xED_Miner

def complete_mining(parameters, data):
	# MINE..
	miner = xED_Miner.Miner(data, parameters, factorizations=[], round_count=0) 
	new_miner = miner.mine()
	
	from pickle import dump
	with open(os.path.join(parameters['storing_prefix'], "miner.dump"), "w") as f:
		dump(miner, f, 0)

	if parameters['interactive']:
		raw_input("Press ENTER to finish. Will close all the plots")
	
	new_miner.log_to_csv()

	return new_miner


if __name__ == "__main__":
	import MySQLdb
	import argparse 
	from DatasetParser import read_dataset

	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	parameter_definitions = Param.build_xED_parameters()
	parser.add_argument('-d', '--dbname', 
			metavar = 'DATASET',
			required = True, 
			help = 'Read DATASET from mysql server')
	parser.add_argument('-t', '--table_name', default="EVENT", 
			help="Name of the table where the events are stored. Should have a 'timestamp' and a 'label' columns")

	#parser.add_argument('-f', '--file', dest = 'dbname', 
	#		metavar = 'DATABASE', help = 'load DATABASE', required=True)

	#
	# Construction de l'interface à partir des options par defaut. Normalement toujours à jour ;)
	#
	for k, v in parameter_definitions.items():
		v.add_command_line_option(parser)

	##### END OF OPTIONS DEFINITIONS

	options = parser.parse_args()
        logging.basicConfig(level=options.logging_level)

	logging.info("\n\t".join([" Parameters used", ] + 
		["%s: %s" % (k, str(v)) for k, v in options.__dict__.items()]))

	# MINING PARAMETERS
	#parameters = parse_config_file(options.parameters) # FIXME

	parameter_values = {}
	for k in parameter_definitions:
		if k in options.__dict__:
			parameter_values[k] = options.__dict__[k]

	# LOGGING INIT
	log_path = init_log.from_options(options, name="xED")
	parameter_values['storing_prefix'] = log_path
	

	# LOAD DATABASE
	database = MySQLdb.connect(host="localhost",
			user="julie", passwd="julie", db=options.dbname)
	cursor = database.cursor()
	cursor.execute("SELECT timestamp, label FROM %s ORDER BY timestamp" % options.table_name)
	data = build_dataset_from_db(cursor, name=options.dbname)
   
	complete_mining(parameter_values, data)

