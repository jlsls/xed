# -*- coding: utf-8 -*-


class PeriodicityFinder_autocorrelation:
    def __init__(self, episode, occurrences, **params):
        self.episode = episode
        self.occurrences = occurrences
        self.__dict__.update(params)
        #self.params = params
        name = "candidate%s.txt" % str(self.episode.label)
        self.logfile = "/".join([self.storing_prefix,
                'candidates', name])
        with open(self.logfile, 'a') as f:
            f.write("="*50)
            f.write("\nIteration %d\n\n" % cfg.COUNTER_iteration)
            f.write("CANDIDATE %s:\n" % str(episode))
            f.write("%d occurrences\n\n" % (len(occurrences)))
            #f.write("period\tnbcomp\tPattern quality\n" + \
            #        "------\t------\t---------------\n\n")

        pass

    def what_periodicity(self):

        lowest_error = 999
        best_length = 0
        gaps = self.get_time_gaps()
        if gaps.count(gaps[0]) > .75*len(gaps):
            # toutes les valeurs sont égales
            best_length = 1
            lowest_error = 0
        
        else:
            for length in range(1, len(gaps)/5):
                
                s1 = gaps[length:]
                s2 = gaps[:-length]
                assert len(s1) == len(s2)
                a, b, _, _, err = stats.linregress(s1, s2)
                if a>0 and err < lowest_error:
                    lowest_error = err
                    best_length = length
                    print "New best! %d" % length

        if best_length == 0:
            print "BEST LENGTH == 0"
            with open(self.logfile, 'a') as f:
                f.write('Nothing interesting\n')
            return [EMPTY_PAT_QUAL]

        else:
            candidates = [gaps[i:i+best_length] for i in range(len(gaps)-best_length)]
            counts = [candidates.count(i) for i in candidates]
            
            description = candidates[counts.index(max(counts))]
            with open(self.logfile, 'a') as f:
                f.write('Periodicity description: %s\n' % description)

            period = sum(description)
            with open(self.logfile, 'a') as f:
                f.write('Period: %d\n' % period)

            ae, mi, ex = self.fit(description)
            if len(mi) > self.min_accuracy * len(ae):
                with open(self.logfile, 'a') as f:
                    f.write('No good fit: not accurate enough. Return EMPTY\n')
                return [EMPTY_PAT_QUAL]
            start = ae[0]
            end = ae[-1]

            p = PatternQuality(Periodicity_ED(period, description, start, end), ae, [], mi, ex)

            with open(self.logfile, 'a') as f:
                f.write('Pattern quality: %s\n' % p)

            return [p]

    def fit(self, description):
        best_try = 0
        max_fit = 0
        ae = []
        ex = []
        mi = []

        start_times = self.get_start_times()
        start_times.sort()
        for start in start_times:
            index = -1
            as_expected = []
            missing = []

            last = start -description[0]

            while last < start_times[-1]:
                index = (index +1)%len(description)
                last += description[index]
                if last in start_times:
                    as_expected.append(last)
                else:
                    missing.append(last)

            if len(as_expected) > max_fit:
                max_fit = len(as_expected)
                best_try = start
                ae = as_expected
                mi = missing
                ex = [i for i in start_times if i not in ae]

        mi = [i for i in mi if (i > ae[0] and i < ae[-1])]
        return ae, mi, ex




    def get_start_times(self):
        return [o.beginning().timestamp for o in self.occurrences]

    def get_time_gaps(self):
        start_times = self.get_start_times()
        start_times.sort()
        return [start_times[i]-start_times[i-1] \
                for i in range(1, len(start_times))]

    def autocorrelation(self):
        gaps = self.get_time_gaps()
        raw_input(gaps)

        pl.figure()
        lags, c, linecol, b = pl.acorr(gaps, maxlags=None)

        zero = [i for i in lags].index(0)
        lags = lags[zero+1:]
        c = c[zero:]
        lf_positive = c[0] > 0
        return gaps, lags, c[1:], lf_positive

