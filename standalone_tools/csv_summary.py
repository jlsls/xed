import os
import csv
import fileinput
import numpy


interesting_filenames = ['ED_factorizations.csv', 'ED_candidates.csv', 
		'factorization_stats.csv', 'cand_stats.csv'
		]


def subfolders_generator(root):
	"""
	Generator, that iterates over the folders created for each ED/xED iteration
	"""
	exists = True
	i = 1
	yield root
	while exists:
		path = os.path.join(root, 'iteration%02d' % i)
		exists = os.access(path, os.F_OK)
		if exists:
			yield path
		i += 1

def updatefile(filename):
	print 'processing', filename
	with open(filename, 'rb') as f:
		reader = csv.reader(f)
		data = []
		first_line = True
		for line in reader:
			if first_line:
				first_line = False
				headers = line
				continue
			line[1:] = map(float, line[1:])
			data.append(line[1:])

	# Compute the necessary data
	mins = ['min',]
	maxs = ['max',]
	means = ['mean',]
	medians = ['median',]
	stds = ['std',]
	mins.extend(numpy.amin(data, axis=0))
	maxs.extend(numpy.amax(data, axis=0))
	means.extend(numpy.mean(data, axis=0))
	medians.extend(numpy.median(data, axis=0))
	stds.extend(numpy.std(data, axis=0))

	# Write file
	f = fileinput.input(filename, inplace=1, backup='.bak')
	first_line = True
	print f.readline().strip()
	print
	print ",".join(map(str, mins))
	print ",".join(map(str, maxs))
	print ",".join(map(str, means))
	print ",".join(map(str, medians))
	print ",".join(map(str, stds))
	print
	
	for line in f:
		print line.strip()

	f.close()

def process_directory(directory):
	for filename in interesting_filenames:
		f = os.path.join(directory, filename)
		if os.access(f, os.F_OK):
			updatefile(f)

def main(root):
	dirs = subfolders_generator(root)
	for directory in dirs:
		process_directory(directory)



		


if __name__ == "__main__":
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument('-d', '--directory',
			help = "The result directory that needs summarizing",
			required = True,
			)

	options = parser.parse_args()
	main(options.directory)


