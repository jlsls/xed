class Btree:
    """
    Class for binary tree.
    The nodes are expected to have all the methods from class _BNode. If you 
    need nodes richer, make sure to use something compatible (heritance from 
    _BNode is strongly advised).
    """
    def __init__(self):
        self.root = _BNode()

    def __str__(self):
        result = ""
        to_be_printed = [("root", self.root, 0), ]
        while to_be_printed:
            node_type, current_node, level = to_be_printed.pop(0)
            result += "%s%s: %s\n" % (" "*level, node_type, current_node)
            if current_node.right:
                to_be_printed.insert(0, ("R", current_node.right, level+1))
            if current_node.left:
                to_be_printed.insert(0, ("L", current_node.left, level+1))
        return result

    def bfs(self):
        return self.root.bfs()

    def search(self, key):
        self.root.search(key)

    def insert(self, node):
        if self.root.key == None:
            self.root = node
            return True
        
        current_node = self.root
        while True:
            if current_node.key == node.key:
                # There is already a node with the same key
                return False
                
            if current_node.key > node.key:
                if current_node.left:
                    current_node = current_node.left
                else:
                    # Insert here
                    current_node.left = node
                    node.parent = current_node
                    return True
            else:
                if current_node.right:
                    current_node = current_node.right
                else:
                    # Insert here
                    current_node.right = node
                    node.parent = current_node
                    return True

    def find_predecessor(self, node):
        current_node = self.root
        predecessor = None
        while current_node:
            if current_node < node:
                if not predecessor or current_node > predecessor:
                    predecessor = current_node
                current_node = current_node.right
            else:
                current_node = current_node.left
        return predecessor


    def _remove_no_child(self, node):
        assert not node.left and not node.right
        parent = node.parent
        if parent: # If not: removing the last node in the tree: the tree
            if parent.left == node:
                parent.left = None
            else:
                assert parent.right == node
                parent.right = None
        else:
            self.root = None

    def _remove_one_right_child(self, node):
        assert not node.left
        parent = node.parent
        if parent:
            if parent.left == node:
                parent.left = node.right
            else:
                assert parent.right == node
                parent.right = node.right
            node.right.parent = parent
        else:
            # Removing the root
            self.root = node.right
            node.right.parent = None

    def _remove_one_left_child(self, node):
        assert not node.right
        parent = node.parent
        if parent:
            if parent.left == node:
                parent.left = node.left
            else:
                assert parent.right == node
                parent.right = node.left
            node.left.parent = parent
        else:
            # Removing the root
            self.root = node.left
            node.left.parent = None


    def remove(self, node):
        if not node.left and not node.right:
            # No children
            self._remove_no_child(node)

        elif not node.left:
            # Only one child: the right one
            self._remove_one_right_child(node)

        elif not node.right:
            # Only one child: the left one
            self._remove_one_left_child(node)

        else:
            # Two children: detach the predecessor, and replace the node to delete with it 
            pred = self.find_predecessor(node)
            self.remove(pred)

            # Links with the parent
            if node.parent:
                if node.parent.left and node.parent.left == node:
                    node.parent.left = pred
                else:
                    node.parent.right = pred
                pred.parent = node.parent
            else:
                # Removing the root
                self.root = pred
                pred.parent = None

            # Links with the children
            pred.left = node.left
            pred.right = node.right


class _BNode:
    def __init__(self, key=None):
        self.key = key
        self.parent = None
        self.left = None
        self.right = None
            
    def __cmp__(self, node):
        return cmp(self.key, node.key)

    def __str__(self):
        return str(self.key)

    def bfs(self):
        to_be_seen = [self, ]
        while to_be_seen:
            current_node = to_be_seen.pop(0)
            if current_node.left:
                to_be_seen.append(current_node.left)
            if current_node.right:
                to_be_seen.append(current_node.right)
            yield current_node

    def search(self, key):
        if key == self.key:
            return self
        if key < self.key:
            next_node = self.left
        else:
            next_node = self.right
        if not next_node:
            return None
        else:
            next_node.search(key)


if __name__ == "__main__":
    import logging
    logging.basicConfig(level=logging.INFO)

    logging.info(" Tree construction")
    tree = Btree()
    n13 = _BNode(13)
    tree.insert(n13)
    tree.insert(_BNode(5))
    tree.insert(_BNode(3))
    tree.insert(_BNode(4))
    tree.insert(_BNode(12))
    n8 = _BNode(8)
    tree.insert(n8)
    tree.insert(_BNode(7))
    n10 = _BNode(10)
    tree.insert(n10)
    tree.insert(_BNode(16))
    tree.insert(_BNode(15))
    tree.insert(_BNode(17))
    tree.insert(_BNode(14))

    print tree
    
    logging.info(" Removal of 10")
    tree.remove(n10)
    print tree

    logging.info(" Removal of 8")
    tree.remove(n8)
    print tree

    logging.info(" Removal of 13")
    tree.remove(n13)
    print tree

    assert not tree.root.parent

    logging.info(" BFS traversal")
    for node in tree.bfs():
        print node.key
