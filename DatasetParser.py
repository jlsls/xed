from dataset import Event, Database
import logging

def read_dataset(f):
	from time import strptime, mktime
	lines = f.readlines()
	_, datasetname = f.name.rsplit('/', 1)
	datasetname, _ = datasetname.rsplit('.', 1)
	dataset = Database(name=datasetname)

	for line in lines:
		line = line.rstrip()
		date, time, label = line.split(' ', 2)

		if label not in dataset.labels:
			dataset.labels.add(label)

		timestamp = mktime(strptime(
			'%s %s' % (date, time), '%Y-%m-%d %H:%M:%S'))
		dataset.new_event(Event(timestamp, label))
		
	dataset.sort_events()
	logging.info((" Database '%s' loaded. %d events, %d different labels" % 
		(datasetname, len(dataset.events), len(dataset.labels))))
	return dataset

if __name__ == '__main__':
	import pickle
	from argparse import ArgumentParser
	
	logging.basicConfig(level=logging.INFO)

	parser = ArgumentParser()
	parser.add_argument('-f', '--filename', 
			required = True, 
			help = 'Datafile to read. Expected: one event per line, event format: "YYYY-MM-DD HH:MM:SS SOME EVENT LABEL".')
	parser.add_argument('-s', '--store',
			default = None, nargs="?",
			const = "/tmp/dataset.dump",
			help = "Store the dataset (python utils.Dataset object) in a pickle dump file. Default location: /tmp/dataset.dump. If -s/--store not used, the pickle dump is not saved.")
	parser.add_argument('-v', '--verbose',
			action = "store_true", 
			help = "Set logging to DEBUG mode instead of INFO"
			)

	## READ THE PARAMETERS
	options = parser.parse_args()

	if options.verbose:
		logging.basicConfig(level=logging.DEBUG)
	else:
		logging.basicConfig(level=logging.INFO)
	logging.info("\n\t".join([" Parameters used", ] + 
		["%s: %s" % (k, str(v)) for k, v in options.__dict__.items()]))

	with open(options.filename, 'r') as f:
		dataset = read_dataset(f)

	if options.store:
		with open(options.store, 'w') as f:
			pickle.dump(dataset, f)
			logging.info(" Pickle dump writen to %s" % options.store)


