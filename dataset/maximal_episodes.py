import logging 

from dataset import Event, Database

class MaximalEpisodesBuilder:
    """
    Calling these "maximal episode" is poorly chosen, in the sense that they 
    are not episodes, but merely lists of events. However, the episodes 
    present in the dataset can all be extracted from the maximal episodes. 
    The name "maximal episode" comes from the original design of Episode 
    Discovery [1] (and we chose not to change it).

    [1] Edwin O. Heierman. Using information-theoretic principles to discover 
    interesting episodes in a time-ordered input sequence. Phd thesis. 
    University of Texas at Arlington, 2004.
    """

    def __init__(self, episode_length=1200, episode_capacity=None, memory_length=None):
        self.episode_length = episode_length
        self.episode_capacity = episode_capacity
        self.memory_length = memory_length # FIXME completely useless for now

        # Time-wrapping window
        self.window_contents = []

    def build_maximal_episodes(self, dataset):
        """
        Follows quite precisely the algorithm described in [1]
        """
        maximal_episodes = []
        for event in dataset.events:
            me = self.new_event(event)
            if me:
                maximal_episodes.append(me)
        if self.window_contents:
            maximal_episodes.append(list(self.window_contents))
        logging.info(" Built the maximal episodes. %d max episodes." % len(maximal_episodes))
        return maximal_episodes

    def new_event(self, event):
        created = None
        if self.window_contents:
            max_allowed_start = event.timestamp - self.episode_length
            if (self.window_contents[0].timestamp < max_allowed_start or 
                    (self.episode_capacity and 
                        len(self.window_contents) == self.episode_capacity)):
                created = list(self.window_contents)
                self.window_contents.pop(0)
            while (self.window_contents and 
                    self.window_contents[0].timestamp < max_allowed_start):
                self.window_contents.pop(0)

        self.window_contents.append(event)
        return created


def argument_parser_for_maximal_episodes(parser):
    ## PARAMETERS FOR MAXIMAL EPISODE CREATION
    # episode length
    parser.add_argument('--episode_length', 
            type = int, 
            help = "Maximal episode duration in seconds", 
            default = 1200)
    # memory length
    parser.add_argument('--memory_length', 
            type = int,
            help = ("Time laps before an old piece of info is forgotten in "
            "seconds. Unless this option is specifically specified, no "
            "transaction is considered as outdated"), 
            default = None)
    # episode capacity
    parser.add_argument('--episode_capacity', 
            type=int,
            help = "Maximum number of events in an episode",  
            default = None)



if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO)

    from argparse import ArgumentParser
    parser = ArgumentParser()

    # DATA FILE
    parser.add_argument('-f', '--filename', 
            metavar = 'DATASET',
            required = True, 
            help = 'Read DATASET dump file')
    # MAXIMAL EPISODES PARAMS
    argument_parser_for_maximal_episodes(parser)

    ## READ THE PARAMETERS
    options = parser.parse_args()

    print ("OPTIONS: \n%s" % 
            '\n'.join(['\t%s:\t%s' % (a[0], a[1]) 
                for a in vars(options).items()]))

    from general_tools.dataset.parser import read_dataset
    with open(options.filename, 'r') as f:
        dataset = read_dataset(f)

    meb = MaximalEpisodesBuilder(episode_length = options.episode_length,
            episode_capacity = options.episode_capacity,
            memory_length = options.episode_length)
    meb.build_maximal_episodes(dataset)
