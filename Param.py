import logging
class Param:
	def __init__(self, name, doc, type, default_value, short_name='', required=False, choices=[]):
		self.name			= name
		self.short_name 	= short_name
		self.doc			= doc
		self.type			= type
		self.default_value	= default_value
		self.required 		= required
		self.choices 		= choices

	def __str__(self):
		s = "%s: default_value=%s %s\n%s" % (self.name, self.default_value, self.type, self.doc)
		return s

	def add_command_line_option(self, parser):
		posargs = []
		if self.short_name:
			posargs.append('-' + self.shortname)
		posargs.append("--" + self.name)

		kwargs = {'help': self.doc}

		if self.type == bool:
			if self.default_value == True:
				kwargs['action'] = "store_false"

			else:
				kwargs['action'] = "store_true"
		else:
			kwargs['default'] = self.default_value
			if self.type in [int, float, str]:
				kwargs['type'] = self.type

		if self.required:
			kwargs['required'] = True
		if self.choices:
			kwargs['choices'] = self.choices

		parser.add_argument(*posargs, **kwargs)

def build_xED_parameters(from_file=None): # FIXME to be implemented

	parameters = {}

	# General parameters
	parameters["runtime_plot"] = Param("runtime_plot",  
			"Plot some figures at runtime? Increases MUCH execution time", 
			bool, False)
	parameters["format"] = Param("format", "Image format", str, 'png')
	parameters["interactive"] = Param("interactive",  
			"Use interactive matplotlib windows instead of automatically saving the figures to file", 
			bool, False)
	parameters["log_directory"] = Param("log_directory", "Storing path prefix", str, '/home/julie/Documents/xED/exec_results')
	parameters["logging_level"] = Param("logging_level", "Logging level", str, logging.INFO)
	 

	# Episode characteristics
	parameters["maximal_episode_duration"] = Param("maximal_episode_duration", "Maximal duration of the episode occurrences", int, 1800)
	parameters["maximal_episode_length"] = Param("maximal_episode_length", "Maximal number of event labels in the episodes", int, None)


	# Frequent episode mining
	parameters["fem_method"] = Param("fem_method", "Algorithm used for frequent episode discovery", str, 'fpgrowth', choices=['fpgrowth', 'edlike', 'apriori', 'el'])
	parameters["minimal_support"] = Param("minimal_support", "Minimal support", int, 5)


	# Periodicity determination
	parameters["pem_method"] = Param("pem_method", "Algorithm used for periodic episode discovery", str, 'mixture', choices=['mixture', 'autocorrelation'])
	parameters["end_of_periodic_pattern"] = Param("end_of_periodic_pattern", "When no occurrence of a pattern has been seen for more than END_OF_PERIODIC_PATTERN * its period, it is considered as over", int, 5)
	parameters["minimal_accuracy"] = Param("minimal_accuracy", "Minimal accuracy for a candidate pattern to be fully analysed.", float, None)

	parameters["ncomp_determination_method"] = Param("ncomp_determination_method", "Method for the determination of the number of components", str, 'DBSCAN', choices=['DBSCAN', 'brute_force'])
	parameters["maximal_ncomp"] = Param("maximal_ncomp", "Maximal number of components", int, None)
	parameters["default_periods"] = Param("default_periods", "Default periods", dict, {86400: 5400, 604800:10800}) 
	parameters["maximal_stddev"] = Param("maximal_stddev", "Maximal standard deviation", float, None) #FIXME

	return parameters


