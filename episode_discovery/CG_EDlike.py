#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
import itertools
import logging

from dataset import Episode
from episode_discovery.CandidateGenerator import CandidateGenerator, MaximalEpisodesBuilder

class CG_EDlike(CandidateGenerator):
	"""
	Candidate generation based on the method used in Episode Discovery (Heierman et al., 2004)
	"""
	def __init__(self, dataset, 
			maximal_episode_length		= None, 
			maximal_episode_duration	= 1800, 
			minimal_support				= None, 
			storing_prefix				= '/tmp',
			):

		CandidateGenerator.__init__(self, dataset, 
				maximal_episode_duration 	= maximal_episode_duration,
				maximal_episode_length 		= maximal_episode_length,
				minimal_support 			= minimal_support,
				storing_prefix 				= storing_prefix,
				)

		self.maximal_episodes = []
		
	def generate_candidates(self):
		t = time.clock()

		# 1. Compute the maximal episodes
		meb = MaximalEpisodesBuilder(
				maximal_episode_duration = self.maximal_episode_duration,
				maximal_episode_length = self.maximal_episode_length,
				)
		self.maximal_episodes = meb.build_maximal_episodes(self.dataset)

		# 2. Find the candidate episodes
		# 2a. Initial set: the episodes extracted from the maximal episodes...
		for me in self.maximal_episodes:
			self.candidates.add(Episode([e.label for e in me]))
		# 2b. ... extended with the intersections of maximal episodes
		for me1, me2 in itertools.combinations(self.candidates, 2):
			intersec = me1.intersection(me2)
			if intersec:
				self.candidates.add(intersec)

		# 3. Retrieving the occurrence times
		# If minimal_support is set, remove the rare episodes
		to_be_removed = []
		for episode in self.candidates:
			occurrences, count = self.find_occurrences(episode)
			if self.minimal_support and count < self.minimal_support:
				to_be_removed.append(episode)
			else:
				self.occurrences[episode] = occurrences
		map(self.candidates.discard, to_be_removed)

		# 4. Enjoy!
		logging.debug(" %d episodes, found in %.2fs" % (len(self.candidates), time.clock()-t))
		return self.occurrences

	def find_occurrences(self, candidate):
		occurrences = []
		seen_events = []
		count = 0
		candidate_length = len(candidate)

		for max_ep in self.maximal_episodes:
			# Occurrences are subsequences of the maximal episodes
			found_new_occurrence = True
			while found_new_occurrence:
				found_new_occurrence = False
				items = []
				evts = []
				l = 0 # length of the occurrence under construction
				for evt in max_ep:
					if (evt.label not in candidate	# not the right label
							or evt.label in items	# label already used for the occ under construction
							or evt in seen_events	# event already used
							):
						continue
					evts.append(evt)
					items.append(evt.label)
					l += 1
					if l == candidate_length:
						# Occurrence is complete
						found_new_occurrence = True
						count += 1
						seen_events.extend(evts)
						occurrences.append(evts)
						break # end for loop

		return occurrences, count
	
if __name__ == '__main__':
	import argparse
	import pickle
	from DatasetParser import read_dataset
	
	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	# DATA FILE
	parser.add_argument('-f', '--filename', 
			required = True, 
			help = 'Txt dataset file')

	parser.add_argument('--maximal_episode_duration', 
			type = int, 
			help = "Maximal episode duration in seconds", 
			default = 1800)
	parser.add_argument('--maximal_episode_length', 
			type=int,
			help = "Maximum number of events in an episode",  
			default = None)
	parser.add_argument('--minimal_support', 
			type = int, 
			default = None,
			help = ('The minimal support for an episode to be considered as frequent. '
				'In ED, support is not a criterion for interestingness, '
				'so it is not mandatory here.'))
	parser.add_argument("-v", "--verbose", action="store_true", help="Verbose")

	## READ THE PARAMETERS
	options = parser.parse_args()
	if options.verbose:
		logging.basicConfig(level=logging.DEBUG)
	else:
		logging.basicConfig(level=logging.INFO)

	logging.info("\n\t".join([" Parameters used", ] + 
		["%s: %s" % (k, str(v)) for k, v in options.__dict__.items()]))

	with open(options.filename, 'r') as f:
		dataset = read_dataset(f)

	## Mine the stream...
	edl = CG_EDlike(dataset, minimal_support = options.minimal_support, 
			maximal_episode_length = options.maximal_episode_length, 
			maximal_episode_duration = options.maximal_episode_duration)

	edl.generate_candidates()
	if len(edl.candidates) < 50:
		for fe in edl.candidates:
			print fe
	logging.info(" Finished candidate generation. %d episodes" % len(edl.candidates))

	with open("episodes_fp.dump", "w") as f:
		pickle.dump(edl, f)
